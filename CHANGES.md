# ChangeLog
## version 3.5.0 - 2025-01-09
- switch to qt 6.8
- fix bad edition of spinbox when getting focus or leave focus.
## version 3.4.1 - 2024-05-22
- fix potential crash when deleting menu
## version 3.4.0 - 2024-05-21
- switch to dtk-core 3.2
## version 3.3.1 - 2024-04-05
- fix removeMenu bug
## version 3.3.0 - 2023-12-06
- fix rendering bug then a menu/item destroys itself
## version 3.2.0 - 2023-11-27
- add dtkWidgetsParameterStringVariableListComboBox
## version 3.1.3 - 2023-11-17
- fix compilation warnings
- fix cleaning bug in dtkWidgetsLogView
## version 3.1.2 - 2023-11-13
- when importing parameters values, restore by menu order.
## version 3.1.1 - 2023-11-10
- fix internal storage of parameters menus to have predictable behaviour
## version 3.1.0 - 2023-11-09
- switch to c++20
- update cmakelists
- depends on dtk-core 3.1
## version 3.0.1 - 2023-10-13
- fix connect + remove null chars in plugin path with conda
## version 3.0.0 - 2023-09-21
- update to Qt6
## version 2.30.0 - 2022-05-23
 - override setCentralWidget in order to avoid calling stackUnder at every call of menubar()
## version 2.29.1 - 2021-10-05
 - fix build on windows (missing export)
## version 2.29.0 - 2021-10-05
 - add dtkWidgetsWorkspaceLog & dtkWidgetsLogView
## version 2.28.0 - 2021-09-01
 - fix clear of dtkWidgetsController
 - fix inclusions for dtk-core 2.11
 - fix menubar reset
## version 2.27.0 - 2021-08-06
 - add intRange and doubleRange widgets for dtkCoreParameters
 - remove the connection from the widget when it's deleted
 - add new widget for parameter d_liststringlist
## version 2.26.2 - 2021-06-28
- set the configuration menu icon to "cog", instead of "question", since it is more common.
## version 2.26.1 - 2021-06-16
- highlight selected menu
- allow to reload the current path with shift+click in dtkWidgetsParameterBrowse
## version 2.26.0 - 2021-06-04
- add methods to import/export in menu
- add reset method in dtkWidgetsParameter
- add tooltips for layout buttons
- add the possibility of overriding textOn and textOff for dtkWidgetsParameterBoolPushButton
## version 2.25.1 - 2021-04-02
- fix emission of inserted signal of dtkWidgetsController
## version 2.25.0 - 2021-02-01
- add dtkWidgetsReferences example
- fix parameter/parameterWidget accessors for dtkWidgetsMenuBar
- cmake fixes : OS X flags, dependencies
- udate conda dependencies : dtk-log >= 2.3.0, alsa-lib
## version 2.24.1 - 2020-11-13
 - Fix regression introduced in 2.24.0 for dtkWidgetsParameterDirBrowse
## version 2.24.0 - 2020-10-01
 - display doc and path in the tooltip, according if both, either one or none is available
 - save button can be defined as read-only
 - support multiple menubars: bugfix #67
 - support multiple menubars: bugfix #68
## version 2.23.1 - 2020-05-28
 - fix workspace: we must leave previous workspace before creating a new one
 - apply theme in workspaceBar (also raise font size of popup menu) and dtkWidgetsHUDInfo
## version 2.23.0 - 2020-04-30
 - expose parameter menubar widgets
## version 2.22.2 - 2020-04-27
 - hotfix widgets won't change dtk parameter on manual editing
## version 2.22.1 - 2020-04-24
 - hotfix the dtk-core version for conda recipe at run time
## version 2.22.0 - 2020-04-23
 - add advanced parameters switch in dtkWidgetsMenuBar
 - move `advanced` property from core param to widget param
 - add persistent widget option for parameters
 - bugfix when destroying a workspace
## version 2.21.1 - 2020-04-08
 - fix signals handling in dtkWidgetsMenuItem that broke dtkThemes menu
## version 2.21.0 - 2020-04-07
 - Added the option to programmatically expand and collapse the menu bar
## version 2.20.0 - 2020-03-27
 - Bugfix the main window size can not be decreased vertically: gnomon issue number 254.
 - add parameters to an already existing menu with dtkWidgetParametersMenuBarGenerator
 - added read-only state for DoubleSpinBox and SpinBox, CheckBox, PushButton...
 - Bugfix: menu bar generation dtk-widgets issue #35
## version 2.19.1 - 2020-03-18
 - ReadOnly parameter widgets
## version 2.19.0 - 2020-03-11
 - Adds a convenience method to generate a menu and connect its element to parameters passed as arguments.
## version 2.18.0 - 2020-03-10
 - Adds convenience method to rename a submenu of a menu
 - Fix bugs related to mishandling of view controller
 - Improves reader and writer. Don't rely on the view name to create a view from the reader, but on the action that was used to create it the first time.
## version 2.17.2 - 2020-02-26
 - Adding convenience methods to be menus out of existing Qt APIs
## version 2.17.1 - 2020-02-24
 - Bug fixes in the menu framework
## version 2.17.0 - 2020-01-23
 - Introducing the method dtkWidgetsMenuBar::reset() to be called whenever a workspace is left to reset properly the pane slider content.
 - Fix: spinboxes now change their value upon losing focus even if Enter is not pressed.
 - Fix: menu bar buttons bigger, menu bar moved forward in Z-dimension.
 - Keyboards shortcut should now work to change workspaces.
## version 2.16.12 - 2019-12-20
 - fix enter/leave execution order in workspaces
 - redefined setCurrentIndex to match previously used API
## version 2.16.11 - 2019-12-19
 - Mimic popup behavior even though it is not (worksapce selector).
## version 2.16.10 - 2019-12-19
 - Font initialization whenever relevant.
## version 2.16.9 - 2019-12-18
 - Rewrite on workspace bar.
## version 2.16.8 - 2019-12-18
 - Ergnonomy enhanced.
## version 2.16.7 - 2019-12-17
 - Adding descriptions for each item in any level of the workspace selector.
## version 2.16.6 - 2019-12-16
 - Workspace selection enhancements.
## version 2.16.5 - 2019-12-13
 - Bring QML frameworks
 - Reimplement workspace selectors
## version 2.16.4 - 2019-12-12
 - Fixed menu bar framework inconsistency
## version 2.16.3 - 2019-12-11
 - Hotfix for standalone containers
## version 2.16.2 - 2019-12-11
 - fix dtkWidgetsLayout when closing a widget
 - Add options to menu framework
## version 2.16.1 - 2019-12-05
 - fix export for windows
 - start a drag on a dataManagerItem only when leftButton is pressed
## version 2.16.0 - 2019-11-29
 - first version of dtkWidgetsDataManager
## version 2.15.2 - 2019-11-12
 - Fixed workspace bar count.
## version 2.15.1 - 2019-10-18
 - Allow menu framework to tweak an existing Qt menu bar.
## version 2.15.0 - 2019-10-18
 - use  use shareValue when connecting a widget to a parameter
## version 2.14.0 - 2019-10-11
 - connect the method parameter->setdvanced() to show/hide a parameter
## version 2.13.2 - 2019-10-10
 - fixed horizontal scrolling in menu framework
 - allows for addition and deletion of submenus
 - managed the stacj correctly in the menu framework
## version 2.13.1 - 2019-10-09
 - fixed bad release
## version 2.13.0 - 2019-10-08
 - added insertMenu and insertItem for dtkWidgetsMenu
## version 2.12.0 - 2019-10-07
 - fixed value parsing in dtkWidgetsSpinBoxDouble
 - add removeMenu method in dtkWidgetsMenu
 - enhancing menu framework API
 - order workspaces alphabetically
 - switch to previous workspace when current one is deleted
 - apply theme in initialize instead of exec
 - add method to generator to fill a dtkWidgetsMenu
## version 2.11.7 - 2019-07-03
 - bugfix on dtkwidgetsLayout clear()
## version 2.11.6 - 2019-06-28
 - set rpath
 - expose some internals of mainWindow
## version 2.11.5 - 2019-06-03
- remove hardcoded resize in dtkApp
## version 2.11.4 - 2019-06-03
- fix mouse tracking propagation in dtkApps by calling touch in dtkWidgetsWidget showEvent
## version 2.11.3 - 2019-05-29
- call enter/leave when using workspace menu
- fix main window events on linux
- set ini format in initialize
## version 2.11.2 - 2019-05-27
- settings in ini format per default for dtkApplication
- bugfix in dtkWidgetsParameterFileSave, fileBrowse, boolPushButton
- add menuBar callback
## version 2.11.1 - 2019-05-21
- menubar generator use dtk-core readParameters static method
## version 2.11.0 - 2019-05-21
- menubar generator use dtk-core parameter reader
- add python converter for parameters new json file formats
- fix menubar size
## version 2.10.0 - 2019-05-17
- cmake refactoring
- fix interaction
- better handling of font setting
## version 2.9.2 - 2019-05-03
- do not move window when we are over a menu button
## version 2.9.1 - 2019-05-02
- call enter/leave method when entering/leaving workspaces
- deactivate unused fileMenu and AboutMenu. Move ThemeMenu in a sub-menu
- menubar fixed for HighDpi
## version 2.9.0 - 2019-04-29
- add dtkWidgetsParameterLongLongSpinBox
- use dtk-themes
## version 2.8.0 - 2019-03-29
- add dtkWidgetsParameterFileSave
- add dtkWidgetsParameterScientificSpinBox
- add dtkWidgetsParameterStringLineEdit
## version 2.7.0 - 2019-03-21
- add dtkWidgetsMenu (remove dtkWidgetsOverlayPaneManager and toggle button in Pane)
- add dtk-core as dependancie for new features:
- add Workspace abstraction with factory and manager; add also dtkWidgetsWorkspaceBar and StackBar
- add widgets parameters
## version 2.6.4 - 2019-03-12
- overlayPane dynamically adjust its width
## version 2.6.3 - 2019-02-22
- fix resizing of dtkWidgetsOverlayPane
- do not use hard coded color in dtkWidgetsHUDItem QPainter
## version 2.6.2 - 2019-02-20
- add the possibility to have a toogle button (with tooltip) to dtkWidgetsOverlayPane
- add a dtkWidgetsOverlayPaneManager to have multiple dtkWidgetsOverlayPane with toogle buttons
## version 2.6.0 - 2019-02-14
- allows to disable the footer of the dtkWidgetsLayoutItem
- fix the display of the object name of the view in the footer of the dtkWidgetsLayoutItem
- allows to rename the view from the footer of the dtkWidgetsLayoutItem
- API CHANGE : dtkWidgetsController inserted(dtkWidgetsWidget *, const QString&) to inserted(dtkWidgetsWidget *)
## version 2.5.0 - 2019-02-06
- add method to change size of dtkWidgetsOverlayPane & dtkWidgetsOverlayPaneSlider
## version 2.4.0 - 2019-02-03
- add method to provide and display tooltip on hover of slider bar icons
## version 2.3.0 - 2018-12-10
- add back dtkApplication
- depends on dtk-log
## version 2.2.0 - 2018-10-12
- layout persistence
- add scrolling framework
## version 2.1.3 - 2018-09-20
- fix HUD rendering
- add clear method to OverlayPane
## version 2.1.2 - 2018-09-11
- fix install for windows
## version 2.1.1 - 2018-09-11
- fix EXPORT for windows build
## version 2.1.0 - 2018-07-02
- sdm-modeler release
- add dtkWidgetsStylesheetParser
## version 2.0.0 - 2018-05-18
- initial release
