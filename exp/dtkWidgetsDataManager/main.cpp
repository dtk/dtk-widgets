// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>
#include <QtWidgets>

#include <dtkCore>
#include <dtkFonts>
#include <dtkThemes>
#include <dtkWidgets>

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class DropArea : public QLabel
{
    Q_OBJECT

public:
    explicit DropArea(QWidget *parent = nullptr) {
        setMinimumSize(200, 200);
        setFrameStyle(QFrame::Sunken | QFrame::StyledPanel);
        setAlignment(Qt::AlignCenter);
        setAcceptDrops(true);
        setAutoFillBackground(true);
        clear();
    }

public slots:
    void clear(void) {
        setText(tr("<drop content>"));
        setBackgroundRole(QPalette::Dark);

        emit changed();
    }

signals:
    void changed(const QMimeData *mimeData = nullptr);

protected:
    void dragEnterEvent(QDragEnterEvent *event) override {
        setText(tr("<drop content>"));
        setBackgroundRole(QPalette::Highlight);

        event->acceptProposedAction();

        emit changed(event->mimeData());
    }

    void dragMoveEvent(QDragMoveEvent *event) override {
        event->acceptProposedAction();
    }

    void dropEvent(QDropEvent *event) override {
        const QMimeData *mimeData = event->mimeData();
        setText(mimeData->text());
        long idWorld = std::stol(mimeData->text().toStdString().substr(1));
        setBackgroundRole(QPalette::Dark);
        event->acceptProposedAction();
    }

    void dragLeaveEvent(QDragLeaveEvent *event) override {
        clear();
        event->accept();
    }
};


// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class dtkFinderExamplePrivate
{
public:
    dtkWidgetsFinder *finder;
    dtkWidgetsFinderPathBar *path;
    dtkWidgetsFinderToolBar *toolbar;
    dtkWidgetsFinderSideView *sideView;
};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class dtkFinderExample : public QFrame
{
public:
    dtkFinderExample(QWidget *parent = nullptr)
    {
        d = new dtkFinderExamplePrivate;

        d->finder = new dtkWidgetsFinder(this);
        d->finder->switchToTreeView();
        d->finder->setPath(QDir::currentPath());

        d->path = new dtkWidgetsFinderPathBar(this);
        d->path->setPath(QDir::currentPath());
        d->path->setFixedHeight(32);

        d->toolbar = new dtkWidgetsFinderToolBar(this);
        d->toolbar->setPath(QDir::currentPath());

        d->sideView = new dtkWidgetsFinderSideView(this);

        QHBoxLayout *toolbar_layout = new QHBoxLayout;
        toolbar_layout->setContentsMargins(0, 0, 0, 0);
        toolbar_layout->setSpacing(0);
        toolbar_layout->addWidget(d->toolbar);
        toolbar_layout->addWidget(d->path);

        QVBoxLayout *finder_layout = new QVBoxLayout;
        finder_layout->setContentsMargins(0, 0, 0, 0);
        finder_layout->setSpacing(0);
        finder_layout->addLayout(toolbar_layout);
        finder_layout->addWidget(d->finder);

        QHBoxLayout *finder_with_sider_layout = new QHBoxLayout;
        finder_with_sider_layout->setContentsMargins(0, 0, 0, 0);
        finder_with_sider_layout->setSpacing(0);
        finder_with_sider_layout->addWidget(d->sideView);
        finder_with_sider_layout->addLayout(finder_layout);

        QWidget *finder = new QWidget(this);
        finder->setLayout(finder_with_sider_layout);
        QWidget* emitters[] = {d->finder, d->path, d->sideView, d->toolbar};
        for (auto emitter: emitters) {
            for (auto receiver: emitters) {
                if (emitter != receiver) {
                    connect(emitter, SIGNAL(changed(QString)), receiver, SLOT(setPath(QString)));
                }
            }
        }

        connect(d->toolbar, SIGNAL(treeView()), d->finder, SLOT(switchToTreeView()));
        connect(d->toolbar, SIGNAL(listView()), d->finder, SLOT(switchToListView()));

        QSplitter *splitter = new QSplitter(this);
        splitter->addWidget(finder);

        QVBoxLayout *layout = new QVBoxLayout(this);
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(0);
        layout->addWidget(splitter);
    }

    ~dtkFinderExample(void) {
        delete d;
    }

private:
    dtkFinderExamplePrivate *d;
};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    dtk::widgets::initialize("");
    dtkThemesEngine::instance()->apply();

    dtkApplication *application = dtkApplication::create(argc, argv);
    application->setApplicationName("dtkDataManager");
    application->setOrganizationName("inria");
    application->setOrganizationDomain("fr");
    application->initialize();

    auto data1 = new dtkWidgetsDataManagerData;
    data1->addMetadata(dtkWidgetsDataManagerData::ICON, ":earth");
    data1->addMetadata("creation", QDate(2007, 5, 3));
    data1->setData(QString("Hello data1"));

    dtkWidgetsDataManager *manager = dtkWidgetsDataManager::instance();
    manager->addData(data1, Qt::red);

    //auto metadataModel = new QStandardItemModel;

    //auto metadataView = new QTableView;
    //metadataView->setModel(metadataModel);

    // QObject::connect(manager, &dtkWidgetsDataManager::itemClickedSignal, [=] (dtkWidgetsDataManagerItem *item)
    // {
    //     long dataId = item->id;

    //     qDebug() << Q_FUNC_INFO <<  "item = " << dataId;

    //     auto data = manager->get(dataId);

    //     metadataModel->clear();

    //     int row = 0;

    //     for (auto it = data->metadata.begin(); it != data->metadata.end(); ++it, ++row) {
    //         auto key = new QStandardItem(it.key());
    //         auto value = new QStandardItem(it.value().toString());
    //         metadataModel->setItem(row, 0, key);
    //         metadataModel->setItem(row, 1, value);
    //     }
    // });

    auto dropArea = new DropArea;

    auto vlayout = new QVBoxLayout;
    vlayout->addWidget(manager);
    //vlayout->addWidget(metadataView);
    vlayout->addWidget(dropArea);

    static int l_h = 0;

    QObject::connect(manager, &dtkWidgetsDataManager::expand, [&](void)
    {
        if(dropArea->height() < 10)
            return;

        int m_h = manager->height();
        int d_h = dropArea->height();
        l_h = d_h;

        QVariantAnimation *animation = new QVariantAnimation();
        animation->setDuration(500);
        animation->setStartValue(d_h);
        animation->setEndValue(0);
        animation->setEasingCurve(QEasingCurve::OutQuad);

        QObject::connect(animation, &QVariantAnimation::valueChanged, [=] (const QVariant& value)
        {
            dropArea->setFixedHeight(value.toInt());
            manager->setFixedHeight(m_h + d_h -  value.toInt());
        });

        animation->start(QAbstractAnimation::DeleteWhenStopped);
    });

    QObject::connect(manager, &dtkWidgetsDataManager::shrink, [&](void)
    {
        if(manager->height() < 10)
            return;

        int m_h = manager->height();

        QVariantAnimation *animation = new QVariantAnimation();
        animation->setDuration(500);
        animation->setStartValue(0);
        animation->setEndValue(l_h);
        animation->setEasingCurve(QEasingCurve::OutQuad);

        QObject::connect(animation, &QVariantAnimation::valueChanged, [=] (const QVariant& value)
        {
            dropArea->setFixedHeight(value.toInt());
            manager->setFixedHeight(m_h - value.toInt());
        });

        animation->start(QAbstractAnimation::DeleteWhenStopped);
    });


    auto hlayout = new QHBoxLayout;
    hlayout->addWidget(new dtkFinderExample);
    hlayout->addLayout(vlayout);

    auto frame = new QFrame;
    frame->setLayout(hlayout);
    frame->show();
    frame->raise();

    return QApplication::exec();
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

#include "main.moc"

//
// main.cpp ends here
