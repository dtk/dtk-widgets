// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include  <QtWidgets>
#include <dtkWidgets>

// /////////////////////////////////////////////////////////////////////////////
// 
// /////////////////////////////////////////////////////////////////////////////

class dtkFinderExample : public QFrame
{
public:
    dtkFinderExample(QWidget *parent = nullptr) {

        this->finder = new dtkWidgetsFinder(this);
        this->finder->switchToTreeView();

        this->path = new dtkWidgetsFinderPathBar(this);
        this->path->setPath(QDir::currentPath());
        this->path->setFixedHeight(32);

        this->toolbar = new dtkWidgetsFinderToolBar(this);
        this->toolbar->setPath(QDir::currentPath());

        this->sideView = new dtkWidgetsFinderSideView(this);

        QHBoxLayout *toolbar_layout = new QHBoxLayout;
        toolbar_layout->setContentsMargins(0, 0, 0, 0);
        toolbar_layout->setSpacing(0);
        toolbar_layout->addWidget(this->toolbar);
        toolbar_layout->addWidget(this->path);

        QVBoxLayout *finder_layout = new QVBoxLayout;
        finder_layout->setContentsMargins(0, 0, 0, 0);
        finder_layout->setSpacing(0);
        finder_layout->addLayout(toolbar_layout);
        finder_layout->addWidget(this->finder);

        QHBoxLayout *finder_with_sider_layout = new QHBoxLayout;
        finder_with_sider_layout->setContentsMargins(0, 0, 0, 0);
        finder_with_sider_layout->setSpacing(0);
        finder_with_sider_layout->addWidget(this->sideView);
        finder_with_sider_layout->addLayout(finder_layout);

        QWidget *finder = new QWidget(this);
        finder->setLayout(finder_with_sider_layout);

        connect(this->finder, SIGNAL(changed(QString)), this->path, SLOT(setPath(QString)));
        connect(this->finder, SIGNAL(changed(QString)), this->toolbar, SLOT(setPath(QString)));

        connect(this->path, SIGNAL(changed(QString)), this->finder, SLOT(setPath(QString)));
        connect(this->path, SIGNAL(changed(QString)), this->toolbar, SLOT(setPath(QString)));

        connect(this->toolbar, SIGNAL(changed(QString)), this->finder, SLOT(setPath(QString)));
        connect(this->toolbar, SIGNAL(changed(QString)), this->path, SLOT(setPath(QString)));
        connect(this->toolbar, SIGNAL(treeView()), this->finder, SLOT(switchToTreeView()));
        connect(this->toolbar, SIGNAL(listView()), this->finder, SLOT(switchToListView()));
        connect(this->toolbar, SIGNAL(showHiddenFiles(bool)), this->finder, SLOT(switchShowHiddenFiles()));

        QSplitter *splitter = new QSplitter(this);
        splitter->addWidget(finder);

        QVBoxLayout *layout = new QVBoxLayout(this);
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(0);
        layout->addWidget(splitter);
    }

private:
    dtkWidgetsFinder *finder;
    dtkWidgetsFinderPathBar *path;
    dtkWidgetsFinderToolBar *toolbar;
    dtkWidgetsFinderSideView *sideView;
};

// /////////////////////////////////////////////////////////////////////////////
// 
// /////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    QApplication application(argc, argv);

    dtkFinderExample *finder = new dtkFinderExample;
    finder->show();
    finder->raise();

    return application.exec();
}

//
// main.cpp ends here
