//                              -*- Mode: C++ -*-
//
// Version: $Id$
//
//

// Commentary:
//
// Request example: https://api.archives-ouvertes.fr/search/?q=authLastName_s:Wintz&wt=xml

// Change Log:
//
//

// Code:

#include <QtCore>
#include <QtDebug>
#include <QtNetwork>
#include <QtWebEngine>
#include <QtWebEngineWidgets>
#include <QtWidgets>

#include <dtkThemes>
#include <dtkWidgets>

// /////////////////////////////////////////////////////////////////////////////
// dtkReferenceFetcher
// /////////////////////////////////////////////////////////////////////////////

class dtkReferenceFetcher : public QObject
{
    Q_OBJECT

public:
     dtkReferenceFetcher(void);
    ~dtkReferenceFetcher(void);

public:
    void addField(QString, QString);
    void addResultType(QString);
    void fetch(void);
    void clear(void);
    int count(void);
    QString at(int) const;

public: // TODO: Switch to private
    QString url; 
    QJsonDocument document;

private:
    QNetworkRequest *request = nullptr;
    QNetworkAccessManager *manager = nullptr;

private:
    int counter = 0;
    bool dirty = true;

private:
    static QString prefix_url;

private:
    friend class dtkReferenceFetcherModel;
};

// /////////////////////////////////////////////////////////////////////////////

QString dtkReferenceFetcher::prefix_url = "https://api.archives-ouvertes.fr/search/?q=";

dtkReferenceFetcher::dtkReferenceFetcher(void)
{
    this->url = prefix_url;
    this->request = new QNetworkRequest;
    this->manager = new QNetworkAccessManager;

    connect(this->manager, &QNetworkAccessManager::finished, [&] (QNetworkReply *reply) -> void
    {
        QString contents = reply->readAll();

        QJsonDocument proxy = QJsonDocument::fromJson(qPrintable(contents));

        this->document = QJsonDocument(proxy["response"]["docs"].toArray());

        dirty = false;
    });
}

dtkReferenceFetcher::~dtkReferenceFetcher()
{
    delete this->request;
    delete this->manager;
}

void dtkReferenceFetcher::addField(QString name_field, QString value_field)
{
    if(counter++)
        this->url += "&";

    this->url += name_field + ":" + value_field;
}

void dtkReferenceFetcher::addResultType(QString result_type)
{   
    this->url += "&wt="+ result_type;
}

void dtkReferenceFetcher::fetch(void)
{
    dirty = true;

    request->setUrl(QUrl(this-> url));

    manager->get(*(this->request));

    while(dirty)
        qApp->processEvents();

    qApp->processEvents();
}

void dtkReferenceFetcher::clear(void)
{
    this->url = prefix_url;

    this->counter = 0;
}

int dtkReferenceFetcher::count(void)
{   
    return this->document["response"]["numFound"].toVariant().toDouble();
}

QString dtkReferenceFetcher::at(int index) const
{   
    return this->document["response"]["docs"][index]["uri_s"].toVariant().toString();
}

// /////////////////////////////////////////////////////////////////////////////
// dtkReferenceFetcherModelItem
// /////////////////////////////////////////////////////////////////////////////

class dtkReferenceModelItem
{
public:
     dtkReferenceModelItem(dtkReferenceModelItem * parent = nullptr);
    ~dtkReferenceModelItem(void);

    void appendChild(dtkReferenceModelItem * item);

    dtkReferenceModelItem *child(int row);
    dtkReferenceModelItem *parent();

    int childCount() const;
    int row() const;

    void setKey(const QString& key);
    void setValue(const QString& value);
    void setType(const QJsonValue::Type& type);

    QString key() const;
    QString value() const;

    QJsonValue::Type type() const;

    static dtkReferenceModelItem* load(const QJsonValue& value, dtkReferenceModelItem * parent = 0);

private:
    QString mKey;
    QString mValue;
    QJsonValue::Type mType;
    QList<dtkReferenceModelItem*> mChilds;
    dtkReferenceModelItem * mParent;
};

// /////////////////////////////////////////////////////////////////////////////

dtkReferenceModelItem::dtkReferenceModelItem(dtkReferenceModelItem *parent)
{
    mParent = parent;
}

dtkReferenceModelItem::~dtkReferenceModelItem()
{
    qDeleteAll(mChilds);
}

void dtkReferenceModelItem::appendChild(dtkReferenceModelItem *item)
{
    mChilds.append(item);
}

dtkReferenceModelItem *dtkReferenceModelItem::child(int row)
{
    return mChilds.value(row);
}

dtkReferenceModelItem *dtkReferenceModelItem::parent()
{
    return mParent;
}

int dtkReferenceModelItem::childCount() const
{
    return mChilds.count();
}

int dtkReferenceModelItem::row() const
{
    if (mParent)
        return mParent->mChilds.indexOf(const_cast<dtkReferenceModelItem*>(this));

    return 0;
}

void dtkReferenceModelItem::setKey(const QString &key)
{
    mKey = key;
}

void dtkReferenceModelItem::setValue(const QString &value)
{
    mValue = value;
}

void dtkReferenceModelItem::setType(const QJsonValue::Type &type)
{
    mType = type;
}

QString dtkReferenceModelItem::key() const
{
    return mKey;
}

QString dtkReferenceModelItem::value() const
{
    return mValue;
}

QJsonValue::Type dtkReferenceModelItem::type() const
{
    return mType;
}

dtkReferenceModelItem* dtkReferenceModelItem::load(const QJsonValue& value, dtkReferenceModelItem* parent)
{
    dtkReferenceModelItem * rootItem = new dtkReferenceModelItem(parent);
    rootItem->setKey("root");

    if ( value.isObject())
    {

        //Get all QJsonValue childs
        for (QString key : value.toObject().keys()){
            QJsonValue v = value.toObject().value(key);
            dtkReferenceModelItem * child = load(v,rootItem);
            child->setKey(key);
            child->setType(v.type());
            rootItem->appendChild(child);

        }

    }

    else if ( value.isArray())
    {
        //Get all QJsonValue childs
        int index = 0;
        for (QJsonValue v : value.toArray()){

            dtkReferenceModelItem * child = load(v,rootItem);
            child->setKey(QString::number(index));
            child->setType(v.type());
            rootItem->appendChild(child);
            ++index;
        }
    }
    else
    {
        rootItem->setValue(value.toVariant().toString());
        rootItem->setType(value.type());
    }

    return rootItem;
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class dtkReferenceModel : public QAbstractItemModel
{
    Q_OBJECT

public:
     dtkReferenceModel(QObject *parent = nullptr);
     dtkReferenceModel(const QString& fileName, QObject *parent = nullptr);
     dtkReferenceModel(QIODevice * device, QObject *parent = nullptr);
     dtkReferenceModel(const QByteArray& json, QObject *parent = nullptr);
    ~dtkReferenceModel();

    bool load(const QJsonDocument& document);
    bool load(const QString& fileName);
    bool load(QIODevice * device);
    bool loadJson(const QByteArray& json);
    bool loadJson(const QJsonObject& json);

public:
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;

    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) Q_DECL_OVERRIDE;

    QVariant headerData(int section, Qt::Orientation orientation, int role) const Q_DECL_OVERRIDE;

    QModelIndex index(int row, int column,const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QModelIndex parent(const QModelIndex &index) const Q_DECL_OVERRIDE;

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

    QJsonDocument json() const;

public:
    void setAlignment(Qt::AlignmentFlag flag)
    {
        this->alignment = flag;
    }

private:
    QJsonValue genJson(dtkReferenceModelItem *) const;
    dtkReferenceModelItem * mRootItem;
    QStringList mHeaders;
    Qt::AlignmentFlag alignment = Qt::AlignLeft;
};

// /////////////////////////////////////////////////////////////////////////////

dtkReferenceModel::dtkReferenceModel(QObject *parent)
    : QAbstractItemModel(parent)
    , mRootItem{new dtkReferenceModelItem}
{
    mHeaders.append("key");
    mHeaders.append("value");
}

dtkReferenceModel::dtkReferenceModel(const QString& fileName, QObject *parent)
    : QAbstractItemModel(parent)
    , mRootItem{new dtkReferenceModelItem}
{
    mHeaders.append("key");
    mHeaders.append("value");
    load(fileName);
}

dtkReferenceModel::dtkReferenceModel(QIODevice * device, QObject *parent)
    : QAbstractItemModel(parent)
    , mRootItem{new dtkReferenceModelItem}
{
    mHeaders.append("key");
    mHeaders.append("value");
    load(device);
}

dtkReferenceModel::dtkReferenceModel(const QByteArray& json, QObject *parent)
    : QAbstractItemModel(parent)
    , mRootItem{new dtkReferenceModelItem}
{
    mHeaders.append("key");
    mHeaders.append("value");
    loadJson(json);
}

dtkReferenceModel::~dtkReferenceModel()
{
    delete mRootItem;
}

bool dtkReferenceModel::load(const QJsonDocument& document)
{
    if (!document.isNull())
    {
        beginResetModel();
        delete mRootItem;
        if (document.isArray()) {
            mRootItem = dtkReferenceModelItem::load(QJsonValue(document.array()));
            mRootItem->setType(QJsonValue::Array);

        } else {
            mRootItem = dtkReferenceModelItem::load(QJsonValue(document.object()));
            mRootItem->setType(QJsonValue::Object);
        }
        endResetModel();
        return true;
    }

    qDebug() << Q_FUNC_INFO << "cannot load json";

    return false;
}

bool dtkReferenceModel::load(const QString &fileName)
{
    QFile file(fileName);

    bool success = false;

    if (file.open(QIODevice::ReadOnly)) {
        success = load(&file);
        file.close();
    }
    else
        success = false;

    return success;
}

bool dtkReferenceModel::load(QIODevice *device)
{
    return loadJson(device->readAll());
}

bool dtkReferenceModel::loadJson(const QByteArray &json)
{
    auto const& document = QJsonDocument::fromJson(json);

    return this->load(document);
}

bool dtkReferenceModel::loadJson(const QJsonObject& json)
{
    auto const& document = QJsonDocument(json);

    return this->load(document);
}

QVariant dtkReferenceModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    dtkReferenceModelItem *item = static_cast<dtkReferenceModelItem*>(index.internalPointer());

    if (role == Qt::DisplayRole) {
        if (index.column() == 0)
            return QString("%1").arg(item->key());
        if (index.column() == 1)
            return QString("%1").arg(item->value());
    } else if (Qt::EditRole == role) {
        if (index.column() == 1) {
            return QString("%1").arg(item->value());
        }
    } else if (Qt::TextAlignmentRole == role) {
        if (index.column() == 0 && this->alignment != Qt::AlignLeft) {
            return this->alignment;
        }
    }

    return QVariant();
}

bool dtkReferenceModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    int col = index.column();
    
    if (Qt::EditRole == role) {
        if (col == 1) {
            dtkReferenceModelItem *item = static_cast<dtkReferenceModelItem*>(index.internalPointer());
                item->setValue(value.toString());
                emit dataChanged(index, index, {Qt::EditRole});
                return true;
        }
    }

    return false;
}

QVariant dtkReferenceModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {

        return mHeaders.value(section);
    }
    else
        return QVariant();
}

QModelIndex dtkReferenceModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    dtkReferenceModelItem *parentItem;

    if (!parent.isValid())
        parentItem = mRootItem;
    else
        parentItem = static_cast<dtkReferenceModelItem*>(parent.internalPointer());

    dtkReferenceModelItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex dtkReferenceModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    dtkReferenceModelItem *childItem = static_cast<dtkReferenceModelItem*>(index.internalPointer());
    dtkReferenceModelItem *parentItem = childItem->parent();

    if (parentItem == mRootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int dtkReferenceModel::rowCount(const QModelIndex &parent) const
{
    dtkReferenceModelItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = mRootItem;
    else
        parentItem = static_cast<dtkReferenceModelItem*>(parent.internalPointer());

    return parentItem->childCount();
}

int dtkReferenceModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 2;
}

Qt::ItemFlags dtkReferenceModel::flags(const QModelIndex &index) const
{
    int col   = index.column();
    auto item = static_cast<dtkReferenceModelItem*>(index.internalPointer());

    auto isArray = QJsonValue::Array == item->type();
    auto isObject = QJsonValue::Object == item->type();

    if ((col == 1) && !(isArray || isObject)) {
        return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
    } else {
        return QAbstractItemModel::flags(index);
    }
}

QJsonDocument dtkReferenceModel::json() const
{

    auto v = genJson(mRootItem);
    QJsonDocument doc;

    if (v.isObject()) {
        doc = QJsonDocument(v.toObject());
    } else {
        doc = QJsonDocument(v.toArray());
    }

    return doc;
}

QJsonValue  dtkReferenceModel::genJson(dtkReferenceModelItem * item) const
{
    auto type   = item->type();
    int  nchild = item->childCount();

    if (QJsonValue::Object == type) {
        QJsonObject jo;
        for (int i = 0; i < nchild; ++i) {
            auto ch = item->child(i);
            auto key = ch->key();
            jo.insert(key, genJson(ch));
        }
        return  jo;
    } else if (QJsonValue::Array == type) {
        QJsonArray arr;
        for (int i = 0; i < nchild; ++i) {
            auto ch = item->child(i);
            arr.append(genJson(ch));
        }
        return arr;
    } else {
        QJsonValue va(item->value());
        return va;
    }

}

// /////////////////////////////////////////////////////////////////////////////
// dtkReferenceFetcherDialog
// /////////////////////////////////////////////////////////////////////////////

class dtkReferenceFetcherDialog : public QWebEngineView
{
    Q_OBJECT

public:
     dtkReferenceFetcherDialog(void);
    ~dtkReferenceFetcherDialog(void);

public slots:
    void setIndex(int index);

public:
    void setFetcher(dtkReferenceFetcher *);

// private:
//     QWebEnginePage *page = nullptr;

private:
    dtkReferenceFetcher *fetcher;
};

dtkReferenceFetcherDialog::dtkReferenceFetcherDialog(void) : QWebEngineView(0)
{
    this->page()->setBackgroundColor(dtkThemesEngine::instance()->color("@bg"));
}
 
dtkReferenceFetcherDialog::~dtkReferenceFetcherDialog(void)
{

}

void dtkReferenceFetcherDialog::setIndex(int index)
{
    this->setHtml(QString("<html><head><style>body { color: %2; }</style></head><body><h1>%1</h1><br/><br/><p>There will be some results</p></body></html>").arg(index).arg(dtkThemesEngine::instance()->value("@fg")));
    this->page()->setBackgroundColor(dtkThemesEngine::instance()->color("@bg"));
}

void dtkReferenceFetcherDialog::setFetcher(dtkReferenceFetcher *fetcher)
{
    this->fetcher = fetcher;
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class dtkReferenceSearchBar : public QLineEdit
{
    Q_OBJECT

public:
    dtkReferenceSearchBar(QWidget *parent = nullptr);

public:
    QSize sizeHint(void) const;

public slots:
    void polish(void);

protected:
    void paintEvent(QPaintEvent *event);
};

dtkReferenceSearchBar::dtkReferenceSearchBar(QWidget *parent) : QLineEdit(parent)
{
    QStringList completions;
    completions << "email_s";
    completions << "authFirstName_t";
    completions << "authFullName_t";
    completions << "authLastName_t";
    completions << "text";

    QCompleter *completer = new QCompleter(completions, this);
    completer->setCaseSensitivity(Qt::CaseInsensitive);

    this->setCompleter(completer);
    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    connect(dtkThemesEngine::instance(), SIGNAL(changed()), this, SLOT(polish()));
}

QSize dtkReferenceSearchBar::sizeHint(void) const
{
    return QSize(100, 32);
}

void dtkReferenceSearchBar::polish(void)
{
    this->setStyleSheet(QString("background: %1; border-radius: 0px; padding-left: 32px;").arg(dtkThemesEngine::instance()->value("@base6")));
}

void dtkReferenceSearchBar::paintEvent(QPaintEvent *event)
{
    QLineEdit::paintEvent(event);

    dtkFontAwesome::instance()->initFontAwesome();
    dtkFontAwesome::instance()->setDefaultOption("color", dtkThemesEngine::instance()->value("@fg"));

    QPainter painter(this);
    painter.drawPixmap(5, 5, 22, 22, dtkFontAwesome::instance()->icon(fa::search).pixmap(22, 22));
}

// /////////////////////////////////////////////////////////////////////////////
// main
// /////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    dtkThemesEngine::instance()->apply();

    QApplication application(argc, argv);
    application.setApplicationName("dtkWidgetsReferences");
    application.setOrganizationName("inria");
    application.setOrganizationDomain("fr");

// /////////////////////////////////////////////////////////////////////////////
// Fetcher (Logic Part)
// /////////////////////////////////////////////////////////////////////////////

    dtkReferenceFetcher fetcher;
    fetcher.addField("authLastName_t", "Kloczko");
    fetcher.addResultType("json");
    fetcher.fetch();

    // TODO: Fixme
    // qDebug() << "count" << fetcher.count();
    // qDebug() << "link for to the second element" << fetcher.at(1);

// /////////////////////////////////////////////////////////////////////////////
// Fetcher (Ux Part - Model)
// /////////////////////////////////////////////////////////////////////////////

    dtkReferenceModel model;
    model.setAlignment(Qt::AlignCenter);
    model.load(fetcher.document);

    dtkReferenceModel model_p;
    if (model.rowCount())
        model_p.loadJson(fetcher.document.array().at(0).toObject());

    QTableView view_a;
    view_a.setModel(&model_p);
    view_a.setAlternatingRowColors(true);
    view_a.horizontalHeader()->setStretchLastSection(true);
    view_a.verticalHeader()->hide();

    QListView view_l;
    view_l.setModel(&model);
    view_l.setAlternatingRowColors(true);
    view_l.setFixedWidth(80);
    view_l.setStyleSheet("QListView::item { min-height: 30; }");

    dtkReferenceSearchBar *search = new dtkReferenceSearchBar;
    search->setText("authLastName_t:Kloczko");

    dtkReferenceFetcherDialog fetcher_dialog;
    fetcher_dialog.setFetcher(&fetcher);
    fetcher_dialog.setMinimumHeight(200);
    fetcher_dialog.setIndex(0);

    QVBoxLayout *layout_i = new QVBoxLayout;
    layout_i->setContentsMargins(0, 0, 0, 0);
    layout_i->setSpacing(0);
    layout_i->addWidget(search);
    layout_i->addWidget(&view_a);
    layout_i->addWidget(&fetcher_dialog);

    QWidget *rhs = new QWidget;
    rhs->setLayout(layout_i);

    QSplitter *splitter = new QSplitter;
    splitter->addWidget(&view_l);
    splitter->addWidget(rhs);
    splitter->setHandleWidth(1);

    QWidget *central = new QWidget;

    dtkWidgetsMenuBar *menu = new dtkWidgetsMenuBar(splitter);
    menu->setWidth(32);

    dtkWidgetsMenu *menu_themes = menu->addMenu(fa::paintbrush, "Choose Themes");
    for (const QString theme : dtkThemesEngine::instance()->themes()) {
        dtkWidgetsMenuItem * item = menu_themes->addItem(new dtkWidgetsMenuItemTheme(theme));
        QObject::connect(item, &dtkWidgetsMenuItem::clicked, [=] () {
            dtkThemesEngine::instance()->apply(theme);
        });
    }
    menu->touch();

    QHBoxLayout *layout = new QHBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    layout->addWidget(menu);
    layout->addWidget(splitter);

    central->setLayout(layout);

    QMainWindow *window = new QMainWindow;
    window->setCentralWidget(central);
    window->resize(800, 400);
    window->show();
    window->raise();

// /////////////////////////////////////////////////////////////////////////////
// Connections
// /////////////////////////////////////////////////////////////////////////////

    QObject::connect(search, &QLineEdit::returnPressed, [&] (void) -> void
    {
        fetcher.clear();
        fetcher.addField(search->text().split(":").at(0), search->text().split(":").at(1));
        fetcher.addResultType("json");
        fetcher.fetch();

        model.load(fetcher.document);

        view_l.setModel(&model);
        view_a.setModel(0);
    });

    QObject::connect(&view_l, &QListView::clicked, [&] (const QModelIndex &index) -> void
    {
        int row = index.row();

        QJsonObject data = fetcher.document.array().at(row).toObject();

        fetcher_dialog.setIndex(row);

        model_p.loadJson(data);
        model_p.revert();

        view_a.setModel(&model_p);
    });

// /////////////////////////////////////////////////////////////////////////////
// Fetcher (Ux Part - MenuItem) - c.f. dtkWidgetsMenu*
// /////////////////////////////////////////////////////////////////////////////

    // dtkReferenceFetcherMenuItem fetcher_menu_item;
    // fetcher_menu_item.setFetcher(&fetcher);
    // fetcher_menu_item.show();

// /////////////////////////////////////////////////////////////////////////////

    dtkThemesEngine::instance()->apply();

    int status = application.exec();

    return status;
}

#include "main.moc" // Ne regarde pas ca

//
// main.cpp ends here
