// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtWidgets>

#include <dtkFonts>
#include <dtkThemes>
#include <dtkWidgets>

// ///////////////////////////////////////////////////////////////////
// dummyWorkspaceA
// ///////////////////////////////////////////////////////////////////

class dummyWorkspaceA: public dtkWidgetsWorkspace
{
    Q_OBJECT

public:
    dummyWorkspaceA(QWidget *parent = nullptr) {

        QLabel *label =  new QLabel("Workspace A", this);
        label->setAlignment(Qt::AlignCenter);
        label->setStyleSheet("background: \"#ff0000\"; color: \"#ffffff\"");

        QHBoxLayout *layout = new QHBoxLayout(this);
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(0);
        layout->addWidget(label);
    };

    ~dummyWorkspaceA(void) {};

public:
    void enter(void) override {}
    void leave(void) override {}

public slots:
    void apply(void) override {};
};

static dtkWidgetsWorkspace *dummyWorkspaceCreatorA(void) {
    return new dummyWorkspaceA;
}

// ///////////////////////////////////////////////////////////////////
// dummyWorkspaceB
// ///////////////////////////////////////////////////////////////////

class dummyWorkspaceB: public dtkWidgetsWorkspace
{
    Q_OBJECT

public:
    dummyWorkspaceB(QWidget *parent = nullptr) {

        QLabel *label =  new QLabel("Workspace B", this);
        label->setAlignment(Qt::AlignCenter);
        label->setStyleSheet("background: \"#00ff00\"; color: \"#ffffff\"");

        QHBoxLayout *layout = new QHBoxLayout(this);
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(0);
        layout->addWidget(label);
    };

    ~dummyWorkspaceB(void) {};

public:
    void enter(void) override {}
    void leave(void) override {}

public slots:
    void apply(void) override {};
};

static dtkWidgetsWorkspace *dummyWorkspaceCreatorB(void) {
    return new dummyWorkspaceB;
}

// ///////////////////////////////////////////////////////////////////
// dummyWorkspaceC
// ///////////////////////////////////////////////////////////////////

class dummyWorkspaceC: public dtkWidgetsWorkspace
{
    Q_OBJECT

public:
    dummyWorkspaceC(QWidget *parent = nullptr) {

        QLabel *label =  new QLabel("Workspace C", this);
        label->setAlignment(Qt::AlignCenter);
        label->setStyleSheet("background: \"#0000ff\"; color: \"#ffffff\"");

        QHBoxLayout *layout = new QHBoxLayout(this);
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(0);
        layout->addWidget(label);
    };

    ~dummyWorkspaceC(void) {};

public:
    void enter(void) override {}
    void leave(void) override {}

public slots:
    void apply(void) override {};
};

static dtkWidgetsWorkspace *dummyWorkspaceCreatorC(void) {
    return new dummyWorkspaceC;
}
// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

static dtkWidgetsWorkspace *WidgetsWorkspaceLog(void) {
    return new dtkWidgetsWorkspaceLog;
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class dummyWindowBar : public QFrame
{
    Q_OBJECT

public:
    dummyWindowBar(QWidget *parent = nullptr);

public:
    QSize sizeHint(void) const { return QSize(640, 480); }

public:
    QStackedWidget *stack;
};

dummyWindowBar::dummyWindowBar(QWidget *parent) : QFrame(parent)
{
    this->stack = new QStackedWidget(this);

    dtkWidgetsWorkspaceBar *bar = new dtkWidgetsWorkspaceBar(this);
    bar->setStack(this->stack);

    // bar->buildFromFactory();

    bar->addWorkspaceInMenu("Group 1", "Lorem Ipsum", "dummyWorkspaceA", "Dolor Sit Amet", "Workspace A");
    bar->addWorkspaceInMenu("Group 2", "Lorem Ipsum", "dummyWorkspaceB", "Dolor Sit Amet", "Workspace B");
    bar->addWorkspaceInMenu("Group 2", "Lorem Ipsum", "dummyWorkspaceC", "Dolor Sit Amet", "Workspace C");
    bar->addWorkspaceInMenu("Group 3", "Lorem Ipsum", "WidgetsWorkspaceLog", "Dolor Sit Amet", "Log Workspace");

    QVBoxLayout *l = new QVBoxLayout(this);
    l->setContentsMargins(0, 0, 0, 0);
    l->setSpacing(0);
    l->addWidget(stack);
    l->addWidget(bar);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    dtk::widgets::initialize();

    QApplication application(argc, argv);

    dtkFontAwesome::instance()->initFontAwesome();

    dtkThemesEngine::instance()->apply();

    dtk::widgets::workspace::pluginFactory().record("dummyWorkspaceA", dummyWorkspaceCreatorA);
    dtk::widgets::workspace::pluginFactory().record("dummyWorkspaceB", dummyWorkspaceCreatorB);
    dtk::widgets::workspace::pluginFactory().record("dummyWorkspaceC", dummyWorkspaceCreatorC);
    dtk::widgets::workspace::pluginFactory().record("WidgetsWorkspaceLog", WidgetsWorkspaceLog);

    dummyWindowBar *windowBar = new dummyWindowBar;
    windowBar->setWindowTitle("Multiple menus, multiple workspaces: one menu per workspace");
    windowBar->show();
    windowBar->raise();
    windowBar->move(windowBar->pos() + QPoint(50, 50));

    return application.exec();
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

#include "main.moc"

//
// main.cpp ends here
