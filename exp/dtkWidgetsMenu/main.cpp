// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>
#include <QtWidgets>

#include <dtkCore>
#include <dtkFonts>
#include <dtkThemes>
#include <dtkWidgets>


// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

void setup(dtkApplication *);

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    dtk::widgets::initialize("");

    dtkThemesEngine::instance()->apply(); // TODO

    dtkApplication *application = dtkApplication::create(argc, argv);
    application->setApplicationName("dtkWidgetsMenu");
    application->setOrganizationName("inria");
    application->setOrganizationDomain("fr");
    application->initialize();
    application->setup(setup);

#if 0

// ///////////////////////////////////////////////////////////////////
// Retrieve an arbitrary parameter
// ///////////////////////////////////////////////////////////////////

    if (dtkCoreParameter *parameter = application->window()->menubar()->parameter(":Model parameters 1:prec:"))
        qDebug() << 1 << "Oh yeah" << parameter;

    if (dtkCoreParameter *parameter = application->window()->menubar()->parameter(":Model parameters 1:Model parameters 1.1:prec:"))
        qDebug() << 2 << "Oh yeah" << parameter;

// ///////////////////////////////////////////////////////////////////
// Retrieve an arbitrary parameter widget
// ///////////////////////////////////////////////////////////////////

    if (dtkWidgetsParameter *parameter = application->window()->menubar()->parameterWidget(":Model parameters 1:prec:"))
        qDebug() << 3 << "Oh yeah" << parameter;

    if (dtkWidgetsParameter *parameter = application->window()->menubar()->parameterWidget(":Model parameters 1:Model parameters 1.1:prec:"))
        qDebug() << 4 << "Oh yeah" << parameter;

// ///////////////////////////////////////////////////////////////////

#endif

    dtkThemesEngine::instance()->apply(); // TODO

    return application->exec();
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

void setup(dtkApplication *application)
{
    QLabel *bg = new QLabel;
    bg->setPixmap(QPixmap(":/main_1.jpg"));

    QScrollArea *area = new QScrollArea(application->window());
    area->setWidget(bg);

    dtkWidgetsMenu *menu_1 = new dtkWidgetsMenu(fa::circlethin, "MainLevel 1");
    dtkWidgetsMenuItem *menuitem_11 = menu_1->addItem(fa::recycle, "Cycle through background");
    menu_1->addItem(fa::circleo, "SubLevel 1-2");
    menu_1->addItem(fa::circleo, "SubLevel 1-3");
    menu_1->addSeparator();
    menu_1->addItem(fa::circleo, "SubLevel 1-4");

    dtkWidgetsMenu *menu_2 = new dtkWidgetsMenu(fa::circlethin, "MainLevel 2");
    menu_2->addItem(fa::circleo, "SubLevel 2-1");
    dtkWidgetsMenu *menu_22 = menu_2->addMenu(fa::circleo, "SubLevel 2-2");
    menu_22->addItem(fa::fileo, "SubSubLevel1");
    menu_22->addItem(fa::fileo, "SubSubLevel2");

    // /////////////////////////////////////////////////////////////////////////////
    //
    // /////////////////////////////////////////////////////////////////////////////

    { // Sub level menu

    dtkWidgetsMenu *menu_23 = menu_2->addMenu(fa::circleo, "SubLevel 2-3");

    QPushButton *button = new QPushButton("Delete my parent menu");

    QObject::connect(button, &QPushButton::clicked, [=] ()
    {
        menu_23->deleteLater();
    });

    dtkWidgetsMenuItemDIY *item = new dtkWidgetsMenuItemDIY("Self destructing item for menu");
    item->addWidget(button);

    menu_23->addItem(item);

    }

    { // Sub level item

    dtkWidgetsMenuItem *item = menu_2->addItem(fa::gear, "Delete myself (as an item)");

    QObject::connect(item, &dtkWidgetsMenuItem::clicked, [=] () -> void
    {
        item->deleteLater();
    });

    }

    // /////////////////////////////////////////////////////////////////////////////

    menu_2->addItem(fa::circleo, "Sublevel 2-5");

    dtkWidgetsMenu *menu_3 = new dtkWidgetsMenu(fa::circlethin, "MainLevel 3");
    menu_3->addItem(fa::circleo, "Sublevel 3-1");
    menu_3->addItem(fa::circleo, "Sublevel 3-2");

    dtkWidgetsMenu *menu_4 = new dtkWidgetsMenu(fa::circlethin, "MainLevel 4");

    QString placeholder = "SubSub";

    dtkWidgetsMenu *m = menu_22->addMenu(fa::home, QString("%1Level3").arg(placeholder));

    for(int i = 0; i < 5; i++) {

        placeholder += "Sub";

        m->addItem(fa::home, QString("%1Level1").arg(placeholder));
        dtkWidgetsMenu *menu = m->addMenu(fa::home, QString("%1").arg(placeholder));
        m->addItem(fa::home, QString("%1Level3").arg(placeholder));
        m->addSeparator();
        m->addItem(fa::home, QString("%1Level4").arg(placeholder));

        m = menu;
    }

    QObject::connect(menuitem_11, &dtkWidgetsMenuItem::clicked, [=] (void) -> void
    {
        static int count = 1;

        bg->setPixmap(QPixmap(QString(":main_%1.jpg").arg((count++ % 5) + 1)));
    });

    // dtkWidgetsParameterMenuBarGenerator menubar_generator_1(":parameters_menu.json", ":parameters_definition.json");

    // dtkCoreParameters parameters_only;
    // dtk::d_int *int_parameter = new dtk::d_int("Int parameter", 1, 0, 2, "I am an int parameter, slightly limited by my bounds.");
    // parameters_only["int_parameter"] = int_parameter;

    // dtkWidgetsParameterMenuBarGenerator menubar_generator_2(":parameters_only_menu.json", parameters_only);
    // menubar_generator_2.populate(menu_4);

    dtkWidgetsMenuBar *menu_bar = application->window()->menubar();

    menu_bar->addMenu(menu_1);
    menu_bar->addMenu(menu_2);
    menu_bar->addMenu(menu_3);

    { // Top level menu

    dtkWidgetsMenu *menu = menu_bar->addMenu(fa::circleo, "Top level");

    QPushButton *button = new QPushButton("Delete my parent menu");

    QObject::connect(button, &QPushButton::clicked, [=] ()
    {
        menu->deleteLater();
    });

    dtkWidgetsMenuItemDIY *item = new dtkWidgetsMenuItemDIY("Self destructing item for menu");
    item->addWidget(button);

    menu->addItem(item);

    }

    // menu_bar->addMenu(menu_4);

    // menubar_generator_1.populate(menu_bar);

    // auto params = menubar_generator_1.parameters();

    application->window()->populate();

    application->window()->setCentralWidget(area);

    //menu_bar->touch();

    // params["checkbox_ro_toggle"]->connect( [=] (QVariant v) {
    //     bool visible = v.value<dtk::d_bool>();

    //     dtkWidgetsParameter *widget_slider = application->window()->menubar()->parameterWidget(":Read-only parameters:slider_double_ro_and_rw:");
    //     dtkWidgetsParameter *widget_filebrowser = application->window()->menubar()->parameterWidget(":Read-only parameters:file_browse_ro:");

    //     if(visible) {
    //         //params["slider_double_ro_and_rw"]->setReadOnly(false);
    //         widget_slider->setReadOnly(false);
    //         widget_filebrowser->setReadOnly(false);
    //         params["slider_double_ro_and_rw"]->setLabel("Slider READWRITE");
    //         params["checkbox_ro_toggle"]->setDocumentation("Next widget is READWRITE");
    //     } else {
    //         //params["slider_double_ro_and_rw"]->setReadOnly(true);
    //         widget_slider->setReadOnly(true);
    //         widget_filebrowser->setReadOnly(true);
    //         params["slider_double_ro_and_rw"]->setLabel("Slider READONLY");
    //         params["checkbox_ro_toggle"]->setDocumentation("Next widget is READONLY");
    //     }
    // });

    // QObject::connect(application->window()->menubar(), &dtkWidgetsMenuBar::entered, [=] (dtkWidgetsMenu *target) {
    //     qDebug() << "Entered:" << target->title();
    // });

    // QObject::connect(application->window()->menubar(), &dtkWidgetsMenuBar::left, [=] (dtkWidgetsMenu *source) {
    //     qDebug() << "Left:" << source->title();
    // });

    menu_bar->touch();
}

//
// main.cpp ends here
