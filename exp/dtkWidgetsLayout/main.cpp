// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include  <QtWidgets>
#include <dtkWidgets>
#include <dtkThemesEngine>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dummy : public QFrame
{
public:
    dummy(const QString& name, QWidget *parent = nullptr);

protected:
    void paintEvent(QPaintEvent *);

private:
    QString name;
};

dummy::dummy(const QString& name, QWidget *parent)
{
    this->name = name;
}

void dummy::paintEvent(QPaintEvent *event)
{
    int n = this->name.toInt();

    QPainter painter(this);

    switch(n) {
    case 1:
        painter.fillRect(event->rect(), Qt::red);
        break;
    case 2:
        painter.fillRect(event->rect(), Qt::green);
        break;
    case 3:
        painter.fillRect(event->rect(), Qt::blue);
        break;
    default:
        painter.fillRect(event->rect(), Qt::cyan);
        break;
    }

    QFontMetrics metrics(qApp->font());

    painter.setPen(Qt::white);
    painter.drawText(event->rect().width()/2 - metrics.horizontalAdvance(this->name)/2, event->rect().height()/2, this->name);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dummyWidget : public dtkWidgetsWidget
{
    Q_OBJECT

public:
    dummyWidget(const QString& name, QWidget *parent = nullptr);

public:
    QWidget *widget(void) override;

private:
    dummy *contents;
};

dummyWidget::dummyWidget(const QString& name, QWidget *parent) : dtkWidgetsWidget(parent)
{
    this->contents = new dummy(name, this);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    layout->addWidget(this->contents);

    this->setObjectName(name);
}

QWidget *dummyWidget::widget(void)
{
    return this->contents;
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dummyWindow : public QFrame
{
    Q_OBJECT

public:
    dummyWindow(QWidget *parent = nullptr);

public:
    static dtkWidgetsWidget *create(const QString&);

protected:
    void showEvent(QShowEvent *);
    void closeEvent(QCloseEvent *);

public:
    dtkWidgetsLayout *layout;
};

dummyWindow::dummyWindow(QWidget *parent) : QFrame(parent)
{
    dtkWidgetsLayoutItem::Actions actions;
    actions["1"] = "1";
    actions["2"] = "2";
    actions["3"] = "3";
    actions["4"] = "4";

    dtkWidgetsLayoutItem::setActions(actions);

    this->layout = new dtkWidgetsLayout(this);
    this->layout->setCreator(create);

    QVBoxLayout *l = new QVBoxLayout(this);
    l->setContentsMargins(0, 0, 0, 0);
    l->setSpacing(0);
    l->addWidget(this->layout);
}

dtkWidgetsWidget *dummyWindow::create(const QString& name)
{
    return new dummyWidget(name);
}

void dummyWindow::showEvent(QShowEvent *)
{
    dtkWidgetsLayoutReader reader;
    reader.setLayout(this->layout);
    reader.read();
}

void dummyWindow::closeEvent(QCloseEvent *)
{
    dtkWidgetsLayoutWriter writer;
    writer.setLayout(this->layout);
    writer.write();
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    QApplication application(argc, argv);

    dummyWindow *window = new dummyWindow;
    window->show();
    window->raise();

    dtkThemesEngine::instance()->apply();

    return application.exec();
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

#include "main.moc"

//
// main.cpp ends here
