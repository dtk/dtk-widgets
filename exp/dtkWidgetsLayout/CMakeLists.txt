### CMakeLists.txt ---
##

project(dtkWidgetsLayout)

## #################################################################
## Sources
## #################################################################

set(${PROJECT_NAME}_SOURCES main.cpp)

## #################################################################
## Build rules
## #################################################################

qt_add_executable(${PROJECT_NAME}
  ${${PROJECT_NAME}_SOURCES})

target_link_libraries(${PROJECT_NAME} PRIVATE Qt6::Core)
target_link_libraries(${PROJECT_NAME} PRIVATE Qt6::Widgets)
target_link_libraries(${PROJECT_NAME} PRIVATE dtk::Themes)
target_link_libraries(${PROJECT_NAME} PRIVATE dtk::Widgets)

######################################################################
### CMakeLists.txt ends here
