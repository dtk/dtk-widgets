// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>
#include <QtWidgets>

#include <dtkThemes>
#include <dtkWidgets>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

QString dtkReadFile(const QString& path)
{
    QFile file(path);

    if(!file.open(QIODevice::ReadOnly))
        return QString();

    QString contents = file.readAll();

    file.close();

    return contents;
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dummy : public QFrame
{
    Q_OBJECT

public:
     dummy(void);
    ~dummy(void) {}

protected:
    void resizeEvent(QResizeEvent *event);

private:
    dtkWidgetsOverlayPane *pane;
    dtkWidgetsOverlayPaneItem *item_1;
    dtkWidgetsOverlayPaneItem *item_2;
    dtkWidgetsOverlayPaneSlider *slide_1;

    dtkWidgetsHUD *hud;
};

dummy::dummy(void) : QFrame()
{
    QFrame *frame_1 = new QFrame(this);
    QFrame *frame_2 = new QFrame(this);
    QFrame *frame_3 = new QFrame(this);
    QFrame *frame_4 = new QFrame(this);
    QFrame *frame_5 = new QFrame(this);

    frame_1->setStyleSheet("background: #ff0000;"); frame_1->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    frame_2->setStyleSheet("background: #ffff00;"); frame_2->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    frame_3->setStyleSheet("background: #ffffff;"); frame_3->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    frame_4->setStyleSheet("background: #00ff00;"); frame_4->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    frame_5->setStyleSheet("background: #00ffff;"); frame_5->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    this->item_1 = new dtkWidgetsOverlayPaneItem(this);
    this->item_1->setTitle("Item 1");
    this->item_1->addWidget(frame_1);

    this->item_2 = new dtkWidgetsOverlayPaneItem(this);
    this->item_2->setTitle("Item 2");
    this->item_2->addWidget(frame_2);

    this->slide_1 = new dtkWidgetsOverlayPaneSlider(this);
    this->slide_1->setTitle("Slide 1");
    this->slide_1->addSlide(fa::dotcircleo, frame_3);
    this->slide_1->addSlide(fa::dotcircleo, frame_4);
    this->slide_1->addSlide(fa::dotcircleo, frame_5);

    this->pane = new dtkWidgetsOverlayPane(this);
    this->pane->addWidget(this->item_1);
    this->pane->addWidget(this->item_2);
    this->pane->addWidget(this->slide_1);

    this->hud = new dtkWidgetsHUD(this);
    connect(this->hud->addItem(fa::paw), SIGNAL(clicked()), this->pane, SLOT(toggle()));

    this->pane->toggle();
}

void dummy::resizeEvent(QResizeEvent *event)
{
    this->hud->resize(event->size());

    this->pane->setFixedHeight(event->size().height());
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    QApplication application(argc, argv);

    dtkThemesEngine::instance()->apply("NordLight");

    dummy *window = new dummy;
    window->resize(1024, 600);
    window->show();
    window->raise();
    window->setStyleSheet(dtkReadFile(":main.qss"));

    dtkThemesEngine::instance()->apply("NordLight");

    return application.exec();
}

// ///////////////////////////////////////////////////////////////////

#include "main.moc"

//
// main.cpp ends here
