// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsTest>

class dtkWidgetsParametersTestCase : public QObject
{
    Q_OBJECT

public:
    dtkWidgetsParametersTestCase(void);
    ~dtkWidgetsParametersTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testBasic(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    class dtkWidgetsParametersTestCasePrivate *d;
};

//
// dtkWidgetsParametersTest.h ends here
