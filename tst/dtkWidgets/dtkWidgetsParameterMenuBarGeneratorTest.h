// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsTest>

class dtkWidgetsParameterMenuBarGeneratorTestCase : public QObject
{
    Q_OBJECT

public:
    dtkWidgetsParameterMenuBarGeneratorTestCase(void);
    ~dtkWidgetsParameterMenuBarGeneratorTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testBasic(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    class dtkWidgetsParameterMenuBarGeneratorTestCasePrivate *d;
};

//
// dtkWidgetsParameterMenuBarGeneratorTest.h ends here
