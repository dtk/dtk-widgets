// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkWidgetsParameterMenuBarGeneratorTest.h"

#include <dtkWidgetsTest>

#include <dtkThemes>

#include <dtkWidgets/dtkWidgets>
#include <dtkWidgets/dtkWidgetsParameterMenuBarGenerator>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsParameterMenuBarGeneratorTestCasePrivate
{

};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkWidgetsParameterMenuBarGeneratorTestCase::dtkWidgetsParameterMenuBarGeneratorTestCase(void) : d(new dtkWidgetsParameterMenuBarGeneratorTestCasePrivate)
{
    dtk::widgets::setVerboseLoading(true);
    dtk::widgets::initialize("");
    dtkThemesEngine::instance()->apply();
}

dtkWidgetsParameterMenuBarGeneratorTestCase::~dtkWidgetsParameterMenuBarGeneratorTestCase(void)
{
    delete d;
}

void dtkWidgetsParameterMenuBarGeneratorTestCase::initTestCase(void)
{

}

void dtkWidgetsParameterMenuBarGeneratorTestCase::init(void)
{

}

void dtkWidgetsParameterMenuBarGeneratorTestCase::testBasic(void)
{
    QString nature_file_path  = QFINDTESTDATA("../resources/new_menu.json");
    QString definition_file_path  = QFINDTESTDATA("../resources/new_definition.json");

    dtkWidgetsParameterMenuBarGenerator menu_bar_generator(nature_file_path, definition_file_path);

    dtkCoreParameters params = menu_bar_generator.parameters();
    QCOMPARE(params.count() , 78);

    dtkWidgetsMenuBar bar;
    menu_bar_generator.populate(&bar);
    auto menus  = bar.menus();
    QCOMPARE(menus.count() , 4);
    QJsonObject json;
    for (auto menu: menus) {
        menu->exportParameters(json);
    }
    QJsonObject json2 = json["framerate"].toObject();
    int fps = json2["value"].toInt();
    QCOMPARE(fps, 10);
    json2["value"] = "42";
    json["framerate"] = json2;
    for (auto menu: menus) {
        menu->importParameters(json);
    }
    QJsonObject json3;
    for (auto menu: menus) {
        menu->exportParameters(json3);
    }
    fps = json3["framerate"].toObject()["value"].toInt();
    QCOMPARE(fps, 42);
}

void dtkWidgetsParameterMenuBarGeneratorTestCase::cleanup(void)
{

}

void dtkWidgetsParameterMenuBarGeneratorTestCase::cleanupTestCase(void)
{

}

DTKWIDGETSTEST_MAIN(dtkWidgetsParameterMenuBarGeneratorTest, dtkWidgetsParameterMenuBarGeneratorTestCase)

//
// dtkWidgetsParameterMenuBarGeneratorTest.cpp ends here
