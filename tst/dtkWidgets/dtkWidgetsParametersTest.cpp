// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkWidgetsParametersTest.h"

#include <dtkWidgetsTest>

#include <dtkCore/dtkCoreParameter>

#include <dtkWidgets/dtkWidgets>
#include <dtkWidgets/dtkWidgetsParameter>
#include <dtkWidgets/dtkWidgetsParameterFactory>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsParametersTestCasePrivate
{

};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkWidgetsParametersTestCase::dtkWidgetsParametersTestCase(void) : d(new dtkWidgetsParametersTestCasePrivate)
{
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel("trace");

    dtk::widgets::setVerboseLoading(true);
    dtk::widgets::initialize("");
}

dtkWidgetsParametersTestCase::~dtkWidgetsParametersTestCase(void)
{
    delete d;
}

void dtkWidgetsParametersTestCase::initTestCase(void)
{

}

void dtkWidgetsParametersTestCase::init(void)
{

}

void dtkWidgetsParametersTestCase::testBasic(void)
{
    {
        //////////////////// dtkWidgetsParameterBoolCheckBox ////////////////////

        dtk::d_bool bool_parameter = false;

        dtkWidgetsParameter *bool_check_box = dtk::widgets::parameters::pluginFactory().create(&bool_parameter, "dtkWidgetsParameterBoolCheckBox");

        QVERIFY(bool_check_box);
        QVERIFY(bool_check_box->parameter() == &bool_parameter);

        //////////////////// dtkWidgetsParameterDoubleSpinBox ////////////////////

        dtk::d_real real_parameter = 0.;

        dtkWidgetsParameter *real_spin_box = dtk::widgets::parameters::pluginFactory().create(&real_parameter, "dtkWidgetsParameterDoubleSpinBox");

        QVERIFY(real_spin_box);
        QVERIFY(real_spin_box->parameter() == &real_parameter);

        //////////////////// dtkWidgetsParameterIntSpinBox ////////////////////

        dtk::d_int int_parameter = 0.;

        dtkWidgetsParameter *int_spin_box = dtk::widgets::parameters::pluginFactory().create(&int_parameter, "dtkWidgetsParameterIntSpinBox");

        QVERIFY(int_spin_box);
        QVERIFY(int_spin_box->parameter() == &int_parameter);

        dtk::d_real wrong_type_parameter = 0.;

        dtkWidgetsParameter *wrong_type_parameter_spin_box = dtk::widgets::parameters::pluginFactory().create(&wrong_type_parameter, "dtkWidgetsParameterIntSpinBox");

        QVERIFY(wrong_type_parameter_spin_box);
        QVERIFY(wrong_type_parameter_spin_box->parameter() == nullptr);

        //////////////////// dtkWidgetsParameterStringListComboBox ////////////////////

        QList<QString> available_strings;
        available_strings.append("FirstString");
        available_strings.append("SecondString");
        available_strings.append("ThirdString");

        dtk::d_inliststring inliststring_parameter("listofstrings", available_strings);
        inliststring_parameter.setDocumentation("A inliststring parameter.");
        dtkWidgetsParameter *inliststring_combo_box = dtk::widgets::parameters::pluginFactory().create(&inliststring_parameter, "dtkWidgetsParameterStringListComboBox");

        QVERIFY(inliststring_combo_box);
        QVERIFY(inliststring_combo_box->parameter() == &inliststring_parameter);

    }

    {
        //////////////////// dtkWidgetsParameterBoolCheckBox ////////////////////

        dtk::d_bool bool_parameter = false;

        dtkWidgetsParameter *bool_check_box = dtk::widgets::parameters::pluginFactory().create("dtkWidgetsParameterBoolCheckBox");

        QVERIFY(bool_check_box);
        QVERIFY(bool_check_box->connect(&bool_parameter));
        QVERIFY(bool_check_box->parameter() == &bool_parameter);

        //////////////////// dtkWidgetsParameterDoubleSpinBox ////////////////////

        dtk::d_real real_parameter = 0.;

        dtkWidgetsParameter *spin_box = dtk::widgets::parameters::pluginFactory().create("dtkWidgetsParameterDoubleSpinBox");

        QVERIFY(spin_box);
        QVERIFY(spin_box->connect(&real_parameter));
        QVERIFY(spin_box->parameter());

        //////////////////// dtkWidgetsParameterIntSpinBox ////////////////////

        dtk::d_int int_parameter = 0.;

        dtkWidgetsParameter *int_spin_box = dtk::widgets::parameters::pluginFactory().create("dtkWidgetsParameterIntSpinBox");

        QVERIFY(int_spin_box);
        QVERIFY(int_spin_box->connect(&int_parameter));
        QVERIFY(int_spin_box->parameter());

        dtk::d_real wrong_type_parameter = 0.;

        dtkWidgetsParameter *wrong_type_parameter_spin_box = dtk::widgets::parameters::pluginFactory().create("dtkWidgetsParameterIntSpinBox");

        QVERIFY(wrong_type_parameter_spin_box);
        QVERIFY(!wrong_type_parameter_spin_box->connect(&wrong_type_parameter));
        QVERIFY(wrong_type_parameter_spin_box->parameter() == nullptr);

        //////////////////// dtkWidgetsParameterStringListComboBox ////////////////////

        QList<QString> available_strings;
        available_strings.append("FirstString");
        available_strings.append("SecondString");
        available_strings.append("ThirdString");

        dtk::d_inliststring inliststring_parameter("listofstrings", available_strings);
        inliststring_parameter.setDocumentation("A inliststring parameter.");
        dtkWidgetsParameter *inliststring_combo_box = dtk::widgets::parameters::pluginFactory().create("dtkWidgetsParameterStringListComboBox");

        QVERIFY(inliststring_combo_box);
        QVERIFY(inliststring_combo_box->connect(&inliststring_parameter));
        QVERIFY(inliststring_combo_box->parameter() == &inliststring_parameter);
    }

}

void dtkWidgetsParametersTestCase::cleanup(void)
{

}

void dtkWidgetsParametersTestCase::cleanupTestCase(void)
{

}

DTKWIDGETSTEST_MAIN(dtkWidgetsParametersTest, dtkWidgetsParametersTestCase)

//
// dtkWidgetsParametersTest.cpp ends here
