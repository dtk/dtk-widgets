#pragma once

#include <QtTest>

#define DTKWIDGETSTEST_MAIN(TestMain, TestObject)   \
    int TestMain(int argc, char *argv[])            \
    {                                               \
        QApplication app(argc, argv);               \
        TestObject tc;                              \
        return QTest::qExec(&tc, argc, argv);       \
    }

#define DTKWIDGETSTEST_MAIN_GUI(TestMain, TestObject) \
    int TestMain(int argc, char *argv[])              \
    {                                                 \
        QGuiApplication app(argc, argv);              \
        TestObject tc;                                \
        return QTest::qExec(&tc, argc, argv);         \
    }

#define DTKWIDGETSTEST_MAIN_NOGUI(TestMain, TestObject)	\
    int TestMain(int argc, char *argv[]) {              \
        QCoreApplication app(argc, argv);               \
        TestObject tc;                                  \
        return QTest::qExec(&tc, argc, argv);           \
    }
