// Version: $Id: a3997e6b1600ea62b311cddb3f0a5c2e90d61b5b $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <QtWidgets/QApplication>

class dtkWidgetsMainWindow;
class QCommandLineParser;
class QSettings;

class DTKWIDGETS_EXPORT dtkApplication : public QApplication
{
public:
    typedef void (*setup_t)(dtkApplication *);

public:
     dtkApplication(int& argc, char **argv);
    ~dtkApplication(void);

public:
    virtual void initialize(void);
    virtual void setup(setup_t);

public:
    virtual bool noGui(void);

public:
    static dtkApplication *create(int& argc, char *argv[]);

public:
    static void setWindow(dtkWidgetsMainWindow *);
    static dtkWidgetsMainWindow *window(void);

public:
    static int exec(void);

public:
    QCommandLineParser *parser(void);

public:
    QSettings *settings(void);

protected:
    class dtkApplicationPrivate *d;
};

#define dtkApp (static_cast<dtkApplication *>(qApp))

//
// dtkApplication.h ends here
