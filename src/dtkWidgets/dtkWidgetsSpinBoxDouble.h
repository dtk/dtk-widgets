// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:
#pragma once

#include <dtkWidgetsExport>

#include <QtWidgets/QAbstractSpinBox>

#include "dtkWidgetsBaseSpinBox.h"

// ///////////////////////////////////////////////////////////////////
// dtkWidgetSpinBoxDouble
// ///////////////////////////////////////////////////////////////////
//
// holds both standard and scienti
// . is the separator and does not depend on locale()
//
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsSpinBoxDouble : public dtkWidgetsBaseSpinBox
{
    Q_OBJECT

public:
     dtkWidgetsSpinBoxDouble(QDoubleValidator::Notation, QWidget * parent = 0);
    ~dtkWidgetsSpinBoxDouble(void);

signals:
    void valueChanged(double);

public:
    void stepBy(int) override;
    void stepDown(void);
    void stepUp(void);

    void setMinimum(double);
    void setMaximum(double);
    void setValue(double);
    void setDecimals(int);

private:
    class dtkWidgetsSpinBoxDoublePrivate *d;
};

//
// dtkWidgetsSpinBoxDouble.h ends here
