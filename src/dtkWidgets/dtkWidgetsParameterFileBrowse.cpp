// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsParameterFileBrowse.h"

#include <QtWidgets>
#include <QStringBuilder>

dtkWidgetsParameterFileBrowse::dtkWidgetsParameterFileBrowse(QWidget* parent) : dtkWidgetsParameterBrowse(parent)
{}

QString dtkWidgetsParameterFileBrowse::browse(void) {
    return QFileDialog::getOpenFileName(this, "Open file", dtkWidgetsParameterBrowse::m_parameter->dirName(), dtkWidgetsParameterBrowse::m_parameter->filters().join(";;"));
}

//
// dtkWidgetsParameterFileBrowse.cpp ends here
