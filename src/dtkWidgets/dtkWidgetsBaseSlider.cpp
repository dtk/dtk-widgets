// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsBaseSlider.h"

#include <QtWidgets>

dtkWidgetsBaseSlider::dtkWidgetsBaseSlider(QWidget * parent)
{
    this->setLocale(QLocale::C);
    this->setNoScrollFocusFilter();
}

dtkWidgetsBaseSlider::~ dtkWidgetsBaseSlider()
{
    
}

void dtkWidgetsBaseSlider::setNoScrollFocusFilter() 
{
    this->installEventFilter(this); // filter scroll events (see below)
}


// shameless ripoff https://stackoverflow.com/questions/5821802/qspinbox-inside-a-qscrollarea-how-to-prevent-spin-box-from-stealing-focus-when
// Forbid scroll events to reach spinbox when said spinbox has no focus (allow to scroll the pane without scrolling the spinbox if you didn't click on it)
bool dtkWidgetsBaseSlider::eventFilter(QObject *o, QEvent *e)
{
    if(e->type() == QEvent::Wheel &&
       qobject_cast<QSlider*>(o))
    {
        if(qobject_cast<QSlider*>(o)->focusPolicy() >= Qt::WheelFocus)
        {
            e->accept();
            return false;
        }
        else
        {
            e->ignore();
            return true;
        }
    }
    return QWidget::eventFilter(o, e);
}
