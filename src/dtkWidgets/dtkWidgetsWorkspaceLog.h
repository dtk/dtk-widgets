// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport.h>

#include <dtkWidgetsWorkspace.h>

class DTKWIDGETS_EXPORT dtkWidgetsWorkspaceLog : public dtkWidgetsWorkspace
{
    Q_OBJECT

public:
     dtkWidgetsWorkspaceLog(QWidget *parent = nullptr);
    ~dtkWidgetsWorkspaceLog(void);

public:
    void enter(void) override;
    void leave(void) override;

public slots:
    void apply(void) override;

public:
    static const QColor color;

private:
    class dtkWidgetsWorkspaceLogPrivate *d;
};

//
// dtkWidgetsWorkspaceLog.h ends here
