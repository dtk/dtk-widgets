// Version: $Id: 3a705ab307cb9b8b26c0b42ef257d101556de70f $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsController.h"
#include "dtkWidgetsList.h"
#include "dtkWidgetsWidget.h"

#include <QtCore/QMimeData>

#include <QtWidgets/QListWidgetItem>

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsListPrivate
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsListPrivate
{
public:
    QList<QWidget *> connected_views;
};

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsList
// ///////////////////////////////////////////////////////////////////

dtkWidgetsList::dtkWidgetsList(QWidget *parent) : QListWidget(parent), d(new dtkWidgetsListPrivate)
{
    this->setAttribute(Qt::WA_MacShowFocusRect, false);
    this->setDragEnabled(true);
    this->setFrameStyle(QFrame::NoFrame);
    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    connect(dtkWidgetsController::instance(), SIGNAL(inserted(dtkWidgetsWidget *, const QString&)), this, SLOT(update()));

    this->update();
}

dtkWidgetsList::~dtkWidgetsList(void)
{
    delete d;

    d = nullptr;
}

void dtkWidgetsList::update(void)
{
    this->clear();

    for (QString objectName : dtkWidgetsController::instance()->viewNames()) {
        QWidget *view = dtkWidgetsController::instance()->view(objectName);
        QListWidgetItem *item = new QListWidgetItem;
        QString text = objectName;
        item->setText(text);
        this->addItem(item);

        if(!d->connected_views.contains(view)) {
            d->connected_views.append(view);
        }
    }
}

void dtkWidgetsList::clear(void)
{
    QListWidget::clear();
}

QMimeData *dtkWidgetsList::mimeData(const QList<QListWidgetItem *>& items) const
{
    QMimeData *mimeData = new QMimeData;
    mimeData->setText(items.first()->text());

    return mimeData;
}

QStringList dtkWidgetsList::mimeTypes(void) const
{
    return QStringList() << "text/plain";
}

//
// dtkWidgetsList.cpp ends here
