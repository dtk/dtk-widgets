#include "dtkWidgetsParameterListStringListCheckBox.h"

#include <QtWidgets>

class dtkWidgetsParameterListStringListCheckBoxPrivate
{
public:
    QVBoxLayout *layout = nullptr;
    QMap<QString, QCheckBox *> checkbox_value;
};



// ///////////////////////////////////////////////////////////////////
// dtkWidgetsParameterListStringListCheckBox implementation
// ///////////////////////////////////////////////////////////////////

dtkWidgetsParameterListStringListCheckBox::dtkWidgetsParameterListStringListCheckBox(QWidget* parent) : dtkWidgetsParameterBase<dtk::d_inliststringlist>(parent) , d(new dtkWidgetsParameterListStringListCheckBoxPrivate)
{
    d->layout = new QVBoxLayout(this);
    d->layout->setContentsMargins(0, 0, 0, 0);
    d->layout->setSpacing(0);
}

dtkWidgetsParameterListStringListCheckBox::~dtkWidgetsParameterListStringListCheckBox(void)
{
    delete d;
}

void dtkWidgetsParameterListStringListCheckBox::setReadOnly(bool val)
{
    this->dtkWidgetsParameter::m_readonly = val;
    this->setEnabled(!this->dtkWidgetsParameter::m_readonly);
}

bool dtkWidgetsParameterListStringListCheckBox::connect(dtkCoreParameter *p)
{
    if (!p) {
        qWarning() << Q_FUNC_INFO << "The input parameter is null. Nothing is done.";
        return false;
    }

    m_parameter = dynamic_cast<dtk::d_inliststringlist *>(p);

    if(!m_parameter) {
        qWarning() << Q_FUNC_INFO << "The type of the parameter is not compatible with the widget dtkWidgetsParameterListStringListCheckBox.";
        return false;
    }

    this->setToolTip(m_parameter->documentation());

    for (const auto& v : m_parameter->list()) {
        d->checkbox_value[v] = new QCheckBox(v);
        if (m_parameter->value().contains(v)) {
            d->checkbox_value[v]->setCheckState(Qt::Checked);
        } else {
            d->checkbox_value[v]->setCheckState(Qt::Unchecked);
        }
        d->layout->addWidget(d->checkbox_value[v]);
        QObject::connect(d->checkbox_value[v], &QCheckBox::stateChanged, [=,this] () {
            QStringList value;
            auto it = d->checkbox_value.constBegin();
            while(it != d->checkbox_value.constEnd()) {
                if(it.value()->isChecked()) value.append(it.key());
                ++it;
            }
            m_parameter->shareValue(value);
        });
    }

    m_default  = m_parameter->variant();


    m_c = m_parameter->connect([=,this] (QVariant v) {
        if(d->checkbox_value.count() != m_parameter->list().count()) {
            for(auto * v : d->checkbox_value) {
                d->layout->removeWidget(v);
            }
            d->checkbox_value.clear(); // deleteall ??

            for (const auto& v : m_parameter->list()) {
                d->checkbox_value[v] = new QCheckBox(v);
                d->layout->addWidget(d->checkbox_value[v]);
                QObject::connect(d->checkbox_value[v], &QCheckBox::stateChanged, [=,this] () {
                    QStringList value;
                    auto it = d->checkbox_value.constBegin();
                    while(it != d->checkbox_value.constEnd()) {
                        if(it.value()->isChecked()) value.append(it.key());
                        ++it;
                    }

                    m_parameter->shareValue(value);
                });
            }
        }

        for (const auto& v : m_parameter->list()) {
            if (m_parameter->value().contains(v)) {
                d->checkbox_value[v]->setCheckState(Qt::Checked);
            } else {
                d->checkbox_value[v]->setCheckState(Qt::Unchecked);
            }
        }
    });

    return true;
}

//
// dtkWidgetsParameterListStringListCheckBox.cpp ends here
