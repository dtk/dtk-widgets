// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkWidgets"

#include <QtCore>
#include <QtWidgets>

class dtkWidgetsDataManager;
class dtkWidgetsDataManagerItem;
class dtkWidgetsDataManagerFocus;

class dtkWidgetsDataManagerPrivate : public QScrollArea
{
public:
    enum State {
        Collapsed,
        Expanded
    };

public:
     dtkWidgetsDataManagerPrivate(QWidget *parent = nullptr);
    ~dtkWidgetsDataManagerPrivate(void);

public:
    QSize sizeHint(void) const;

public:
    dtkWidgetsDataManagerItem *create(dtkWidgetsDataManagerDataPtr, const QColor&, const QImage &img = QImage());

public:
    QHash<dtkWidgetsDataManagerItem *, dtkWidgetsDataManagerDataPtr> data_series;

public:
    dtkWidgetsDataManager *q;

public:
    dtkWidgetsDataManagerFocus *focus_item = nullptr;

public:
    static int item_counter;

public:
    bool inside = false;

public:
    QWidget *contents;

public:
    State state = Collapsed;
};

//
// dtkWidgetsDataManager_p.h ends here
