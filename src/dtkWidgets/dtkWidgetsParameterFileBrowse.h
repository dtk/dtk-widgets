// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include "dtkWidgetsParameterBrowse.h"

class  DTKWIDGETS_EXPORT dtkWidgetsParameterFileBrowse : public dtkWidgetsParameterBrowse
{
    Q_OBJECT

public:
     dtkWidgetsParameterFileBrowse(QWidget* parent = nullptr);

private:
    QString browse(void) override;
};

// ///////////////////////////////////////////////////////////////////

inline dtkWidgetsParameter *dtkWidgetsParameterFileBrowseCreator(void)
{
    return new dtkWidgetsParameterFileBrowse();
}
//
// dtkWidgetsParameterFileBrowse.h ends here
