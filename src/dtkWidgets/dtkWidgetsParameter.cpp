// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsParameter.h"
#include <dtkCore/dtkCoreParameter.h>

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsAbstractParameter implementation
// ///////////////////////////////////////////////////////////////////

dtkWidgetsParameter::dtkWidgetsParameter(QWidget *p) : QWidget(p)
{

}

bool dtkWidgetsParameter::isReadOnly(void)
{
    return m_readonly;
}

void dtkWidgetsParameter::setReadOnly(bool)
{

};

void dtkWidgetsParameter::reset(void)
{
    this->parameter()->shareValue(m_default);
}

void dtkWidgetsParameter::setAdvanced(bool adv)
{
    m_advanced = adv;
    emit advancedChanged(m_advanced);
}

bool dtkWidgetsParameter::advanced(void)
{
    return m_advanced;
}

//
// dtkWidgetsParameter.cpp ends here
