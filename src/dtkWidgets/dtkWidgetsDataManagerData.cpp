// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsDataManagerData.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

QWidget *dtkWidgetsDataManagerDataCreate(const QVariant& v)
{
    QLabel *label = new QLabel;

    if(v.typeId() == QMetaType::Int)
        label->setText(QString::number(v.toInt()));

    if(v.typeId() == QMetaType::Double)
        label->setText(QString::number(v.toDouble()));

    if(v.typeId() == QMetaType::QString)
        label->setText(v.toString());

    if(v.typeId() == QMetaType::QColor) {

        QPixmap pixmap(16, 16);

        QPainter painter(&pixmap);
        painter.setRenderHint(QPainter::Antialiasing, true);
        painter.setPen(Qt::white);
        painter.setBrush(v.value<QColor>());
        painter.drawRoundedRect(0, 0, 16, 16, 8, 8);

        label->setPixmap(pixmap);
    }

    return label;
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsDataManagerDataPrivate : public QScrollArea
{
public:
     dtkWidgetsDataManagerDataPrivate(dtkWidgetsDataManagerData *data);
    ~dtkWidgetsDataManagerDataPrivate(void);

public:
    QWidget *contents;
};

dtkWidgetsDataManagerDataPrivate::dtkWidgetsDataManagerDataPrivate(dtkWidgetsDataManagerData *data) : QScrollArea(0)
{
    QFormLayout *layout = new QFormLayout;

    for(QString key : data->metadata.keys())
        layout->addRow(key, dtkWidgetsDataManagerDataCreate(data->metadata.value(key)));

    this->contents = new QWidget(this);
    this->contents->setLayout(layout);

    this->setWidget(this->contents);
    this->setWidgetResizable(true);
}

dtkWidgetsDataManagerDataPrivate::~dtkWidgetsDataManagerDataPrivate(void)
{

}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkWidgetsDataManagerData::dtkWidgetsDataManagerData(QObject *parent) : QObject(parent)
{

}

dtkWidgetsDataManagerData::~dtkWidgetsDataManagerData(void)
{

}

const QString dtkWidgetsDataManagerData::ICON = "icon";

//
// dtkWidgetsDataManagerData.cpp ends here
