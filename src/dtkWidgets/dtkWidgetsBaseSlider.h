// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <QtWidgets/QSlider>

class dtkWidgetsBaseSlider : public QSlider
{
    Q_OBJECT

public:
     dtkWidgetsBaseSlider(QWidget * parent = nullptr);
    ~dtkWidgetsBaseSlider(void);
    

protected:
    bool eventFilter(QObject *o, QEvent *e) override;
    void setNoScrollFocusFilter();

};


