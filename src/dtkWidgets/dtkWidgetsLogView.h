// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <QtWidgets/QWidget>

class DTKWIDGETS_EXPORT dtkWidgetsLogView : public QWidget
{
    Q_OBJECT

public:
     dtkWidgetsLogView(QWidget *parent = 0);
    ~dtkWidgetsLogView(void);

public slots:
    void displayTrace(bool display);
    void displayDebug(bool display);
    void displayInfo(bool display);
    void displayWarn(bool display);
    void displayError(bool display);
    void displayFatal(bool display);

protected slots:
    void autoScrollChecked(int state);
    void disableAutoScroll(void);
    void enableAutoScroll(void);

private:
    class dtkWidgetsLogViewPrivate *d;
};

//
// dtkWidgetsLogView.h ends here
