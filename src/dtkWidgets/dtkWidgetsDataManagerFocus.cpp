// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsDataManagerFocus.h"
#include "dtkWidgetsDataManagerItem.h"

class dtkWidgetsDataManagerFocusPrivate {
public:
    QHBoxLayout *layout = nullptr;
    QLabel *label = nullptr;

    QStandardItemModel *model = nullptr;
    QTableView *metadata_table = nullptr;

public:
    dtkWidgetsDataManagerItem *previous_item = nullptr;
};

dtkWidgetsDataManagerFocus::dtkWidgetsDataManagerFocus(QWidget *parent) : QWidget(parent)
{
    d = new dtkWidgetsDataManagerFocusPrivate();
    d->label = new QLabel(this);

    d->model = new QStandardItemModel;
    d->metadata_table = new QTableView(this);
    d->metadata_table->setSortingEnabled(true);
    d->metadata_table->setShowGrid(false);
    d->metadata_table->horizontalHeader()->hide();
    d->metadata_table->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    d->metadata_table->verticalHeader()->hide();
    d->metadata_table->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    d->metadata_table->setModel(d->model);

    d->layout = new QHBoxLayout(this);
    d->layout->setContentsMargins(0, 0, 0, 0);
    d->layout->setSpacing(0);

    d->layout->addWidget(d->label);
    d->layout->addWidget(d->metadata_table);
    d->metadata_table->hide();
}

dtkWidgetsDataManagerFocus::~dtkWidgetsDataManagerFocus(void)
{
    delete d;
}

void dtkWidgetsDataManagerFocus::setItem(dtkWidgetsDataManagerItem *item, const QVariantMap& metadata)
{
    if(item == d->previous_item) {
        return; // item already set, nothing to do
    }
    d->previous_item = item;

    this->presented = false; // new item so not yet presented
    this->hideMetaData();

    d->label->setPixmap(item->pixmap());

    d->model->clear();
    int row = 0;
    for (auto it = metadata.begin(); it != metadata.end(); ++it, ++row) {
        auto key = new QStandardItem(it.key());
        auto value = new QStandardItem(it.value().toString());
        d->model->setItem(row, 0, key);
        d->model->setItem(row, 1, value);
    }

    d->metadata_table->updateGeometry();
}

void dtkWidgetsDataManagerFocus::scalePixmap(int w, int h)
{
    d->label->pixmap().scaled(w, h);
}

void dtkWidgetsDataManagerFocus::hideMetaData(void)
{
    d->metadata_table->hide();
    //d->layout->removeWidget(d->metadata_table);
}

void  dtkWidgetsDataManagerFocus::showMetadata(void)
{
    d->metadata_table->show();
}

void dtkWidgetsDataManagerFocus::mouseReleaseEvent(QMouseEvent *ev) {
    QWidget::mouseReleaseEvent(ev);
    emit clicked();
}

//
// dtkWidgetsDataManagerFocus.cpp ends here
