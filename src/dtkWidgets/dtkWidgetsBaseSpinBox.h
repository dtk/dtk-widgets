// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <QtWidgets/QAbstractSpinBox>

class dtkWidgetsBaseSpinBox : public QAbstractSpinBox
{
    Q_OBJECT

public:
     dtkWidgetsBaseSpinBox(QWidget * parent = nullptr);
    ~dtkWidgetsBaseSpinBox(void);


protected:
    bool eventFilter(QObject *o, QEvent *e) override;
    void focusInEvent(QFocusEvent*) override;
    void focusOutEvent(QFocusEvent*) override;
    void setNoScrollFocusFilter();

protected:
    QAbstractSpinBox::StepEnabled m_step_enabled;
    QAbstractSpinBox::StepEnabled stepEnabled(void) const override;
public:
    void setStepEnabled(bool);
};
