// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsWorkspaceLog.h"

#include <dtkThemes>
#include <dtkWidgets>

class dtkWidgetsWorkspaceLogPrivate
{
public:
    dtkWidgetsMenu *menu = nullptr;

public:
    dtkWidgetsLayout *layout = nullptr;

};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkWidgetsWorkspaceLog::dtkWidgetsWorkspaceLog(QWidget *parent) : dtkWidgetsWorkspace(parent)
{
    d = new dtkWidgetsWorkspaceLogPrivate;
    dtkWidgetsLogView *logView = new dtkWidgetsLogView(dtkApp->window());

    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 16, 0, 0);
    layout->setSpacing(0);
    layout->addWidget(logView);  

    dtkWidgetsMenuBar *menubar = dtkApp->window()->menubar();
    dtkWidgetsParameterMenuBarGenerator menubar_generator(":/dtk-widgets/log_menu.json", ":/dtk-widgets/log_params.json");
    menubar_generator.populate(menubar);
    dtkApp->window()->populate();

    dtkCoreParameters parameters = menubar_generator.parameters();
    d->menu = menubar->menu("Log");

    // get actual loglevel value (typically from the command line)
    parameters["log/level"]->setValue(dtkLogger::instance().levelString().toLower());

    parameters["log/level"]->connect([=] (QVariant v) {
        QString input = v.value<dtk::d_inliststring>().value();
        dtkLogger::instance().setLevel(input);
        }
      );
    menubar->touch();
}

dtkWidgetsWorkspaceLog::~dtkWidgetsWorkspaceLog(void)
{
    delete d;
}

void dtkWidgetsWorkspaceLog::enter(void)
{
    dtkWidgetsMenuBar *menubar = dtkApp->window()->menubar();

    menubar->insertMenu(0, d->menu);
    menubar->touch();
    menubar->reset();
}

void dtkWidgetsWorkspaceLog::leave(void)
{
    dtkWidgetsMenuBar *menubar = dtkApp->window()->menubar();
    menubar->removeMenu(d->menu);
}

void dtkWidgetsWorkspaceLog::apply(void){}

// ///////////////////////////////////////////////////////////////////

const QColor dtkWidgetsWorkspaceLog::color = QColor("#a38948");


//
// dtkWidgetsWorkspaceLog.cpp ends here
