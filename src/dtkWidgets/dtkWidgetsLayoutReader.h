// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <QtCore>

class dtkWidgetsLayout;

class QDomNode;

class DTKWIDGETS_EXPORT dtkWidgetsLayoutReader
{
public:
     dtkWidgetsLayoutReader(void);
     virtual ~dtkWidgetsLayoutReader(void);

public:
    void setLayout(dtkWidgetsLayout *);
    dtkWidgetsLayout *layout(void);

public:
    void read(const QString &filename = QString());

public:
    virtual void readNode(const QDomNode&);

private:
    class dtkWidgetsLayoutReaderPrivate *d;
};

//
// dtkWidgetsLayoutReader.h ends here
