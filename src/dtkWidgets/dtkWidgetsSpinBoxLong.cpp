// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsSpinBoxLong.h"

#include <QtWidgets>

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsSpinBoxLongPrivate
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsSpinBoxLongPrivate : public QObject
{
    Q_OBJECT

public slots:
    void update();
    void touch();

public:
    long long value;
    long long minimum;
    long long maximum;
    QValidator *validator;

public:
    QString textFromValue ( long long ) const;
    long long  valueFromText ( const QString& ) const;
    QValidator::State validate(QString&, int&) const;

public:
    dtkWidgetsSpinBoxLong *q;
};

void dtkWidgetsSpinBoxLongPrivate::update(void)
{
    this->value = valueFromText( q->text());
    this->touch();
}

void dtkWidgetsSpinBoxLongPrivate::touch()
{
    q->emit valueChanged(this->value);
}

QString dtkWidgetsSpinBoxLongPrivate::textFromValue ( long long value ) const
{
    QString strValue;

    if (value < this->minimum)
        value = this->minimum;

    if (value > this->maximum)
        value = this->maximum;

    return QString::number(value);
}

long long dtkWidgetsSpinBoxLongPrivate::valueFromText ( const QString & text ) const
{
    long long val = text.toLongLong();

    if (val < this->minimum)
        val = this->minimum;
    if (val > this->maximum)
        val = this->maximum;

    q->setValue(val);
    return val;
}

QValidator::State dtkWidgetsSpinBoxLongPrivate::validate(QString &text, int &pos) const
{
    // check user input
    bool input_check;
    text.toLongLong(&input_check);

    return (input_check ? QValidator::Acceptable : QValidator::Invalid);
}


///

dtkWidgetsSpinBoxLong::dtkWidgetsSpinBoxLong(QWidget *parent) : dtkWidgetsBaseSpinBox(parent), d(new dtkWidgetsSpinBoxLongPrivate)
{
    d->q = this;


    d->value = 0.0;

    QRegularExpression rx("-?\\d{1,18}");
    d->validator = new QRegularExpressionValidator(rx, this);

    d->maximum = std::numeric_limits<long long>::max();
    d->minimum = std::numeric_limits<long long>::lowest();

    this->lineEdit()->setValidator(d->validator);

    connect(this,
            SIGNAL(editingFinished()),
            d,
            SLOT(update()));

}

dtkWidgetsSpinBoxLong::~dtkWidgetsSpinBoxLong(void)
{
    delete d->validator;
    delete d;
}

void dtkWidgetsSpinBoxLong::stepBy(int step)
{
    if (step == 0 ) {
        return;
    }
    if (step < 0) {
        int i = step;
        while ( i < 0 ) {
            stepDown();
            i++;
        }
    }
    else  {
        int i = step;
        while ( i > 0) {
            stepUp();
            i--;
        }
    }
}

void dtkWidgetsSpinBoxLong::stepDown(void)
{
    if(d->value > d->minimum) {
        --d->value;
        d->touch();
    }
}

void dtkWidgetsSpinBoxLong::stepUp(void)
{
    if(d->value < d->maximum) {
        ++d->value;
        d->touch();
    }
}


void dtkWidgetsSpinBoxLong::setMinimum(long long min)
{
    d->minimum = min;
}

void dtkWidgetsSpinBoxLong::setMaximum(long long max)
{
    d->maximum = max;
}

void dtkWidgetsSpinBoxLong::setValue(long long val)
{
    d->value = val;
    this->lineEdit()->blockSignals(true);
    this->lineEdit()->setText(d->textFromValue(val));
    this->lineEdit()->blockSignals(false);
}



#include "dtkWidgetsSpinBoxLong.moc"

//
// dtkWidgetsSpinBoxLong.h ends here
