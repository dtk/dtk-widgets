// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>
#include <dtkWidgetsDataManagerData.h>

#include <QtCore>
#include <QtWidgets>

class dtkWidgetsDataManagerItem;

class DTKWIDGETS_EXPORT dtkWidgetsDataManager : public QFrame
{
    Q_OBJECT

public:
    static dtkWidgetsDataManager *instance(void);

signals:
    void shrink(void);
    void expand(void);

protected:
    void itemClicked(dtkWidgetsDataManagerItem *);

signals:
    void itemClickedSignal(dtkWidgetsDataManagerItem *);

public:
    QSize sizeHint(void) const override;

public slots:
    int addData(dtkWidgetsDataManagerDataPtr, const QColor &, const QImage& img = QImage());

public slots:
    void present(dtkWidgetsDataManagerItem *);

public slots:
    QVector<int> getDataIds(void);

public:
    dtkWidgetsDataManagerDataPtr get(int index);
    dtkWidgetsDataManagerItem *getItem(int index);

public:
    QPixmap thumbnail(int index);

protected:
     dtkWidgetsDataManager(QWidget *parent = nullptr);
    ~dtkWidgetsDataManager(void);

protected:
    void enterEvent(QEnterEvent *) override;
    void leaveEvent(QEvent *) override;

protected:
    void mousePressEvent(QMouseEvent *) override;

protected:
    void paintEvent(QPaintEvent *) override;

private:
    class dtkWidgetsDataManagerPrivate *d;

private:
    static dtkWidgetsDataManager *s_instance;

protected:
    void      dropEvent(QDropEvent *event) override;
    void dragEnterEvent(QDragEnterEvent *event) override;
};

//
// dtkWidgetsDataManager.h ends here
