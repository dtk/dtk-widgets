// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

class dtkWidgetsMainWindow;
class QCoreApplication;
class QSettings;
class QCommandLineParser;

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class DTKWIDGETS_EXPORT dtkApplicationPrivate
{
public:
     dtkApplicationPrivate(void);
    ~dtkApplicationPrivate(void);

public:
    void initialize(void);

public:
    void setApplication(QCoreApplication *app);

public:
    QSettings *settings = nullptr;
    QCommandLineParser *parser = nullptr;
    QCoreApplication *app = nullptr;

public:
    static dtkWidgetsMainWindow *window;
};

//
// dtkApplication_p.h ends here
