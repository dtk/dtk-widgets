// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include "dtkWidgetsParameterBrowse.h"

#include <dtkCore/dtkCoreParameterPath>

class  DTKWIDGETS_EXPORT dtkWidgetsParameterDirBrowse : public dtkWidgetsParameterBrowse
{
    Q_OBJECT

public:
    dtkWidgetsParameterDirBrowse(QWidget* parent = nullptr);

private:
    QString browse(void) override;
};

// ///////////////////////////////////////////////////////////////////

inline dtkWidgetsParameter *dtkWidgetsParameterDirBrowseCreator(void)
{
    return new dtkWidgetsParameterDirBrowse();
}
//
// dtkWidgetsParameterDirBrowse.h ends here
