// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsSpinBoxDouble.h"

#include <QtWidgets>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsSpinBoxDoublePrivate : public QObject
{
    Q_OBJECT

public slots:
    void update();
    void touch();

public:
    double            value;

    QDoubleValidator *validator;
    bool              scientific;
    int               decimals;
    double            increment;
    double            minimum;
    double            maximum;

public:
    QString textFromValue ( double ) const;
    double  valueFromText ( const QString& ) const;

    QValidator::State validate(QString&, int&) const;


public:
    dtkWidgetsSpinBoxDouble *q;
};

void dtkWidgetsSpinBoxDoublePrivate::update(void)
{
    double old_value = this->value;

    this->valueFromText(q->text());

    if(old_value != this->value) {
        this->touch();
    }
}

void dtkWidgetsSpinBoxDoublePrivate::touch()
{
    q->emit valueChanged(this->value);
}

QString dtkWidgetsSpinBoxDoublePrivate::textFromValue ( double value ) const
{
    QString strValue;

    if (value < this->minimum)
        value = this->minimum;

    if (value > this->maximum)
        value = this->maximum;

    if (this->scientific)
        strValue = QString::number(value, 'e', this->decimals);
    else
        strValue = QString::number(value, 'f', this->decimals);

    return strValue;
}

double dtkWidgetsSpinBoxDoublePrivate::valueFromText ( const QString & text ) const
{
    QString work_on = text;
    QStringList mant_exp;
    QString exponent;

    // split mantisse / exponent
    if ( this->scientific ) { // split mantisse + exp
        mant_exp = text.split(QRegularExpression("e"));

        work_on = mant_exp[0];

        if ( mant_exp.size() > 1 ) {
            exponent = mant_exp[1];
        } else {
            exponent = "0" ;
        }

    }

    // truncate to the right decimal lenght
    QStringList int_dec;
    int_dec = work_on.split(QRegularExpression("\\."));

    // truncate decimals
    QString result;
    if (int_dec.size() > 1) {
        if ( int_dec[1].length() > this->decimals) {
            int_dec[1] = int_dec[1].left(this->decimals);
        }
        result = int_dec[0] + "." + int_dec[1];
    }
    else {
        result = int_dec[0] ;
    }

    if ( this->scientific ) {
        result += "e" + exponent;
    }

    double dblValue = result.toDouble();

    // check min/max ? What if decimals or min and max are inadequate ?
    if (dblValue < this->minimum)
        dblValue = this->minimum;
    if (dblValue > this->maximum)
        dblValue = this->maximum;

    //q->lineEdit()->blockSignals(true);  // protected !!
    //q->lineEdit()->setText(result);
    //q->lineEdit()->blockSignals(false);
    q->setValue(dblValue);
    return dblValue;
}

QValidator::State dtkWidgetsSpinBoxDoublePrivate::validate(QString &text, int &pos) const
{
    // check user input
    bool input_check;
    text.toDouble(&input_check);

    return (input_check ? QValidator::Acceptable : QValidator::Invalid);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkWidgetsSpinBoxDouble::dtkWidgetsSpinBoxDouble(QDoubleValidator::Notation notation, QWidget *parent)
    : dtkWidgetsBaseSpinBox(parent), d(new dtkWidgetsSpinBoxDoublePrivate)
{
    d->q = this;

    d->value = 0.0;

    d->validator = new QDoubleValidator();
    d->validator->setNotation(notation);

    d->scientific = ( notation == QDoubleValidator::ScientificNotation);

    //this->setMaximum(std::numeric_limits<double>::max());    // instead of default 99.99
    //this->setMaximum(std::numeric_limits<double>::lowest());
    //this->setDecimals(32);
    d->maximum = std::numeric_limits<double>::max();    // instead of default 99.99
    d->minimum = std::numeric_limits<double>::lowest();
    d->decimals = 32;
    d->increment = 1. / pow(10, d->decimals);

    this->lineEdit()->setValidator(d->validator);

    QObject::connect(this, &dtkWidgetsBaseSpinBox::editingFinished,
                     d,    &dtkWidgetsSpinBoxDoublePrivate::update);
}

dtkWidgetsSpinBoxDouble::~dtkWidgetsSpinBoxDouble(void)
{
    delete d->validator;
    delete d;
}

void dtkWidgetsSpinBoxDouble::stepBy(int step)
{
    if (step == 0 ) {
        return;
    }
    if (step < 0) {
        int i = step;
        while ( i < 0 ) {
            stepDown();
            i++;
        }
    }
    else  {
        int i = step;
        while ( i > 0) {
            stepUp();
            i--;
        }
    }
}

void dtkWidgetsSpinBoxDouble::stepDown(void)
{
    if (d->scientific) {
        d->value /= 10.0;

    } else {
        d->value -= d->increment;
    }

    if(d->value < d->minimum)
        d->value = d->minimum;

    d->touch();
}

void dtkWidgetsSpinBoxDouble::stepUp(void)
{
    if (d->scientific) {
        d->value *= 10.0;
    } else {
        d->value += d->increment;
    }

    if(d->value > d->maximum)
        d->value = d->maximum;

    d->touch();
}


void dtkWidgetsSpinBoxDouble::setMinimum(double min)
{
    d->minimum = min;
}

void dtkWidgetsSpinBoxDouble::setMaximum(double max)
{
    d->maximum = max;
}

void dtkWidgetsSpinBoxDouble::setValue(double val)
{
    d->value = val;
    this->lineEdit()->blockSignals(true);
    this->lineEdit()->setText(d->textFromValue(val));
    this->lineEdit()->blockSignals(false);
}

void dtkWidgetsSpinBoxDouble::setDecimals(int dec)
{
    d->decimals = dec;
    d->increment = 1. / pow(10, d->decimals);
    this->setValue(d->value);  // muste change the text in the widget accordingly
}



#include "dtkWidgetsSpinBoxDouble.moc"

//
// dtkWidgetsSpinBoxDouble.h ends here
