// Version: $Id: 75b9a2274141f2fdfde6ce56ab55db6d96bd7ece $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsListControl.h"

#include "dtkWidgetsController.h"
#include "dtkWidgetsLayout.h"
#include "dtkWidgetsLayoutItem.h"
#include "dtkWidgetsList.h"

#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>

#include <QtCore>

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsListControlPrivate
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsListControlPrivate
{
public:
    dtkWidgetsLayout *layout = nullptr;
    dtkWidgetsList *list = nullptr;

public:
    QPushButton *hor = nullptr;
    QPushButton *ver = nullptr;
    QPushButton *grd = nullptr;
    QPushButton *cls = nullptr;
};

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsListControl
// ///////////////////////////////////////////////////////////////////

dtkWidgetsListControl::dtkWidgetsListControl(QWidget *parent) : QFrame(parent), d(new dtkWidgetsListControlPrivate)
{
    d->layout = nullptr;
    d->list = nullptr;

    d->hor = new QPushButton("Horizontal", this);
    d->ver = new QPushButton("Vertical", this);
    d->grd = new QPushButton("Grid", this);
    d->cls = new QPushButton("Close All", this);

    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->addWidget(d->hor);
    layout->addWidget(d->ver);
    layout->addWidget(d->grd);
    layout->addWidget(d->cls);

    connect(d->hor, SIGNAL(clicked()), this, SLOT(onLayoutHorizontally()));
    connect(d->ver, SIGNAL(clicked()), this, SLOT(onLayoutVertically()));
    connect(d->grd, SIGNAL(clicked()), this, SLOT(onLayoutGrid()));
    connect(d->cls, SIGNAL(clicked()), this, SLOT(onLayoutCloseAll()));
}

dtkWidgetsListControl::~dtkWidgetsListControl(void)
{
    delete d;

    d = nullptr;
}

void dtkWidgetsListControl::setLayout(dtkWidgetsLayout *layout)
{
    d->layout = layout;
}

void dtkWidgetsListControl::setList(dtkWidgetsList *list)
{
    d->list = list;
}

bool dtkWidgetsListControl::isEmpty(void) const
{
    if (!d->list)
        return true;

    if (!d->list->count())
        return true;

    if (!d->layout)
        return true;

    return false;
}

void dtkWidgetsListControl::onActorStarted(QString view_name)
{
    if (!d->layout)
        return;

    dtkWidgetsWidget *view = dtkWidgetsController::instance()->view(view_name);

    if (view && !d->layout->current()->proxy()->view())
        d->layout->current()->proxy()->setView(view);
}

void dtkWidgetsListControl::layoutHorizontally(void)
{
    if (this->isEmpty())
        return;

    this->closeAllLayout();

    qApp->sendPostedEvents(0, QEvent::DeferredDelete);

    int w = d->layout->current()->width();
    int n = d->list->count();
    int s = d->layout->current()->handleWidth();
    int v = (w - (n - 1) * s) / n;

    for (int i = 1; i <= n; i++) {

        dtkWidgetsLayoutItem *current = d->layout->current();
        current->setOrientation(Qt::Horizontal);
        current->proxy()->setView(dtkWidgetsController::instance()->view(d->list->item(i - 1)->text().split(" ").first()));

        if (i != n) {
            QList<int> sizes = QList<int>() << v << current->width() - s - v;
            current->split();
            current->setSizes(sizes);
        }

        d->layout->setCurrent(current->second());
    }
}

void dtkWidgetsListControl::onLayoutHorizontally(void)
{
    this->layoutHorizontally();
}

void dtkWidgetsListControl::layoutVertically(void)
{
    if (this->isEmpty())
        return;

    this->closeAllLayout();

    qApp->sendPostedEvents(0, QEvent::DeferredDelete);

    int h = d->layout->current()->height();
    int f = d->layout->current()->footerHeight();
    int n = d->list->count();
    int s = d->layout->current()->handleHeight();
    int v = (h - n * f - (n - 1) * s) / n;

    for (int i = 1; i <= n; i++) {

        dtkWidgetsLayoutItem *current = d->layout->current();
        current->setOrientation(Qt::Vertical);
        current->proxy()->setView(dtkWidgetsController::instance()->view(d->list->item(i - 1)->text().split(" ").first()));

        if (i != n) {
            QList<int> sizes = QList<int>() << v + f << current->height() - s - v - f;
            current->split();
            current->setSizes(sizes);
        }

        d->layout->setCurrent(current->second());
    }
}

void dtkWidgetsListControl::onLayoutVertically(void)
{
    this->layoutVertically();
}

void dtkWidgetsListControl::layoutGrid(void)
{
    if (this->isEmpty())
        return;

    int n = d->list->count();
    int i = 0;

    typedef QPair<dtkWidgetsLayoutItem *, Qt::Orientation> item_t;

    d->layout->clear();
    d->layout->setCurrent(d->layout->root());
    d->layout->current()->proxy()->setView(dtkWidgetsController::instance()->view(d->list->item(i)->text().split(" ").first()));

    QList<item_t> items; items << qMakePair(d->layout->current(), Qt::Horizontal);

    for (int i = 1; i < n; i++) {

        item_t item = items.takeFirst();

        d->layout->setCurrent(item.first);

        dtkWidgetsLayoutItem *current = item.first;
        current->setOrientation(item.second);
        current->split();
        current->second()->proxy()->setView(dtkWidgetsController::instance()->view(d->list->item(i)->text().split(" ").first()));

        items << qMakePair(current->first(), item.second == Qt::Horizontal ? Qt::Vertical : Qt::Horizontal);
        items << qMakePair(current->second(), item.second == Qt::Horizontal ? Qt::Vertical : Qt::Horizontal);
    }
}

void dtkWidgetsListControl::onLayoutGrid(void)
{
    this->layoutGrid();
}

void dtkWidgetsListControl::closeAllLayout(void)
{
    if (this->isEmpty())
        return;

    d->layout->clear();
    d->layout->setCurrent(d->layout->root());
}

void dtkWidgetsListControl::onLayoutCloseAll(void)
{
    this->closeAllLayout();
}

//
// dtkWidgetsListControl.cpp ends here
