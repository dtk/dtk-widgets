//
// dtkWidgetsParameterDirBrowse.cpp ends here
// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsParameterDirBrowse.h"

#include <QtWidgets>

dtkWidgetsParameterDirBrowse::dtkWidgetsParameterDirBrowse(QWidget* parent) : dtkWidgetsParameterBrowse(parent)
{}

QString dtkWidgetsParameterDirBrowse::browse(void) {
    return QFileDialog::getExistingDirectory(this, "Open Directory", dtkWidgetsParameterBrowse::m_parameter->dirName());
}

//
// dtkWidgetsParameterDirBrowse.cpp ends here
