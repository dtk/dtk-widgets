// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkApplication.h"
#include "dtkApplication_p.h"

#include "dtkWidgetsMainWindow.h"

#include <dtkFonts>
#include <dtkLog>
#include <dtkThemes>

#include <QtCore>
#include <QtWidgets>

// /////////////////////////////////////////////////////////////////////////////
// dtkApplicationPrivate implementation
// /////////////////////////////////////////////////////////////////////////////

dtkWidgetsMainWindow *dtkApplicationPrivate::window = nullptr;

dtkApplicationPrivate::dtkApplicationPrivate(void) : settings(nullptr), parser(new QCommandLineParser), app(nullptr)
{
    this->parser->setSingleDashWordOptionMode(QCommandLineParser::ParseAsLongOptions);
}

dtkApplicationPrivate::~dtkApplicationPrivate(void)
{
    if (this->settings) {
        delete this->settings;
        this->settings = nullptr;
    }

    delete this->parser;

    this->parser = nullptr;
}

void dtkApplicationPrivate::setApplication(QCoreApplication *q)
{
    this->app = q;
}

void dtkApplicationPrivate::initialize(void)
{
    QString verbosity = "info";

    qputenv("QT_PLUGIN_PATH", "1");
    qputenv("LC_ALL", "C");

    QLocale::setDefault(QLocale::c());
    QSettings::setDefaultFormat(QSettings::IniFormat);

#if defined (Q_OS_UNIX) && !defined(Q_OS_MAC)
    setlocale(LC_NUMERIC, "C");
#endif

    this->parser->addHelpOption();
    this->parser->addVersionOption();

    QCommandLineOption settingsOption("settings", "main settings file", "filename");
    this->parser->addOption(settingsOption);

    QCommandLineOption verboseOption("verbose", QCoreApplication::translate("main", "verbose plugin initialization"));
    this->parser->addOption(verboseOption);

    QCommandLineOption nonguiOption(QStringList() << "nw" << "no-window", QCoreApplication::translate("main", "non GUI application (no window)"));
    this->parser->addOption(nonguiOption);

    QCommandLineOption loglevelOption("loglevel", "log level used by dtkLog (default is info), available: trace|debug|info|warn|error|fatal", "level", verbosity);
    this->parser->addOption(loglevelOption);

    QCommandLineOption logfileOption("logfile", qPrintable(QString("log file used by dtkLog; default is: ").append(dtkLogPath(app))), "filename | console", dtkLogPath(app));
    this->parser->addOption(logfileOption);

    QCommandLineOption logfileMaxSizeOption("logfilemax", "log file max size  (in MB); default is: 3072 (3GB)", "size");
    this->parser->addOption(logfileMaxSizeOption);

    QCommandLineOption themeOption("theme", "theme", "themename");
    this->parser->addOption(themeOption);

    this->parser->process(*(this->app));

    if (this->parser->isSet(settingsOption))
        this->settings = new QSettings(this->parser->value(settingsOption), QSettings::IniFormat);
    else
        this->settings = new QSettings();

    if (settings->contains("log_level")) {

        dtkLogger::instance().setLevel(settings->value("log_level").toString());

    } else {

        if (this->parser->isSet(loglevelOption))
            verbosity = this->parser->value(loglevelOption);

        dtkLogger::instance().setLevel(verbosity);
    }

    qlonglong max_size = 1024L * 1024L * 1024L;

    if (this->parser->isSet(logfileMaxSizeOption))
        max_size = this->parser->value(logfileMaxSizeOption).toLongLong() * 1024 * 1024;

    if (this->parser->isSet(logfileOption)) {

        if (this->parser->value(logfileOption) == "console")
            dtkLogger::instance().attachConsole();
        else
            dtkLogger::instance().attachFile(this->parser->value(logfileOption), max_size);

    } else {

        dtkLogger::instance().attachFile(dtkLogPath(this->app), max_size);
    }

    if (this->parser->isSet("theme"))
        dtkThemesEngine::instance()->apply(this->parser->value("theme"));
    else
        dtkThemesEngine::instance()->apply();
}

// ///////////////////////////////////////////////////////////////////
// dtkApplication implementation
// ///////////////////////////////////////////////////////////////////

dtkApplication::dtkApplication(int& argc, char **argv): QApplication(argc, argv), d(new dtkApplicationPrivate)
{
    dtkFontSourceSansPro::instance()->initFontSourceSansPro();

    d->setApplication(this);

    this->setFont(dtkFontSourceSansPro::instance()->font(13));
}

dtkApplication::~dtkApplication(void)
{
    delete d;

    d = nullptr;
}

void dtkApplication::initialize(void)
{
    d->initialize();
}

void dtkApplication::setup(setup_t setup)
{
    setup(this);
}

bool dtkApplication::noGui(void)
{
    return !(qApp && qobject_cast<QGuiApplication *>(qApp) && (QGuiApplication::platformName() != "minimal")) ;
}

dtkApplication *dtkApplication::create(int& argc, char *argv[])
{
    for (int i = 0; i < argc; i++)
        if(!qstrcmp(argv[i], "-nw") || !qstrcmp(argv[i], "--nw") ||  !qstrcmp(argv[i], "-no-window") || !qstrcmp(argv[i], "--no-window") || !qstrcmp(argv[i], "-h") || !qstrcmp(argv[i], "--help") || !qstrcmp(argv[i], "--version"))
            qputenv("QT_QPA_PLATFORM", QByteArrayLiteral("minimal"));

    return new dtkApplication(argc, argv);
}

void dtkApplication::setWindow(dtkWidgetsMainWindow *window)
{
    dtkApplicationPrivate::window = window;
}

dtkWidgetsMainWindow *dtkApplication::window(void)
{
    if(!dtkApplicationPrivate::window)
        dtkApplicationPrivate::window = new dtkWidgetsMainWindow;

    return dtkApplicationPrivate::window;
}

int dtkApplication::exec(void)
{
    dtkApplication *application = static_cast<dtkApplication *>(qApp);

    application->window()->show();
    application->window()->raise();

    return QApplication::exec();
}

QCommandLineParser *dtkApplication::parser(void)
{
    return d->parser;
}

QSettings *dtkApplication::settings(void)
{
    return d->settings;
}

//
// dtkApplication.cpp ends here
