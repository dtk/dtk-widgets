#include "dtkWidgetsParameterIntRange.h"

#include "dtkWidgetsSpinBoxLong.h"

#include <QtWidgets>

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsParameterIntRangePrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsParameterIntRangePrivate
{
public:
    dtkWidgetsSpinBoxLong *spin_box_min = nullptr;
    dtkWidgetsSpinBoxLong *spin_box_max = nullptr;
    //QWidget *spinbox_widget = nullptr;

};

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsParameterIntRange implementation
// ///////////////////////////////////////////////////////////////////

dtkWidgetsParameterIntRange::dtkWidgetsParameterIntRange(QWidget* parent) : dtkWidgetsParameterBase<dtk::d_range_int>(parent), d(new dtkWidgetsParameterIntRangePrivate)
{
    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    //layout->setSpacing(0);

    d->spin_box_min = new dtkWidgetsSpinBoxLong;

    d->spin_box_max = new dtkWidgetsSpinBoxLong;

    layout->addWidget(d->spin_box_min);
    layout->addWidget(d->spin_box_max);
}

dtkWidgetsParameterIntRange::~dtkWidgetsParameterIntRange(void)
{
    delete d;
}

void dtkWidgetsParameterIntRange::setReadOnly(bool val)
{
    this->dtkWidgetsParameter::m_readonly = val;
    d->spin_box_min->setReadOnly(this->dtkWidgetsParameter::m_readonly);
    d->spin_box_min->setStepEnabled(!this->dtkWidgetsParameter::m_readonly);
    d->spin_box_min->setFocusPolicy((this->dtkWidgetsParameter::m_readonly? Qt::NoFocus : Qt::StrongFocus));

    d->spin_box_max->setReadOnly(this->dtkWidgetsParameter::m_readonly);
    d->spin_box_max->setStepEnabled(!this->dtkWidgetsParameter::m_readonly);
    d->spin_box_max->setFocusPolicy((this->dtkWidgetsParameter::m_readonly? Qt::NoFocus : Qt::StrongFocus));
};

bool dtkWidgetsParameterIntRange::connect(dtkCoreParameter *p)
{
    if (!p) {
        qWarning() << Q_FUNC_INFO << "The input parameter is null. Nothing is done.";
        return false;
    }

    m_parameter = dynamic_cast<dtk::d_range_int*>(p);

    if(!m_parameter) {
        qWarning() << Q_FUNC_INFO << "The type of the parameter is not compatible with the widget dtkWidgetsParameterIntRange.";
        return false;
    }

    m_default  = m_parameter->variant();

    std::array<long long, 2> value = m_parameter->value();

    d->spin_box_min->setMinimum(m_parameter->min());
    d->spin_box_min->setMaximum(value[1]);
    d->spin_box_min->setValue(value[0]);
    d->spin_box_min->setToolTip(m_parameter->documentation());
    d->spin_box_min->setKeyboardTracking(false);

    d->spin_box_max->setMinimum(value[0]);
    d->spin_box_max->setMaximum(m_parameter->max());
    d->spin_box_max->setValue(value[1]);
    d->spin_box_max->setToolTip(m_parameter->documentation());
    d->spin_box_max->setKeyboardTracking(false);


    m_c = m_parameter->connect([=,this] (QVariant v)
    {
        std::array<long long, 2>  value = v.value<dtk::d_range_int>().value();
        d->spin_box_min->blockSignals(true);
        d->spin_box_max->blockSignals(true);

        d->spin_box_min->setValue(value[0]);
        d->spin_box_min->setMaximum(value[1]);
        d->spin_box_max->setMinimum(value[0]);
        d->spin_box_max->setValue(value[1]);

        d->spin_box_min->blockSignals(false);
        d->spin_box_max->blockSignals(false);
    });

    //Note: here the type is "long long" instead of "int", to inherit the focus manager from the BaseRange.
    QObject::connect(d->spin_box_min, QOverload<long long>::of(&dtkWidgetsSpinBoxLong::valueChanged), [=,this] (int v)
    {
        std::array<long long, 2> value = {v, m_parameter->value()[1]};
        m_parameter->shareValue(QVariant::fromValue(value));
    });

    QObject::connect(d->spin_box_max, QOverload<long long>::of(&dtkWidgetsSpinBoxLong::valueChanged), [=,this] (int v)
    {
        std::array<long long, 2> value = {m_parameter->value()[0], v};
        m_parameter->shareValue(QVariant::fromValue(value));
    });

    return true;
}
