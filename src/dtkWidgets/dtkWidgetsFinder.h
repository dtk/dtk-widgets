// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:
#pragma once
#include <dtkWidgetsExport>

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

// /////////////////////////////////////////////////////////////////
// dtkWidgetsFinderToolBar
// /////////////////////////////////////////////////////////////////

class DTKWIDGETS_EXPORT dtkWidgetsFinderToolBar : public QFrame
{
    Q_OBJECT

public:
     dtkWidgetsFinderToolBar(QWidget *parent = 0);
    ~dtkWidgetsFinderToolBar(void);

public:
     QSize sizeHint(void) const;

signals:
    void changed(const QString& path);
    void listView(void);
    void treeView(void);
    void showHiddenFiles(bool);

public slots:
    void setPath(const QString& path);
    void onPrev(void);
    void onNext(void);
    void onListView(void);
    void onTreeView(void);
    void onShowHiddenFiles(bool);

private:
    class dtkWidgetsFinderToolBarPrivate *d;
};

// /////////////////////////////////////////////////////////////////
// dtkWidgetsFinderSideView
// /////////////////////////////////////////////////////////////////

class DTKWIDGETS_EXPORT dtkWidgetsFinderSideView : public QTreeWidget
{
    Q_OBJECT
    Q_PROPERTY(int headerFontSize READ headerFontSize WRITE setHeaderFontSize)

public:
     dtkWidgetsFinderSideView(QWidget *parent = 0);
    ~dtkWidgetsFinderSideView(void);

    void populate(void);

    int headerFontSize(void) const;

    QSize sizeHint (void) const;

signals:
    void changed(const QString& path);

public slots:
    void setPath(const QString& path);
    void setHeaderFontSize(int value);

public slots:
    void    addBookmark(const QString& path);
    void removeBookmark(const QString& path);
    void  clearBookmarks(void);

private slots:
    void onItemClicked(QTreeWidgetItem *, int);
    void onContextMenu(const QPoint&);

protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dropEvent(QDropEvent *event);

    QString driveLabel(QString drive);

private:
    class dtkWidgetsFinderSideViewPrivate *d;
};

// /////////////////////////////////////////////////////////////////
// dtkWidgetsFinderPathBar
// /////////////////////////////////////////////////////////////////

class DTKWIDGETS_EXPORT dtkWidgetsFinderPathBar : public QFrame
{
    Q_OBJECT

public:
     dtkWidgetsFinderPathBar(QWidget *parent = 0);
    ~dtkWidgetsFinderPathBar(void);

     QSize sizeHint(void) const;

signals:
    void changed(const QString& path);

public slots:
    void setPath(const QString &path);

protected:
    void mousePressEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);

private:
    class dtkWidgetsFinderPathBarPrivate *d;
};

// /////////////////////////////////////////////////////////////////
// dtkWidgetsFinderListView
// /////////////////////////////////////////////////////////////////

class DTKWIDGETS_EXPORT dtkWidgetsFinderListView : public QListView
{
    Q_OBJECT

public:
     dtkWidgetsFinderListView(QWidget *parent = 0);
    ~dtkWidgetsFinderListView(void);

    void addContextMenuAction(QAction *action);

    void addDefaultContextMenuAction(QAction *action);

    QString selectedPath(void) const;

    QStringList selectedPaths(void) const;

    void allowFileBookmarking(bool isAllowed);

signals:
    void changed(const QString& path);
    void bookmarked(const QString& path);

public slots:
    void onBookmarkSelectedItemsRequested(void);

protected slots:
    void updateContextMenu(const QPoint&);

protected:
     void keyPressEvent(QKeyEvent *event);
     void mouseDoubleClickEvent(QMouseEvent *event);
     void startDrag(Qt::DropActions supportedActions);

private:
    class  dtkWidgetsFinderListViewPrivate *d;
};

// /////////////////////////////////////////////////////////////////
// dtkWidgetsFinderTreeView
// /////////////////////////////////////////////////////////////////

class DTKWIDGETS_EXPORT dtkWidgetsFinderTreeView : public QTreeView
{
    Q_OBJECT

public:
     dtkWidgetsFinderTreeView(QWidget *parent = 0);
    ~dtkWidgetsFinderTreeView(void);

    int sizeHintForColumn(int column) const;

    void addContextMenuAction(QAction *action);

    void addDefaultContextMenuAction(QAction *action);

    QString selectedPath(void) const;

    QStringList selectedPaths(void) const;

    void allowFileBookmarking(bool isAllowed);

signals:
    void changed(const QString& path);
    void bookmarked(const QString& path);

public slots:
    void onBookmarkSelectedItemsRequested(void);

protected slots:
    void updateContextMenu(const QPoint&);

protected:
     void keyPressEvent(QKeyEvent *event);
     void mouseDoubleClickEvent(QMouseEvent *event);
     void startDrag(Qt::DropActions supportedActions);
     void resizeEvent(QResizeEvent *event);

private:
     class dtkWidgetsFinderTreeViewPrivate *d;
};

// /////////////////////////////////////////////////////////////////
// dtkWidgetsFinder
// /////////////////////////////////////////////////////////////////

class DTKWIDGETS_EXPORT dtkWidgetsFinder : public QFrame
{
    Q_OBJECT

public:
     dtkWidgetsFinder(QWidget *parent = 0);
    ~dtkWidgetsFinder(void);

    void addContextMenuAction(QAction *action);

    QString selectedPath(void) const;
    QStringList selectedPaths(void) const;

    void allowFileBookmarking(bool isAllowed);
    void allowMultipleSelection(bool isAllowed);

signals:
    void changed(const QString& path);
    void bookmarked(const QString& path);
    void fileDoubleClicked(const QString &filename);
    void fileClicked(const QFileInfo &info);
    void selectionChanged(const QStringList& paths);
    void nothingSelected(void);
    void listView(void);
    void treeView(void);
    void showHiddenFiles(bool);

public slots:
    void setPath(const QString& path);
    void switchToListView(void);
    void switchToTreeView(void);
    void onShowHiddenFiles(bool);
    void switchShowHiddenFiles(void);

public slots:
    void onBookmarkSelectedItemsRequested(void);

protected slots:
    void onIndexDoubleClicked(QModelIndex);
    void onIndexClicked(QModelIndex);
    void onSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected);
    void emitSelectedItems();

private:
    class dtkWidgetsFinderPrivate *d;
};

//
// dtkWidgetsFinder.h ends here
