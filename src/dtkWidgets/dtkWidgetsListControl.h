// Version: $Id: 1e24dcf163066c20eec3b046be614d41dd2ed9f6 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <QtWidgets/QFrame>

#include <QtCore/QString>

class dtkWidgetsLayout;
class dtkWidgetsList;

class DTKWIDGETS_EXPORT dtkWidgetsListControl : public QFrame
{
    Q_OBJECT

public:
     dtkWidgetsListControl(QWidget *parent = nullptr);
    ~dtkWidgetsListControl(void);

public:
    void setLayout(dtkWidgetsLayout *layout);
    void setList(dtkWidgetsList *list);

public:
    bool isEmpty(void) const;

public:
    void closeAllLayout(void);
    void layoutHorizontally(void);
    void layoutVertically(void);
    void layoutGrid(void);

public slots:
    void onActorStarted(QString view_name);

protected slots:
    void onLayoutHorizontally(void);
    void onLayoutVertically(void);
    void onLayoutGrid(void);
    void onLayoutCloseAll(void);

private:
    class dtkWidgetsListControlPrivate *d;
};

//
// dtkWidgetsListControl.h ends here
