// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <QtCore>
#include <QtWidgets>

class dtkWidgetsDataManagerPrivate;
class dtkWidgetsDataManagerItemButton;

class DTKWIDGETS_EXPORT dtkWidgetsDataManagerItem : public QLabel
{
    Q_OBJECT

public:
    dtkWidgetsDataManagerItem(const QColor &, const QPixmap &thumbnail, dtkWidgetsDataManagerPrivate *parent);

    ~dtkWidgetsDataManagerItem(void) override;

signals:

    void clicked(void);
    void destroy(void);
    void save(void);

public:
    int id;

public:
    dtkWidgetsDataManagerPrivate *parent;

public:
    dtkWidgetsDataManagerItemButton *button_destroy;
    dtkWidgetsDataManagerItemButton *button_save;

public:
    QPixmap thumbnail;
    QPixmap transparent_thumbnail;

protected:
    void enterEvent(QEnterEvent *) override;
    void leaveEvent(QEvent *) override;
    void mouseReleaseEvent(QMouseEvent *) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void dropEvent(QDropEvent *event) override;
    void dragEnterEvent(QDragEnterEvent *event) override;

};

//
// dtkWidgetsDataManagerItem.h ends here
