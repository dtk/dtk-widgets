// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include "dtkWidgetsParameter.h"

#include <dtkCore/dtkCoreParameterPath>

class  DTKWIDGETS_EXPORT dtkWidgetsParameterBrowse : public dtkWidgetsParameterBase<dtk::d_path>
{
    Q_OBJECT

public:
     dtkWidgetsParameterBrowse(QWidget* parent = nullptr);
    ~dtkWidgetsParameterBrowse(void);

public:
    bool connect(dtkCoreParameter *) override;
    void setReadOnly(bool) override;

protected:
    virtual QString browse(void) = 0;

protected:
    using dtkWidgetsParameterBase<dtk::d_path>::m_parameter;

private:
    class dtkWidgetsParameterBrowsePrivate *d;
};

//
// dtkWidgetsParameterBrowse.h ends here
