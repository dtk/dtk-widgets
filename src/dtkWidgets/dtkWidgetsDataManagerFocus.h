// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <QtWidgets>

class dtkWidgetsDataManagerItem;

class DTKWIDGETS_EXPORT dtkWidgetsDataManagerFocus : public QWidget
{
    Q_OBJECT

public:
    dtkWidgetsDataManagerFocus(QWidget *parent = nullptr);

    ~dtkWidgetsDataManagerFocus(void);

public:
    void setItem(dtkWidgetsDataManagerItem *item, const QVariantMap& metadata);
    void hideMetaData(void);
    void scalePixmap(int w, int h);
    void showMetadata(void);

public:
    QPoint source;
    QPoint destnt;

public:
    QSize s_size;
    QSize d_size;

public:
    bool presented = false;

protected:
    void mouseReleaseEvent(QMouseEvent *ev) override;

signals:
    void clicked(void);

private:
    class dtkWidgetsDataManagerFocusPrivate *d;
};

//
// dtkWidgetsDataManagerFocus.h ends here
