// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsParameterBrowse.h"

#include <QtWidgets>
#include <QStringBuilder>

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsParameterBrowsePrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsParameterBrowsePrivate
{
public:
    QPushButton *push_button = nullptr;
public:
    void setToolTip(const QString& doc, const QString& path)
    {
        QString tooltip;
        if (!doc.isEmpty()) {
            tooltip += QString("Doc: %1").arg(doc);
        }
        if (!path.isEmpty()) {
            if (!tooltip.isEmpty()) {
                tooltip += "\n";
            }
            tooltip += QString("Path: %1").arg(path);
        }
        push_button->setToolTip( tooltip );
    }
};

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsParameterBrowse implementation
// ///////////////////////////////////////////////////////////////////

dtkWidgetsParameterBrowse::dtkWidgetsParameterBrowse(QWidget* parent) : dtkWidgetsParameterBase<dtk::d_path>(parent), d(new dtkWidgetsParameterBrowsePrivate)
{
    d->push_button = new QPushButton;

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(d->push_button);

    this->setLayout(layout);
}

dtkWidgetsParameterBrowse::~dtkWidgetsParameterBrowse(void)
{
    delete d;
}

void dtkWidgetsParameterBrowse::setReadOnly(bool val)
{
    this->dtkWidgetsParameter::m_readonly = val;
    d->push_button->setEnabled(!this->dtkWidgetsParameter::m_readonly);
    d->push_button->setFocusPolicy((this->dtkWidgetsParameter::m_readonly? Qt::NoFocus : Qt::WheelFocus));
}

bool dtkWidgetsParameterBrowse::connect(dtkCoreParameter *p)
{
    if (!p) {
        qWarning() << Q_FUNC_INFO << "The input parameter is null. Nothing is done.";
        return false;
    }

    m_parameter = dynamic_cast<dtk::d_path *>(p);

    if(!m_parameter) {
        qWarning() << Q_FUNC_INFO << "The type of the parameter is not compatible with the widget dtkWidgetsParameterBrowse.";
        return false;
    }

    QString text = "Browse";

    QString base_name = m_parameter->baseName();
    if(!base_name.isEmpty()) {
        text = base_name;
    }

    d->push_button->setText(text);
    d->setToolTip( m_parameter->documentation(), m_parameter->path() );
    m_default  = m_parameter->variant();
    m_c = m_parameter->connect([=,this] (QVariant v) {
        d->push_button->blockSignals(true);
        QString value = v.value<dtk::d_path>().baseName();
        if (value.isEmpty()) {
            value = "Browse";
        }
        d->push_button->setText(value);
        d->push_button->blockSignals(false);

        QString tip = v.value<dtk::d_path>().path();
        if(!tip.isEmpty()) {
            d->setToolTip( m_parameter->documentation(), tip );
        }
    });

    QObject::connect(d->push_button, &QPushButton::clicked, [=,this] ()
    {
        QString v;

        Qt::KeyboardModifiers modifiers = QApplication::queryKeyboardModifiers();
        if (modifiers & Qt::ShiftModifier) {
            dtkInfo() << "reloading path " << m_parameter->path();
            v = m_parameter->path();
        } else {
            v = this->browse();
        }
        m_parameter->shareValue(QVariant::fromValue(v));
    });

    return true;
}

//
// dtkWidgetsParameterBrowse.cpp ends here
