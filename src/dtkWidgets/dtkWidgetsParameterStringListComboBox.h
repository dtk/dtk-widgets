// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include "dtkWidgetsParameter.h"

#include <dtkCore/dtkCoreParameterInList>

class  DTKWIDGETS_EXPORT dtkWidgetsParameterStringListComboBox : public dtkWidgetsParameterBase<dtk::d_inliststring>
{
    Q_OBJECT

public:
     dtkWidgetsParameterStringListComboBox(QWidget* parent = 0);
    ~dtkWidgetsParameterStringListComboBox(void);

public:
    bool connect(dtkCoreParameter *) override;
    void setReadOnly(bool) override;
    void setFixed(bool);

private:
    using dtkWidgetsParameterBase<dtk::d_inliststring>::m_parameter;

    class dtkWidgetsParameterStringListComboBoxPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkWidgetsParameter *dtkWidgetsParameterStringVariableListComboBoxCreator(void)
{
    dtkWidgetsParameterStringListComboBox *o = new dtkWidgetsParameterStringListComboBox();
    o->setFixed(false);
    return o;
}
inline dtkWidgetsParameter *dtkWidgetsParameterStringListComboBoxCreator(void)
{
    return new dtkWidgetsParameterStringListComboBox();
}
//
// dtkWidgetsParameterStringListComboBox.h ends here
