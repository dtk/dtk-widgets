// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsColorButton.h"

#include <QtGui>
#include <QtWidgets>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsColorButtonPrivate
{
public:
    QColor color;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkWidgetsColorButton::dtkWidgetsColorButton(const QColor& color, QWidget *parent) : QPushButton(parent), d(new dtkWidgetsColorButtonPrivate)
{
    d->color = color;
}

dtkWidgetsColorButton::dtkWidgetsColorButton(QWidget *parent) : QPushButton(parent), d(new dtkWidgetsColorButtonPrivate)
{

}

dtkWidgetsColorButton::~dtkWidgetsColorButton(void)
{
    delete d;
}

const QColor& dtkWidgetsColorButton::color(void)
{
    return d->color;
}

void dtkWidgetsColorButton::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setPen(QColor("#c7c7c7"));
    painter.setBrush(d->color);
    painter.drawRoundedRect(event->rect().adjusted(5, 5-2, -5, -5-2), 2, 2);
}

void dtkWidgetsColorButton::mousePressEvent(QMouseEvent *event)
{
    d->color = QColorDialog::getColor(d->color, this);

    if (d->color.isValid())
        emit colorChosen(d->color);

    QPushButton::mousePressEvent(event);
}

//
// dtkWidgetsColorButton.cpp ends here
