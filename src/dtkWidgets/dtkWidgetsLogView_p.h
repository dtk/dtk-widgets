// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <QtWidgets>

// /////////////////////////////////////////////////////////////////
// dtkWidgetsLogViewBar
// /////////////////////////////////////////////////////////////////

class DTKWIDGETS_EXPORT dtkWidgetsLogViewBar : public QFrame
{
    Q_OBJECT

public:
     dtkWidgetsLogViewBar(QWidget *parent = 0);
    ~dtkWidgetsLogViewBar(void);

signals:
    void displayTrace(bool);
    void displayDebug(bool);
    void displayInfo(bool);
    void displayWarn(bool);
    void displayError(bool);
    void displayFatal(bool);
};

// /////////////////////////////////////////////////////////////////
// dtkWidgetsLogViewTree
// /////////////////////////////////////////////////////////////////

class DTKWIDGETS_EXPORT dtkWidgetsLogViewTree : public QTreeWidget
{
    Q_OBJECT

public:
     dtkWidgetsLogViewTree(QWidget *parent = 0);
    ~dtkWidgetsLogViewTree(void);

signals:
    void runtimeClicked(void);
    void fileClicked(const QString& path);

protected slots:
    void onItemClicked(QTreeWidgetItem *, int);

private:
    QTreeWidgetItem *runtime;
    QTreeWidgetItem *file;
};

// /////////////////////////////////////////////////////////////////
// dtkWidgetsLogViewList
// /////////////////////////////////////////////////////////////////

class DTKWIDGETS_EXPORT dtkWidgetsLogViewList : public QListView
{
    Q_OBJECT

public:
     dtkWidgetsLogViewList(QWidget *parent = 0);
    ~dtkWidgetsLogViewList(void);

public slots:
    void setRuntime(void);
    void setFile(const QString& path);
    void setAutoScroll(bool autoScroll);
    void scrollToBottomThrottled(void);

public:
    void setFilter(const QRegularExpression& expression);

private:
    class dtkLogModel *model;

private:
    QHash<QString, QStringListModel *> models;

private:
    QSortFilterProxyModel *proxy;
    QTimer timer;
};

// /////////////////////////////////////////////////////////////////
// dtkWidgetsLogViewPrivate
// /////////////////////////////////////////////////////////////////

class dtkWidgetsLogViewPrivate
{
public:
    QRegularExpression expression(void);

public:
    dtkWidgetsLogViewBar  *bar;
    dtkWidgetsLogViewTree *tree;
    dtkWidgetsLogViewList *list;

public:
    QStringList exclude;

public:
    QCheckBox *checkbox_auto_scroll;
};

//
// dtkWidgetsLogView_p.h ends here
