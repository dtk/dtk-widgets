// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <dtkThemesWidgets/dtkThemesWidgetsMainWindow>

class dtkWidgetsMenu;
class dtkWidgetsMenuBar;
class QResizeEvent;

class DTKWIDGETS_EXPORT dtkWidgetsMainWindow : public dtkThemesWidgetsMainWindow
{
    Q_OBJECT

public:
     dtkWidgetsMainWindow(QWidget *parent = nullptr);
    ~dtkWidgetsMainWindow(void);

public:
    dtkWidgetsMenuBar *menubar(void);

public slots:
    void setCentralWidget(QWidget *) override;

public:
    void            populate(void);

protected:
    void resizeEvent(QResizeEvent *event) override;

private:
    class dtkWidgetsMainWindowPrivate *d;
};

//
// dtkWidgetsMainWindow.h ends here
