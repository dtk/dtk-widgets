### CMakeLists.txt ---
##

project(dtkWidgets
VERSION
  ${dtkWidgets_VERSION}
LANGUAGES
  CXX)

## #################################################################
## Sources
## #################################################################

set(${PROJECT_NAME}_HEADERS
  dtkApplication.h
  dtkWidgets
  dtkWidgets.h
  dtkWidgetsBaseSpinBox
  dtkWidgetsBaseSpinBox.h
  dtkWidgetsBaseSlider
  dtkWidgetsBaseSlider.h
  dtkWidgetsColorButton
  dtkWidgetsColorButton.h
  dtkWidgetsController
  dtkWidgetsController.h
  dtkWidgetsDataManager
  dtkWidgetsDataManager.h
  dtkWidgetsDataManager_p.h
  dtkWidgetsDataManagerData
  dtkWidgetsDataManagerData.h
  dtkWidgetsDataManagerFocus
  dtkWidgetsDataManagerFocus.h
  dtkWidgetsDataManagerItem
  dtkWidgetsDataManagerItem.h
  dtkWidgetsDataManagerItemButton
  dtkWidgetsDataManagerItemButton.h
  dtkWidgetsFinder
  dtkWidgetsFinder.h
  dtkWidgetsLayout
  dtkWidgetsLayout.h
  dtkWidgetsLayoutItem
  dtkWidgetsLayoutItem.h
  dtkWidgetsLayoutItem_p.h
  dtkWidgetsLayoutReader
  dtkWidgetsLayoutReader.h
  dtkWidgetsLayoutWriter
  dtkWidgetsLayoutWriter.h
  dtkWidgetsList
  dtkWidgetsList.h
  dtkWidgetsListControl
  dtkWidgetsListControl.h
  dtkWidgetsHUD
  dtkWidgetsHUD.h
  dtkWidgetsHUDInfo
  dtkWidgetsHUDInfo.h
  dtkWidgetsHUDItem
  dtkWidgetsHUDItem.h
  dtkWidgetsLogView.h
  dtkWidgetsMainWindow
  dtkWidgetsMainWindow.h
  dtkWidgetsManager
  dtkWidgetsManager.h
  dtkWidgetsMenu
  dtkWidgetsMenu.h
  dtkWidgetsMenu+ux.h
  dtkWidgetsMenuBar
  dtkWidgetsMenuBar.h
  dtkWidgetsMenuBar_p.h
  dtkWidgetsMenuItem
  dtkWidgetsMenuItem.h
  dtkWidgetsMenuItem+custom.h
  dtkWidgetsMenuSpy
  dtkWidgetsMenuSpy.h
  dtkWidgetsOverlayPane
  dtkWidgetsOverlayPane.h
  dtkWidgetsOverlayPaneItem
  dtkWidgetsOverlayPaneItem.h
  dtkWidgetsOverlayPaneSlider
  dtkWidgetsOverlayPaneSlider.h
  dtkWidgetsOverlayRope
  dtkWidgetsOverlayRope.h
  dtkWidgetsParameter
  dtkWidgetsParameter.h
  dtkWidgetsParameterBoolCheckBox
  dtkWidgetsParameterBoolCheckBox.h
  dtkWidgetsParameterBoolPushButton
  dtkWidgetsParameterBoolPushButton.h
  dtkWidgetsParameterBrowse
  dtkWidgetsParameterBrowse.h
  dtkWidgetsParameterDirBrowse
  dtkWidgetsParameterDirBrowse.h
  dtkWidgetsParameterDoubleRange
  dtkWidgetsParameterDoubleRange.h
  dtkWidgetsParameterDoubleSlider
  dtkWidgetsParameterDoubleSlider.h
  dtkWidgetsParameterDoubleSpinBox
  dtkWidgetsParameterDoubleSpinBox.h
  dtkWidgetsParameterFactory
  dtkWidgetsParameterFactory.h
  dtkWidgetsParameterFileBrowse
  dtkWidgetsParameterFileBrowse.h
  dtkWidgetsParameterFileSave
  dtkWidgetsParameterFileSave.h
  dtkWidgetsParameterIntRange
  dtkWidgetsParameterIntRange.h
  dtkWidgetsParameterIntSlider
  dtkWidgetsParameterIntSlider.h
  dtkWidgetsParameterIntSpinBox
  dtkWidgetsParameterIntSpinBox.h
  dtkWidgetsParameterListStringListCheckBox
  dtkWidgetsParameterListStringListCheckBox.h
  dtkWidgetsParameterLongLongSpinBox
  dtkWidgetsParameterLongLongSpinBox.h
  dtkWidgetsParameterMenuBarGenerator
  dtkWidgetsParameterMenuBarGenerator.h
  dtkWidgetsParameterScientificSpinBox
  dtkWidgetsParameterScientificSpinBox.h
  dtkWidgetsParameterStringLineEdit
  dtkWidgetsParameterStringLineEdit.h
  dtkWidgetsParameterStringListComboBox
  dtkWidgetsParameterStringListComboBox.h
  dtkWidgetsSpinBoxDouble.h
  dtkWidgetsSpinBoxLong.h
  dtkWidgetsWidget
  dtkWidgetsWidget.h
  dtkWidgetsWorkspace
  dtkWidgetsWorkspace.h
  dtkWidgetsWorkspaceBar
  dtkWidgetsWorkspaceBar.h
  dtkWidgetsWorkspaceLog.h
  dtkWidgetsWorkspaceStackBar
  dtkWidgetsWorkspaceStackBar.h)

set(${PROJECT_NAME}_SOURCES
  dtkApplication.cpp
  dtkWidgets.cpp
  dtkWidgetsBaseSpinBox.cpp
  dtkWidgetsBaseSlider.cpp
  dtkWidgetsColorButton.cpp
  dtkWidgetsController.cpp
  dtkWidgetsDataManager.cpp
  dtkWidgetsDataManagerData.cpp
  dtkWidgetsDataManagerFocus.cpp
  dtkWidgetsDataManagerItem.cpp
  dtkWidgetsDataManagerItemButton.cpp
  dtkWidgetsFinder.cpp
  dtkWidgetsLayout.cpp
  dtkWidgetsLayoutItem.cpp
  dtkWidgetsLayoutReader.cpp
  dtkWidgetsLayoutWriter.cpp
  dtkWidgetsList.cpp
  dtkWidgetsListControl.cpp
  dtkWidgetsHUD.cpp
  dtkWidgetsHUDInfo.cpp
  dtkWidgetsHUDItem.cpp
  dtkWidgetsLogView.cpp
  dtkWidgetsMainWindow.cpp
  dtkWidgetsManager.cpp
  dtkWidgetsMenu.cpp
  dtkWidgetsMenu+ux.cpp
  dtkWidgetsMenuBar.cpp
  dtkWidgetsMenuItem.cpp
  dtkWidgetsMenuItem+custom.cpp
  dtkWidgetsMenuSpy.cpp
  dtkWidgetsOverlayPane.cpp
  dtkWidgetsOverlayPaneItem.cpp
  dtkWidgetsOverlayPaneSlider.cpp
  dtkWidgetsOverlayRope.cpp
  dtkWidgetsParameter.cpp
  dtkWidgetsParameterBoolCheckBox.cpp
  dtkWidgetsParameterBoolPushButton.cpp
  dtkWidgetsParameterBrowse.cpp
  dtkWidgetsParameterDirBrowse.cpp
  dtkWidgetsParameterDoubleRange.cpp
  dtkWidgetsParameterDoubleSlider.cpp
  dtkWidgetsParameterDoubleSpinBox.cpp
  dtkWidgetsParameterFactory.cpp
  dtkWidgetsParameterFileBrowse.cpp
  dtkWidgetsParameterFileSave.cpp
  dtkWidgetsParameterIntRange.cpp
  dtkWidgetsParameterIntSlider.cpp
  dtkWidgetsParameterIntSpinBox.cpp
  dtkWidgetsParameterListStringListCheckBox.cpp
  dtkWidgetsParameterLongLongSpinBox.cpp
  dtkWidgetsParameterMenuBarGenerator.cpp
  dtkWidgetsParameterScientificSpinBox.cpp
  dtkWidgetsParameterStringLineEdit.cpp
  dtkWidgetsParameterStringListComboBox.cpp
  dtkWidgetsSpinBoxDouble.cpp
  dtkWidgetsSpinBoxLong.cpp
  dtkWidgetsWidget.cpp
  dtkWidgetsWorkspace.cpp
  dtkWidgetsWorkspaceBar.cpp
  dtkWidgetsWorkspaceLog.cpp
  dtkWidgetsWorkspaceStackBar.cpp)

set(${PROJECT_NAME}_SOURCES_RCC
  dtkWidgets.qrc)

set_property(SOURCE qrc_dtkWidgets.cpp PROPERTY SKIP_AUTOMOC ON)

## ###################################################################
## Config file
## ###################################################################

configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}Config.h.in"
  "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.h")
configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}Config.h.in"
  "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config")

set(${PROJECT_NAME}_HEADERS
  ${${PROJECT_NAME}_HEADERS}
  "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.h"
  "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config")

## #################################################################
## Build rules
## #################################################################

qt_add_resources(${PROJECT_NAME}_SOURCES_QRC ${${PROJECT_NAME}_SOURCES_RCC})

qt_add_library(${PROJECT_NAME}
  ${${PROJECT_NAME}_SOURCES_QRC}
  ${${PROJECT_NAME}_SOURCES}
  ${${PROJECT_NAME}_HEADERS})

target_include_directories(${PROJECT_NAME} PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>/..
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>/..
  $<INSTALL_INTERFACE:include/${PROJECT_NAME}>
  $<INSTALL_INTERFACE:include>)

target_link_libraries(${PROJECT_NAME} PRIVATE dtk::Themes)
target_link_libraries(${PROJECT_NAME} PRIVATE dtk::Log)

target_link_libraries(${PROJECT_NAME} PUBLIC Qt6::Core)
target_link_libraries(${PROJECT_NAME} PUBLIC Qt6::Gui)
target_link_libraries(${PROJECT_NAME} PRIVATE Qt6::Quick)
target_link_libraries(${PROJECT_NAME} PRIVATE Qt6::QuickWidgets)
target_link_libraries(${PROJECT_NAME} PUBLIC Qt6::Widgets)
target_link_libraries(${PROJECT_NAME} PUBLIC Qt6::Xml)

target_link_libraries(${PROJECT_NAME} PUBLIC dtk::Core)
target_link_libraries(${PROJECT_NAME} PUBLIC dtk::Fonts)
target_link_libraries(${PROJECT_NAME} PUBLIC dtk::ThemesWidgets)

add_library(dtk::Widgets ALIAS dtkWidgets)

## #################################################################
## Target properties
## #################################################################

set_target_properties(${PROJECT_NAME} PROPERTIES VERSION   ${${PROJECT_NAME}_VERSION}
                                                 SOVERSION ${${PROJECT_NAME}_VERSION_MAJOR})

## #################################################################
## Export header file
## #################################################################

generate_export_header(${PROJECT_NAME} EXPORT_FILE_NAME "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Export")
generate_export_header(${PROJECT_NAME} EXPORT_FILE_NAME "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Export.h")

set(${PROJECT_NAME}_HEADERS
  ${${PROJECT_NAME}_HEADERS}
 "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Export"
 "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Export.h")

## ###################################################################
## Install rules - files
## ###################################################################

install(
FILES
  ${${PROJECT_NAME}_HEADERS}
DESTINATION
  include/${PROJECT_NAME})

## ###################################################################
## Install rules - targets
## ###################################################################

install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}-targets
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

install(EXPORT ${PROJECT_NAME}-targets
FILE
  ${PROJECT_NAME}Targets.cmake
DESTINATION
  ${CMAKE_INSTALL_LIBDIR}/cmake/dtkWidgets)

export(EXPORT ${PROJECT_NAME}-targets
FILE
  ${CMAKE_BINARY_DIR}/${PROJECT_NAME}Targets.cmake)

######################################################################
### CMakeLists.txt ends here
