// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsLayout.h"
#include "dtkWidgetsLayoutItem.h"
#include "dtkWidgetsLayoutItem_p.h"
#include "dtkWidgetsLayoutWriter.h"
#include "dtkWidgetsWidget.h"

#include <QtCore>
#include <QtWidgets>
#include <QtXml>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsLayoutWriterPrivate
{
public:
    QString locate(const QString& file);

public:
    dtkWidgetsLayout *layout = nullptr;
};

QString dtkWidgetsLayoutWriterPrivate::locate(const QString& file)
{
    QString path = QStandardPaths::locate(QStandardPaths::AppDataLocation, file);

    if (path.isEmpty()) {
        path += QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).first();

        QDir dummy; dummy.mkdir(path);

        path += QDir::separator();
        path += file;
    }

    return path;
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////


dtkWidgetsLayoutWriter::dtkWidgetsLayoutWriter(void)
{
    d = new dtkWidgetsLayoutWriterPrivate;
}

dtkWidgetsLayoutWriter::~dtkWidgetsLayoutWriter(void)
{
    delete d;
}

void dtkWidgetsLayoutWriter::setLayout(dtkWidgetsLayout *layout)
{
    d->layout = layout;
}

dtkWidgetsLayout *dtkWidgetsLayoutWriter::layout(void)
{
    return d->layout;
}

void dtkWidgetsLayoutWriter::write(const QString & file_name)
{
    QString real_file = file_name;
    if (real_file.isEmpty())
        real_file = d->locate("layout.dtk");
    QFile file(real_file);

    if(!file.open(QIODevice::WriteOnly))
        return;

// ///////////////////////////////////////////////////////////////////
// Build document
// ///////////////////////////////////////////////////////////////////

    QDomDocument document;

    QDomElement r = document.createElement("r");

    if (d->layout->root()->d->a)
        r.setAttribute("o", d->layout->root()->d->splitter->orientation() == Qt::Horizontal ? "h" : "v");

    std::function<void (QDomNode&, dtkWidgetsLayoutItem *)> fill;

    fill = [&] (QDomNode& node, dtkWidgetsLayoutItem *item) {

        if (item->d->a) {

            QDomElement a = document.createElement("a");

            if (item->d->a->d->a && item->d->a->d->b)
                a.setAttribute("o", item->d->splitter->orientation() == Qt::Horizontal ? "v" : "h");

            fill(a, item->d->a);

            node.appendChild(a);
        }

        if (item->d->b) {

            QDomElement b = document.createElement("b");

            if (item->d->b->d->a && item->d->b->d->b)
                b.setAttribute("o", item->d->splitter->orientation() == Qt::Horizontal ? "v" : "h");

            fill(b, item->d->b);

            node.appendChild(b);
        }

        if(!item->d->a && !item->d->b && item->proxy()->d->view) {
            node.appendChild(createNode(item, document));
        }
    };

    fill(r, d->layout->root());

    document.appendChild(r);

    QTextStream stream(&file);

    stream << document.toString();

    file.close();
}

QDomElement dtkWidgetsLayoutWriter::createNode(dtkWidgetsLayoutItem *item, QDomDocument& document)
{
    QDomElement n = document.createElement("view");
    n.setAttribute("view_name", item->proxy()->d->view->objectName());
    n.setAttribute("class_name", item->proxy()->d->view->metaObject()->className());

    QString action_name = item->proxy()->d->view->actionName();

    if(!action_name.isEmpty()) {
        n.setAttribute("action_name", action_name);
    }

    return n;
}

//
// dtkWidgetsLayoutWriter.cpp ends here
