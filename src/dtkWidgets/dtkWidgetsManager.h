// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <QtWidgets/QFrame>

class dtkWidgetsLayout;
class dtkWidgetsWidget;

class DTKWIDGETS_EXPORT dtkWidgetsManager : public QFrame
{
    Q_OBJECT

public:
     dtkWidgetsManager(QWidget *parent = nullptr);
    ~dtkWidgetsManager(void);

signals:
    void focused(dtkWidgetsWidget *view);

public slots:
    void clear(void);

public slots:
    void onViewFocused(dtkWidgetsWidget *widget);
    void onViewUnfocused(dtkWidgetsWidget *widget);

public:
    dtkWidgetsLayout *layout(void);

private:
    class dtkWidgetsManagerPrivate *d;
};

//
// dtkWidgetsManager.h ends here
