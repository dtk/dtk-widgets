// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsDataManager.h"
#include "dtkWidgetsDataManager_p.h"
#include "dtkWidgetsDataManagerData.h"
#include "dtkWidgetsDataManagerItem.h"
#include "dtkWidgetsDataManagerFocus.h"

#include <dtkThemes>

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsDataManagerPrivate
// ///////////////////////////////////////////////////////////////////

int dtkWidgetsDataManagerPrivate::item_counter = 0;

dtkWidgetsDataManagerPrivate::dtkWidgetsDataManagerPrivate(QWidget *parent) : QScrollArea(parent)
{
    this->contents = new QWidget(this);

    QHBoxLayout *layout = new QHBoxLayout(this->contents);
    layout->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    this->setFrameShape(QFrame::NoFrame);
    this->setWidget(this->contents);
    this->setWidgetResizable(true);
    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

}

dtkWidgetsDataManagerPrivate::~dtkWidgetsDataManagerPrivate(void)
{

}

QSize dtkWidgetsDataManagerPrivate::sizeHint(void) const
{
    return QSize(200, 120);
}

dtkWidgetsDataManagerItem *dtkWidgetsDataManagerPrivate::create(dtkWidgetsDataManagerDataPtr data_serie, const QColor &color, const QImage &img)
{
    if (!data_serie)
        return nullptr;

    QPixmap pixmap;

    if(!img.isNull()) {
        // we want a square image
        int w = img.width();
        int h = img.height();

        int length = w < h ? w : h; // std::min(w, h);
        int ori_x = w < h ? 0 : w/2 - length/2;
        int ori_y = w < h ? h/2 - length/2 : 0;

        pixmap = QPixmap::fromImage(img.copy( ori_x, ori_y, length, length));
    } else {
        if (data_serie->metadata.contains(dtkWidgetsDataManagerData::ICON)) {
            pixmap = QPixmap(data_serie->metadata[dtkWidgetsDataManagerData::ICON].toString());
        }
    }

    //if there is no visualization at all
    if(pixmap.height() == 0 || pixmap.width() == 0) {
        QIcon icon = QApplication::style()->standardIcon(QStyle::SP_MessageBoxQuestion);
        pixmap = icon.pixmap(100,100);
    }

    auto item = new dtkWidgetsDataManagerItem(color, pixmap, this);

    connect(item, &dtkWidgetsDataManagerItem::destroy, [=,this] (void) -> void
    {
        this->contents->layout()->removeWidget(item);
        this->data_series.remove(item);

        delete item;
    });

    connect(item, &dtkWidgetsDataManagerItem::save, [=] (void) -> void
    {
        // TODO: delete through a pure virtual method
    });

    connect(item, &dtkWidgetsDataManagerItem::clicked, [=,this] (void) -> void
    {
       q->present(item);
    });

    // ///////////////////////////////////////////////////////////////////
    // Meta metadata computation
    // ///////////////////////////////////////////////////////////////////

    data_serie->reference = item;

    return item;
}

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsDataManager
// ///////////////////////////////////////////////////////////////////

dtkWidgetsDataManager *dtkWidgetsDataManager::instance(void)
{
    if (!s_instance)
        s_instance = new dtkWidgetsDataManager;

    return s_instance;
}

int dtkWidgetsDataManager::addData(dtkWidgetsDataManagerDataPtr data_serie, const QColor &color, const QImage& img)
{
    dtkWidgetsDataManagerItem *item = d->create(data_serie, color, img);
    item->id = d->item_counter++;

    d->data_series.insert(item, data_serie);

    d->contents->layout()->addWidget(item);

    connect( item, &dtkWidgetsDataManagerItem::clicked, [=,this] (void) { emit itemClickedSignal(item); } );

    return item->id;
}

dtkWidgetsDataManagerDataPtr dtkWidgetsDataManager::get(int index)
{
    for (auto it = d->data_series.begin(); it != d->data_series.end(); ++it)
        if (index == it.key()->id)
            return *it;

    return dtkWidgetsDataManagerDataPtr();
}

dtkWidgetsDataManagerItem *dtkWidgetsDataManager::getItem(int index)
{
    for (auto it = d->data_series.begin(); it != d->data_series.end(); ++it) {
        if (index == it.key()->id) {
            return it.key();
        }
    }

    return nullptr;
}

QPixmap dtkWidgetsDataManager::thumbnail(int index)
{
    for (auto it = d->data_series.begin(); it != d->data_series.end(); ++it)
        if (index == it.key()->id)
            return it.key()->pixmap();

    return QPixmap();
}

QVector<int> dtkWidgetsDataManager::getDataIds(void)
{
    QVector<int> result = QVector<int>();

    for (auto it = d->data_series.begin(); it != d->data_series.end(); ++it) {
        result.push_back(it.key()->id);
    }

    return result;
}

dtkWidgetsDataManager::dtkWidgetsDataManager(QWidget *parent) : QFrame(parent)
{
    d = new dtkWidgetsDataManagerPrivate;
    d->q = this;

    QHBoxLayout *t_layout = new QHBoxLayout;
    t_layout->setContentsMargins(0, 0, 0, 0);
    t_layout->setSpacing(0);
    t_layout->addWidget(d);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setAlignment(Qt::AlignTop);
    layout->addSpacing(10);  // for the title bar
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addLayout(t_layout);

    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    this->setMouseTracking(true);
    this->setAcceptDrops(true);
}

dtkWidgetsDataManager::~dtkWidgetsDataManager(void)
{
    delete d;
}

QSize dtkWidgetsDataManager::sizeHint(void) const
{
    return QSize(200, 120);
}

void dtkWidgetsDataManager::present(dtkWidgetsDataManagerItem *item)
{
    // do nothing in a collapsed state
    if(d->state == dtkWidgetsDataManagerPrivate::Collapsed)
        return;

    if(!d->focus_item)
        d->focus_item = new dtkWidgetsDataManagerFocus(this);

    QVariantMap metadata;
    for (auto it = d->data_series.begin(); it != d->data_series.end(); ++it) {
        if (item == it.key()) {
            metadata =  (*it)->metadata;
        }
    }
    d->focus_item->setItem(item, metadata);

    QSequentialAnimationGroup *animation = new QSequentialAnimationGroup(this);

    if (d->focus_item->presented) {
        QVariantAnimation *p_animation = new QVariantAnimation(this);
        p_animation->setDuration(250);
        p_animation->setStartValue(d->focus_item->destnt);
        p_animation->setEndValue(d->focus_item->source);
        p_animation->setEasingCurve(QEasingCurve::OutQuad);

        QVariantAnimation *s_animation = new QVariantAnimation(this);
        s_animation->setDuration(250);
        s_animation->setStartValue(d->focus_item->d_size);
        s_animation->setEndValue(d->focus_item->s_size);
        s_animation->setEasingCurve(QEasingCurve::OutQuad);

        QParallelAnimationGroup *g_animation = new QParallelAnimationGroup(this);
        g_animation->addAnimation(p_animation);
        g_animation->addAnimation(s_animation);

        connect(p_animation, &QVariantAnimation::valueChanged, [=,this](const QVariant &value) {
            d->focus_item->move(value.toPoint());
        });

        connect(s_animation, &QVariantAnimation::valueChanged, [=,this](const QVariant &value) {
            int min_v = value.toSize().width() < value.toSize().height() ? value.toSize().width() : value.toSize().height();
            d->focus_item->scalePixmap(min_v, min_v);
            d->focus_item->resize(value.toSize());
        });

        connect(g_animation, &QAbstractAnimation::finished, [=,this]() {
            d->focus_item->presented = false;
            d->focus_item->hide();
         });

        d->focus_item->hideMetaData();
        animation->addAnimation(g_animation);
    } else {

        //let's present it;
        d->focus_item->disconnect(SIGNAL(clicked()));
        connect(d->focus_item, &dtkWidgetsDataManagerFocus::clicked, [=,this]() {this->present(item); });

        d->focus_item->move(item->pos());
        d->focus_item->resize(item->size());
        //d->focus_item->setStyleSheet("border: 2px solid white;");
        d->focus_item->show();

        d->focus_item->source = d->focus_item->pos();

        d->focus_item->destnt = QPoint(this->size().width()  * 0.15,
                                       this->size().height() * 0.25);

        d->focus_item->s_size = d->focus_item->size();
        d->focus_item->d_size = QSize(this->size().width()  * 0.7,
                                      this->size().height() * 0.5);

        //p = position
        QVariantAnimation *p_animation = new QVariantAnimation(this);
        p_animation->setDuration(500);
        p_animation->setStartValue(d->focus_item->source);
        p_animation->setEndValue(d->focus_item->destnt);
        p_animation->setEasingCurve(QEasingCurve::OutQuad);

        // s = size
        QVariantAnimation *s_animation = new QVariantAnimation(this);
        s_animation->setDuration(500);
        s_animation->setStartValue(d->focus_item->s_size);
        s_animation->setEndValue(d->focus_item->d_size);
        s_animation->setEasingCurve(QEasingCurve::OutQuad);

        QParallelAnimationGroup *g_animation = new QParallelAnimationGroup(this);
        g_animation->addAnimation(p_animation);
        g_animation->addAnimation(s_animation);

        connect(p_animation, &QVariantAnimation::valueChanged, [=,this] (const QVariant &value)
        {
            d->focus_item->move(value.toPoint());
        });

        connect(s_animation, &QVariantAnimation::valueChanged, [=,this] (const QVariant &value)
        {
            int min_v = value.toSize().width() < value.toSize().height() ? value.toSize().width() : value.toSize().height();
            d->focus_item->scalePixmap(min_v, min_v);
            d->focus_item->resize(value.toSize());
        });

        connect(g_animation, &QAbstractAnimation::finished, [=,this] (void)
        {
            d->focus_item->presented = true;
            d->focus_item->showMetadata();
        });

        animation->addAnimation(g_animation);
    }

    animation->start(QAbstractAnimation::DeleteWhenStopped);
}

void dtkWidgetsDataManager::enterEvent(QEnterEvent *)
{
    d->inside = true;

    this->update();
}

void dtkWidgetsDataManager::leaveEvent(QEvent *)
{
    d->inside = false;

    this->update();
}

void dtkWidgetsDataManager::mousePressEvent(QMouseEvent *event)
{
    QRect handle = QRect(this->size().width() / 2 - 100, this->size().height() - 10, 200, 10);

    if (handle.contains(event->pos())) {

        if (d->state == dtkWidgetsDataManagerPrivate::Collapsed) {
            emit expand();
            d->state = dtkWidgetsDataManagerPrivate::Expanded;
        } else {
            emit shrink();
            d->state = dtkWidgetsDataManagerPrivate::Collapsed;

            // un present the focus item if it's presented
            if(d->focus_item) {
                delete d->focus_item;
                d->focus_item = nullptr;
            }
        }
    }
}

void dtkWidgetsDataManager::paintEvent(QPaintEvent *event)
{
    QFrame::paintEvent(event);

    if (!d->inside)
        return;

    QPainter painter(this);
    painter.fillRect(event->rect(), this->backgroundRole());
    painter.setBrush(Qt::white);
    painter.drawRoundedRect(event->rect().width() / 2 - 100, event->rect().height() - 10, 200, 6, 3, 3);
}

dtkWidgetsDataManager *dtkWidgetsDataManager::s_instance = nullptr;

void dtkWidgetsDataManager::dropEvent(QDropEvent *event)
{
    QString source_class_name = event->source()->metaObject()->className();

    if(source_class_name == "dtkWidgetsDataManagerItem") {
        return; // no drop from a managerItem
    }

    auto newData = new dtkWidgetsDataManagerData;

    auto path = event->mimeData()->text();
    QRandomGenerator prng(QTime::currentTime().msec());

    if (path.startsWith("file://")) {
        path.replace("file://", "");
    }

    newData->metadata[dtkWidgetsDataManagerData::ICON] = path;
    quint32 r = prng.generate();
    quint32 g = prng.generate();
    quint32 b = prng.generate();
    long id = addData(newData, QColor::fromRgb(r % 255, g % 255, b % 255));
    auto item = getItem(id);

    connect(item, &dtkWidgetsDataManagerItem::clicked, [=,this] (void)
    {
        this->itemClicked(item);
    });

    event->accept();
}

void dtkWidgetsDataManager::dragEnterEvent(QDragEnterEvent *event)
{
    event->acceptProposedAction();
}

void dtkWidgetsDataManager::itemClicked(dtkWidgetsDataManagerItem *item)
{
    emit itemClickedSignal(item);
}


//
// dtkWidgetsDataManager.cpp ends here
