#pragma once

#include <dtkWidgetsExport>

#include <QtCore>
#include <QLabel>

class dtkFontAwesome;

// ///////////////////////////////////////////////////////////////////
// gnomonItemButton
// ///////////////////////////////////////////////////////////////////

class DTKWIDGETS_EXPORT dtkWidgetsDataManagerItemButton : public QLabel
{
    Q_OBJECT

public:
    dtkWidgetsDataManagerItemButton(const QColor& color, int icon, QWidget *parent = nullptr);
    ~dtkWidgetsDataManagerItemButton(void);

signals:
    void clicked(void);

protected:
    void mousePressEvent(QMouseEvent *);

private:
    dtkFontAwesome *font;
};
