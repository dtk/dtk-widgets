// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <QtCore>
#include <QtWidgets>

class dtkWidgetsDataManagerItem;

class DTKWIDGETS_EXPORT dtkWidgetsDataManagerData : public QObject
{
    Q_OBJECT

public:
     dtkWidgetsDataManagerData(QObject *parent = nullptr);
    ~dtkWidgetsDataManagerData(void);

public:
    virtual long count(void) const {
        return 0;
    }

public:
    static const QString ICON;

public:
    inline void addMetadata(const QString& key, QVariant value) {
        this->metadata[key] = value;
    }

    inline void setData(QVariant data) {
        this->data = data;
    }

public: // TODO: protected
    dtkWidgetsDataManagerItem *reference;

public: // TODO: protected
    QVariantMap metadata;
    QVariant        data;

private:
    class dtkWidgetsDataManagerDataPrivate *d;

// /////////////////////////////////////////////////////////////////////////////
// TODO
// /////////////////////////////////////////////////////////////////////////////

// private:
//     friend class dtkWidgetsDataManager;
//     friend class dtkWidgetsDataManagerDataPrivate;
//     friend class dtkWidgetsDataManagerItem;
//     friend class dtkWidgetsDataManagerPrivate;
};

using dtkWidgetsDataManagerDataPtr = QPointer<dtkWidgetsDataManagerData>;

//
// dtkWidgetsDataManagerData.h ends here
