#pragma once

#include <dtkWidgetsExport>

#include "dtkWidgetsParameter.h"

#include <dtkCore/dtkCoreParameterRange>

class DTKWIDGETS_EXPORT dtkWidgetsParameterIntRange : public dtkWidgetsParameterBase<dtk::d_range_int>
{
    Q_OBJECT

public:
     dtkWidgetsParameterIntRange(QWidget* parent = 0);
    ~dtkWidgetsParameterIntRange(void);

public:
    bool connect(dtkCoreParameter *) override;
    void setReadOnly(bool) override;

private:
    using dtkWidgetsParameterBase<dtk::d_range_int>::m_parameter;

    class dtkWidgetsParameterIntRangePrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkWidgetsParameter *dtkWidgetsParameterIntRangeCreator(void)
{
    return new dtkWidgetsParameterIntRange();
}

