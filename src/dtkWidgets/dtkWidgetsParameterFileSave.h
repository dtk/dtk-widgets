// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include "dtkWidgetsParameterBrowse.h"

class  DTKWIDGETS_EXPORT dtkWidgetsParameterFileSave : public dtkWidgetsParameterBrowse
{
    Q_OBJECT

public:
    dtkWidgetsParameterFileSave(QWidget* parent = nullptr);

private:
    QString browse(void) override;
};

// ///////////////////////////////////////////////////////////////////

inline dtkWidgetsParameter *dtkWidgetsParameterFileSaveCreator(void)
{
    return new dtkWidgetsParameterFileSave();
}
//
// dtkWidgetsParameterFileSave.h ends here
