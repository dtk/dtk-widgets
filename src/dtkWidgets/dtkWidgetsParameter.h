// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <QtWidgets/QWidget>
#include <QVariant>

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsParameter
// ///////////////////////////////////////////////////////////////////

class dtkCoreParameter;

class DTKWIDGETS_EXPORT dtkWidgetsParameter : public QWidget
{
    Q_OBJECT

public:
     dtkWidgetsParameter(QWidget *);
    ~dtkWidgetsParameter(void) = default;

public:
    virtual bool connect(dtkCoreParameter *) = 0;

public:
    virtual dtkCoreParameter *parameter(void) const = 0;

public:
    bool isReadOnly(void);
    virtual void setReadOnly(bool);

    void setAdvanced(bool);
    bool advanced(void);

public:
    virtual void reset(void);

signals:
    void advancedChanged(bool);

protected:
    bool m_readonly = false;
    bool m_advanced = false;
    QVariant m_default;
};

Q_DECLARE_METATYPE(dtkWidgetsParameter *);

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsParameterBase declaration
// ///////////////////////////////////////////////////////////////////

template <typename T>
class dtkWidgetsParameterBase : public dtkWidgetsParameter
{
public:
     dtkWidgetsParameterBase(QWidget *);
    ~dtkWidgetsParameterBase(void);

    dtkCoreParameter *parameter(void) const final;

protected:
    T *m_parameter = nullptr;
    QMetaObject::Connection m_c;

};

// ///////////////////////////////////////////////////////////////////

template <typename T>
inline dtkWidgetsParameterBase<T>::dtkWidgetsParameterBase(QWidget *p) : dtkWidgetsParameter(p)
{

}

template <typename T>
inline dtkWidgetsParameterBase<T>::~dtkWidgetsParameterBase(void)
{
    if(m_parameter && m_c) {
        m_parameter->disconnect(m_c);
    }
}


template <typename T>
inline dtkCoreParameter *dtkWidgetsParameterBase<T>::parameter(void) const
{
    return m_parameter;
}

//
// dtkWidgetsParameter.h ends here
