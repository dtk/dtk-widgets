// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsMainWindow.h"

#include "dtkApplication.h"
#include "dtkWidgetsMenu.h"
#include "dtkWidgetsMenuBar.h"
#include "dtkWidgetsMenuItem.h"
#include "dtkWidgetsMenuItem+custom.h"

#include <dtkThemes/dtkThemesEngine>
#include <dtkFonts/dtkFontAwesome>

#include <dtkCore>

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsMainWindowPrivate
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsMainWindowPrivate
{
public:
    dtkWidgetsMenu *populateAdvancedMenu(void);
    dtkWidgetsMenu *populateThemesMenu(dtkWidgetsMenu *);
    dtkWidgetsMenu *populateAboutMenu(dtkWidgetsMenu *);

    QObject *populateAdvancedParametersSwitch(dtkWidgetsMenu *);

public:
    dtkWidgetsMenuBar *menubar = nullptr;

public:
    dtkWidgetsMenu *menu_config = nullptr;
};

dtkWidgetsMenu *dtkWidgetsMainWindowPrivate::populateAdvancedMenu(void)
{
    if(!this->menu_config) {
        this->menu_config = this->menubar->addMenu(fa::cogs, "Configuration");
        this->populateThemesMenu(this->menu_config);
        this->populateAdvancedParametersSwitch(this->menu_config);
        //        this->populateAboutMenu(this->menu_advanced);
    }

    return this->menu_config;
}

dtkWidgetsMenu *dtkWidgetsMainWindowPrivate::populateThemesMenu(dtkWidgetsMenu *menu)
{
    dtkWidgetsMenu *menu_themes = menu->menu("Themes");
    if(!menu_themes) {
        menu_themes = menu->addMenu(fa::paintbrush, "Choose Themes");
        for (const QString theme : dtkThemesEngine::instance()->themes()) {
            dtkWidgetsMenuItem * item = menu_themes->addItem(new dtkWidgetsMenuItemTheme(theme));
            QObject::connect(item, &dtkWidgetsMenuItem::clicked, [=] () {
                dtkThemesEngine::instance()->apply(theme);
            });
        }
    }

    return menu_themes;
}

dtkWidgetsMenu *dtkWidgetsMainWindowPrivate::populateAboutMenu(dtkWidgetsMenu *menu)
{
    dtkWidgetsMenu *menu_about = menu->menu("About");
    if(!menu_about) {
        menu_about = menu->addMenu(fa::question, "About");
        menu_about->addMenu(fa::question, "dtk")->addItem(0, "todo");
    }

    return menu_about;
}

QObject *dtkWidgetsMainWindowPrivate::populateAdvancedParametersSwitch(dtkWidgetsMenu *menu)
{
    QObject *switch_widget = menu->object("Switch");

    if(!switch_widget) {
        dtk::d_bool *switch_parameter = new dtk::d_bool("Advanced Parameters", false, false, true, "Shows / Hide the advanced parameters in the menu");
        switch_parameter->setUid("menu_bar/switch_advanced_parameters");

        switch_parameter->connect([=,this](QVariant v) {
            if(!menubar) {
                return;
            }

            menubar->advancedParametersDisplayed(v.value<dtk::d_bool>());
        });

        switch_widget = menu->addParameter("Toggle Advanced Parameters", switch_parameter, "dtkWidgetsParameterBoolCheckBox", true);

        QObject::connect(switch_widget, &QWidget::destroyed, [=] () {
            delete switch_parameter;
        });
    }

    return switch_widget;
}

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsMainWindow
// ///////////////////////////////////////////////////////////////////

dtkWidgetsMainWindow::dtkWidgetsMainWindow(QWidget *parent) : dtkThemesWidgetsMainWindow(parent), d(new dtkWidgetsMainWindowPrivate)
{
    dtkApp->setWindow(this);
}

dtkWidgetsMainWindow::~dtkWidgetsMainWindow(void)
{
    delete d;
}

dtkWidgetsMenuBar *dtkWidgetsMainWindow::menubar(void)
{
    if (!d->menubar) {
        d->menubar = new dtkWidgetsMenuBar(this);
    }
    return d->menubar;
}

void dtkWidgetsMainWindow::setCentralWidget(QWidget *widget)
{
    dtkThemesWidgetsMainWindow::setCentralWidget(widget);
    if (this->titlebar() && d->menubar) {
        d->menubar->stackUnder(this->titlebar());
    }
}

void dtkWidgetsMainWindow::populate(void)
{
    Q_UNUSED(this->menubar());

    Q_UNUSED(d->populateAdvancedMenu());
}

void dtkWidgetsMainWindow::resizeEvent(QResizeEvent *event)
{
    dtkThemesWidgetsMainWindow::resizeEvent(event);

    if (d->menubar)
        d->menubar->setFixedHeight(event->size().height());
}

//
// dtkWidgetsMainWindow.cpp ends here
