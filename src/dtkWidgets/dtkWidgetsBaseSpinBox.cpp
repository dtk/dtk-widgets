// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsBaseSpinBox.h"

#include <QtWidgets>

dtkWidgetsBaseSpinBox::dtkWidgetsBaseSpinBox(QWidget * parent)
{
    this->setStepEnabled(true);
    this->setLocale(QLocale::C);
    this->setButtonSymbols(QAbstractSpinBox::PlusMinus);
    this->setCorrectionMode(QAbstractSpinBox::CorrectToPreviousValue);
    this->setKeyboardTracking(true);
    this->setNoScrollFocusFilter();
}

dtkWidgetsBaseSpinBox::~ dtkWidgetsBaseSpinBox()
{

}

void dtkWidgetsBaseSpinBox::setNoScrollFocusFilter()
{
    this->setFocusPolicy( Qt::StrongFocus ); // must click on spinbox to focus it (doesn't focus on hover only)
    this->installEventFilter(this); // filter scroll events (see below)
}


// shameless ripoff https://stackoverflow.com/questions/5821802/qspinbox-inside-a-qscrollarea-how-to-prevent-spin-box-from-stealing-focus-when
// Forbid scroll events to reach spinbox when said spinbox has no focus (allow to scroll the pane without scrolling the spinbox if you didn't click on it)
bool dtkWidgetsBaseSpinBox::eventFilter(QObject *o, QEvent *e)
{
    if(e->type() == QEvent::Wheel &&
       qobject_cast<QAbstractSpinBox*>(o))
    {
        if(qobject_cast<QAbstractSpinBox*>(o)->focusPolicy() >= Qt::WheelFocus)
        {
            e->accept();
            return false;
        }
        else
        {
            e->ignore();
            return true;
        }
    }
    return QWidget::eventFilter(o, e);
}

QAbstractSpinBox::StepEnabled dtkWidgetsBaseSpinBox::stepEnabled(void) const
{
    return m_step_enabled;
}

void dtkWidgetsBaseSpinBox::setStepEnabled(bool val)
{
    m_step_enabled = (val?  QAbstractSpinBox::StepUpEnabled | QAbstractSpinBox::StepDownEnabled : QAbstractSpinBox::StepNone);
}

void dtkWidgetsBaseSpinBox::focusInEvent(QFocusEvent* e)
{
    this->setFocusPolicy( Qt::WheelFocus );
    this->lineEdit()->event(e);
    this->lineEdit()->setSelection(text().length(), text().length() + 1);

    QWidget::focusInEvent(e); // Bypass parent class QAbstractSpinBox to keep the setSelection of the lineEdit
}

void dtkWidgetsBaseSpinBox::focusOutEvent(QFocusEvent* e)
{
    this->setFocusPolicy( Qt::StrongFocus );

    QAbstractSpinBox::focusOutEvent(e); // parent class is responsible for emitting signal editingFinished

}
