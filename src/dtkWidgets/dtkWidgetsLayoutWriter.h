// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <QtCore>

class dtkWidgetsLayout;
class dtkWidgetsLayoutItem;

class QDomElement;
class QDomDocument;

class DTKWIDGETS_EXPORT dtkWidgetsLayoutWriter
{
public:
     dtkWidgetsLayoutWriter(void);
    ~dtkWidgetsLayoutWriter(void);

public:
    void setLayout(dtkWidgetsLayout *);
    dtkWidgetsLayout *layout(void);

public:
    void write(const QString &filename = QString());

public:
    virtual QDomElement createNode(dtkWidgetsLayoutItem *, QDomDocument&);

private:
    class dtkWidgetsLayoutWriterPrivate *d;
};

//
// dtkWidgetsLayoutWriter.h ends here
