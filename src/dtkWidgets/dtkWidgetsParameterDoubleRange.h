#pragma once

#include <dtkWidgetsExport>

#include "dtkWidgetsParameter.h"

#include <dtkCore/dtkCoreParameterRange>

class DTKWIDGETS_EXPORT dtkWidgetsParameterDoubleRange : public dtkWidgetsParameterBase<dtk::d_range_real>
{
    Q_OBJECT

public:
     dtkWidgetsParameterDoubleRange(QWidget* parent = 0);
    ~dtkWidgetsParameterDoubleRange(void);

public:
    bool connect(dtkCoreParameter *) override;
    void setReadOnly(bool) override;

private:
    using dtkWidgetsParameterBase<dtk::d_range_real>::m_parameter;

    class dtkWidgetsParameterDoubleRangePrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkWidgetsParameter *dtkWidgetsParameterDoubleRangeCreator(void)
{
    return new dtkWidgetsParameterDoubleRange();
}

