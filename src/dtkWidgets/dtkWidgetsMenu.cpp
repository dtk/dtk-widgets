// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsMenu.h"
#include "dtkWidgetsMenu+ux.h"
#include "dtkWidgetsMenuBar.h"
#include "dtkWidgetsMenuBar_p.h"
#include "dtkWidgetsMenuItem.h"

#include "dtkLog"
#include "dtkCoreParameter"

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsMenuPrivate
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsMenuPrivate
{
public:
    QMap<QString, QObject *> object_hash;
    QMap<QString, QObject *> object_hash_uid;
    QVector<QObject *>        object_list;
    QVector<dtkWidgetsMenu *>   menu_list;

public:
    QString title;

public:
    int icon_id;
};

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsMenu
// ///////////////////////////////////////////////////////////////////

dtkWidgetsMenu::dtkWidgetsMenu(int icon_id, const QString& title, QObject *parent): QObject(parent), d(new dtkWidgetsMenuPrivate)
{
    d->title   = title;
    d->icon_id = icon_id;
}

dtkWidgetsMenu::~dtkWidgetsMenu(void)
{
    this->clear();

    delete d;
}

dtkWidgetsMenu *dtkWidgetsMenu::addMenu(dtkWidgetsMenu *menu)
{
    if(menu == nullptr) {
        dtkWarn() << Q_FUNC_INFO << "please provide a non nullptr pointer";
        return nullptr;
    }

    if(!d->object_hash.contains(menu->title())) {
        d->object_hash.insert(menu->title(), menu);
        d->object_hash_uid.insert(menu->title(), menu);
        d->object_list << menu;
        d->menu_list << menu;
        menu->setParent(this);
        return menu;
    }

    dtkWarn() << Q_FUNC_INFO << "menu already exists (" << menu->title() << ")";

    return nullptr;
}

dtkWidgetsMenu *dtkWidgetsMenu::addMenu(int icon_id, const QString& title)
{
    dtkWidgetsMenu *menu = new dtkWidgetsMenu(icon_id, title);

    auto m = this->addMenu(menu);

    if (m != menu) {
        delete menu;
    }

    return m;
}

dtkWidgetsMenu *dtkWidgetsMenu::insertMenu(int pos, dtkWidgetsMenu *menu)
{
    if ( pos < 0 || pos > d->object_list.size() ) {
        dtkWarn() << Q_FUNC_INFO << "insertion position out of range (" << pos << ")";
        return nullptr;
    }

    if ( !menu ) {
        dtkWarn() << Q_FUNC_INFO << "please provide a non nullptr pointer";
        return nullptr;
    }

    int menu_pos = 0;
    for (int i=0; i<d->object_list.size(); ++i) {
        if (i >= pos) {
            break;
        }
        if (dynamic_cast<dtkWidgetsMenu*>(d->object_list[i]) != nullptr) {
            ++menu_pos;
        }
    }
    Q_ASSERT_X(menu_pos <= d->menu_list.size(), "internal error", Q_FUNC_INFO);

    menu->setParent(this);

    d->object_hash.insert(menu->title(), menu);
    d->object_hash_uid.insert(menu->title(), menu);
    d->menu_list.insert(menu_pos, menu);
    d->object_list.insert(pos, menu);
    return menu;
}

dtkWidgetsMenu *dtkWidgetsMenu::insertMenu(int pos, int icon_id, const QString &title)
{
    dtkWidgetsMenu *menu = new dtkWidgetsMenu(icon_id, title);

    auto m = this->insertMenu(pos, menu);

    if (m != menu)
        delete menu;

    return m;
}

void dtkWidgetsMenu::clear(void)
{
    d->object_hash.clear();
    d->object_hash_uid.clear();
    d->object_list.clear();
    d->menu_list.clear();
}

dtkWidgetsMenuItem *dtkWidgetsMenu::addItem(dtkWidgetsMenuItem *item)
{
    if ( item == nullptr ) {
        dtkWarn() << Q_FUNC_INFO << "please provide a non nullptr pointer";
        return nullptr;
    }
    if (!d->object_hash.contains(item->title())) {
        d->object_hash.insert(item->title(), item);
        if (item->isParameter()) {
            dtkWidgetsMenuItemParameter *item_param = static_cast<dtkWidgetsMenuItemParameter *>(item);
            d->object_hash_uid.insert(item_param->parameter()->uid(), item);
        } else {
            d->object_hash_uid.insert(item->title(), item);
        }
        d->object_list << item;
        item->setParent(this);
        return item;
    }

    dtkWarn() << Q_FUNC_INFO << "item already exists (" << item->title() << ")";
    return nullptr;
}

dtkWidgetsMenuItem *dtkWidgetsMenu::addItem(int icon_id, const QString& title)
{
    dtkWidgetsMenuItem *item = new dtkWidgetsMenuItem(icon_id, title);

    auto it = this->addItem(item);

    if (it != item) {
        delete item;
    }

    return it;
}

dtkWidgetsMenuItem *dtkWidgetsMenu::insertItem(int pos, dtkWidgetsMenuItem *item)
{
    if ( item == nullptr ) {
        dtkWarn() << Q_FUNC_INFO << "please provide a non nullptr pointer";
        return nullptr;
    }
    if ( !d->object_hash.contains(item->title()) ) {
        d->object_hash.insert(item->title(), item);
        if (item->isParameter()) {
            dtkWidgetsMenuItemParameter *item_param = static_cast<dtkWidgetsMenuItemParameter *>(item);
            d->object_hash_uid.insert(item_param->parameter()->uid(), item);
        } else {
            d->object_hash_uid.insert(item->title(), item);
        }
        d->object_list.insert(pos, item);
        return item;
    }

    dtkWarn() << Q_FUNC_INFO << "item already exists (" << item->title() << ")";
    return nullptr;
}

dtkWidgetsMenuItem *dtkWidgetsMenu::insertItem(int pos, int icon_id, const QString& title)
{
    dtkWidgetsMenuItem *item = new dtkWidgetsMenuItem(icon_id, title);

    auto it = this->insertItem(pos, item);

    if (it != item) {
        delete item;
    }

    return it;
}

dtkWidgetsMenuItem *dtkWidgetsMenu::addSeparator(void)
{
    static int count = 0;

    QString title = QString("separator_%1").arg(count++);

    dtkWidgetsMenuItem *separator = new dtkWidgetsMenuItem(title, this);
    separator->setSeparator(true);

    d->object_hash.insert(title, separator);
    d->object_hash_uid.insert(title, separator);
    d->object_list << separator;

    return separator;
}

dtkWidgetsMenuItem *dtkWidgetsMenu::addParameter(dtkWidgetsMenuItem *item)
{
    return this->addItem(item);
}

dtkWidgetsMenuItem *dtkWidgetsMenu::addParameter(const QString& title, dtkCoreParameter *parameter, const QString& representation, bool persistent)
{
    dtkWidgetsMenuItem *item = new dtkWidgetsMenuItemParameter(title, parameter, representation, persistent);

    auto it = this->addItem(item);

    if (it != item) {
        delete item;
    }

    return it;
}

void dtkWidgetsMenu::exportParameters(QJsonObject &json, bool recursive) const
{
    for (auto o: d->object_hash) {
        dtkWidgetsMenuItemParameter *item = dynamic_cast<dtkWidgetsMenuItemParameter*>(o);
        if (item){
            json.insert(item->parameter()->uid(), QJsonObject::fromVariantHash(item->parameter()->toVariantHash()));
        }
    }
    if (recursive) {
        for (auto menu: d->menu_list) {
            menu->exportParameters(json, true);
        }
    }
}

// Import parameter value from json to embedded parameters in menu and submenus
void dtkWidgetsMenu::importParameters(const QJsonObject &json, bool recursive) const
{
    for (auto o: d->object_list) {
        dtkWidgetsMenuItemParameter *item = dynamic_cast<dtkWidgetsMenuItemParameter*>(o);
        if (item){
            if (json.contains(item->parameter()->uid())) {
                QVariant v = json[item->parameter()->uid()].toVariant();
                item->parameter()->shareValue(v);
            }
        }
    }
    if (recursive) {
        for (auto menu: d->menu_list) {
            menu->importParameters(json, true);
        }
    }
}

void dtkWidgetsMenu::removeItem(dtkWidgetsMenuItem *item)
{
    if (!(item && d->object_hash.contains(item->title())))
        return;
    d->object_hash.remove(item->title());

    QString title;
    if (item->isParameter())
        title = (static_cast<dtkWidgetsMenuItemParameter *>(item))->parameter()->uid();
    else
        title = item->title();
    if(d->object_hash_uid.contains(title))
        d->object_hash_uid.remove(title);

    d->object_list.removeAll(item);
}

void dtkWidgetsMenu::removeMenu(dtkWidgetsMenu *menu)
{
    if (menu && d->object_hash.contains(menu->title()) && d->object_hash[ menu->title() ] == menu) {
        d->object_hash.remove(menu->title());
        d->object_hash_uid.remove(menu->title());
        d->object_list.removeAll(menu);
        d->menu_list.removeAll(menu);
    }
}

void dtkWidgetsMenu::renameMenu(dtkWidgetsMenu *menu, const QString& title)
{
    if (menu && d->object_hash.contains(menu->title()) && d->object_hash_uid.contains(menu->title())) {
        d->object_hash.remove(menu->title());
        d->object_hash_uid.remove(menu->title());
        menu->d->title = title;
        d->object_hash.insert(menu->title(), menu);
        d->object_hash_uid.insert(menu->title(), menu);
    }
}

void dtkWidgetsMenu::deleteLater(void)
{
    // qDebug() << Q_FUNC_INFO;

    dtkWidgetsMenuBar *parent_bar = dynamic_cast<dtkWidgetsMenuBar *>(this->parent());

    if(parent_bar) {

        // qDebug() << Q_FUNC_INFO << "1 - parent bar" << parent_bar;

        parent_bar->removeMenu(this);
        parent_bar->touch();

        QObject::deleteLater();

        return;
    }

    dtkWidgetsMenu *parent      = dynamic_cast<dtkWidgetsMenu *>(this->parent());
    dtkWidgetsMenu *parent_menu = dynamic_cast<dtkWidgetsMenu *>(this->parent());

    parent_bar = dynamic_cast<dtkWidgetsMenuBar *>(parent_menu->parent());

    while(!parent_bar) {

        parent = dynamic_cast<dtkWidgetsMenu *>(parent->parent());

        if (parent)
            parent_bar = dynamic_cast<dtkWidgetsMenuBar *>(parent->parent());
    }

    // qDebug() << Q_FUNC_INFO << "2 - parent bar"  << parent_bar;
    // qDebug() << Q_FUNC_INFO << "2 - parent menu" << parent_menu << parent_menu->title();
    // qDebug() << Q_FUNC_INFO << "2 -   this menu" << this        << this->title();

    std::function<void()> callback = [=, this] (void) -> void
    {
        parent_menu->removeMenu(this);

        QTimer::singleShot(250, [=, this] () {
            parent_bar->touch();

            QObject::deleteLater();
        });
    };

    if ((!parent_bar->d->c->stack.isEmpty()) && parent_bar->d->c->stack.last() == this) {
        parent_bar->d->c->stack.takeLast();
        parent_bar->d->c->switchToPrevSlide(this, callback);
    }
}

QString dtkWidgetsMenu::title(void) const
{
    return d->title;
}

QVector<dtkWidgetsMenu *> dtkWidgetsMenu::menus(void) const
{
    return d->menu_list;
}

dtkWidgetsMenu *dtkWidgetsMenu::menu(const QString& id) const
{
    if(!d->object_hash.contains(id))
        return nullptr;

    return dynamic_cast<dtkWidgetsMenu *>(d->object_hash[id]);
}

QObject *dtkWidgetsMenu::object(const QString& id)
{
    if(!d->object_hash.contains(id))
        return nullptr;

    return d->object_hash[id];
}

QObject *dtkWidgetsMenu::objectFromUid(const QString& id)
{
    if(!d->object_hash_uid.contains(id))
        return nullptr;

    return d->object_hash_uid[id];
}

QVector<QObject *> dtkWidgetsMenu::objects(void) const
{
    return d->object_list;
}

int dtkWidgetsMenu::icon(void) const
{
    return d->icon_id;
}

//
// dtkWidgetsMenu.cpp ends here
