#include "dtkWidgetsDataManagerItemButton.h"

#include <QApplication>
#include <QStyle>

#include <dtkFontAwesome>

dtkWidgetsDataManagerItemButton::dtkWidgetsDataManagerItemButton(const QColor& color, int icon, QWidget *parent) : QLabel(parent)
{
    this->font = dtkFontAwesome::instance();
    this->font->initFontAwesome();
    this->font->setDefaultOption("color", color);
    this->setPixmap(this->font->icon(icon).pixmap(16, 16));
    this->setStyleSheet("background: none; border: none;");
}

dtkWidgetsDataManagerItemButton::~dtkWidgetsDataManagerItemButton(void)
{

}

void dtkWidgetsDataManagerItemButton::mousePressEvent(QMouseEvent *)
{
    emit clicked();
}
