//
// dtkWidgetsParameterFileSave.cpp ends here
// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsParameterFileSave.h"

#include <QtWidgets>
#include <QStringBuilder>

dtkWidgetsParameterFileSave::dtkWidgetsParameterFileSave(QWidget* parent) : dtkWidgetsParameterBrowse(parent)
{}

QString dtkWidgetsParameterFileSave::browse(void) {
    return QFileDialog::getSaveFileName(this, "Save file", m_parameter->dirName(), m_parameter->filters().join(";;"));
}

//
// dtkWidgetsParameterFileSave.cpp ends here
