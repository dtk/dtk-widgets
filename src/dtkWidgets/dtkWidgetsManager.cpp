// Version: $Id: a5d71ac667471276e2d9bd802e088357b82a5b0f $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsManager.h"

#include "dtkWidgetsLayout.h"
#include "dtkWidgetsList.h"
#include "dtkWidgetsListControl.h"
#include "dtkWidgetsWidget.h"

#include <QtWidgets>

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsManagerPrivate
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsManagerPrivate
{
public:
    dtkWidgetsList *view_list = nullptr;
    dtkWidgetsLayout *view_layout = nullptr;
    QStackedWidget *view_inspector = nullptr;
};

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsManager
// ///////////////////////////////////////////////////////////////////

dtkWidgetsManager::dtkWidgetsManager(QWidget *parent) : QFrame(parent), d(new dtkWidgetsManagerPrivate)
{
    d->view_list = new dtkWidgetsList;
    d->view_layout = new dtkWidgetsLayout;
    d->view_inspector = new QStackedWidget;

    dtkWidgetsListControl *view_control = new dtkWidgetsListControl(this);
    view_control->setLayout(d->view_layout);
    view_control->setList(d->view_list);

    QSplitter *inner = new QSplitter(Qt::Vertical, this);
    inner->setHandleWidth(2);
    inner->addWidget(view_control);
    inner->addWidget(d->view_list);
    inner->addWidget(d->view_inspector);
    inner->setSizes(QList<int>() << 32  << (parent->size().height()-32)/8 << (parent->size().height()-32)/2);

    QSplitter *splitter = new QSplitter(this);
    splitter->setHandleWidth(2);
    splitter->addWidget(inner);
    splitter->addWidget(d->view_layout);
    splitter->setSizes(QList<int>() << 300 << parent->size().width() - 300);

    QHBoxLayout *main_layout = new QHBoxLayout(this);
    main_layout->setContentsMargins(0, 0, 0, 0);
    main_layout->setSpacing(0);
    main_layout->addWidget(splitter);

    connect(d->view_layout, SIGNAL(focused(dtkWidgetsWidget *)), this, SIGNAL(focused(dtkWidgetsWidget *)));
    connect(d->view_layout, SIGNAL(focused(dtkWidgetsWidget *)), this, SLOT(onViewFocused(dtkWidgetsWidget *)));
    connect(d->view_layout, SIGNAL(unfocused(dtkWidgetsWidget *)), this, SLOT(onViewUnfocused(dtkWidgetsWidget *)));
}

dtkWidgetsManager::~dtkWidgetsManager(void)
{
    delete d;

    d = nullptr;
}

void dtkWidgetsManager::clear(void)
{
    d->view_list->clear();
    d->view_layout->clear();
}

void dtkWidgetsManager::onViewFocused(dtkWidgetsWidget *widget)
{
    if (!widget->inspector())
        return;

    d->view_inspector->addWidget(widget->inspector());
    d->view_inspector->setCurrentWidget(widget->inspector());
}

void dtkWidgetsManager::onViewUnfocused(dtkWidgetsWidget *widget)
{
    if (!widget->inspector())
        return;

    d->view_inspector->removeWidget(widget->inspector());
}

dtkWidgetsLayout *dtkWidgetsManager::layout(void)
{
    return d->view_layout;
}

//
// dtkWidgetsManager.cpp ends here
