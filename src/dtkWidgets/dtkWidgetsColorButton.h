// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <QtWidgets/QPushButton>

class DTKWIDGETS_EXPORT dtkWidgetsColorButton : public QPushButton
{
    Q_OBJECT

public:
     dtkWidgetsColorButton(const QColor& color, QWidget *parent = nullptr);
     dtkWidgetsColorButton(QWidget *parent = nullptr);
    ~dtkWidgetsColorButton(void);

signals:
    void colorChosen(QColor);

public:
    const QColor& color(void);

protected:
    void paintEvent(QPaintEvent *);

protected:
    void mousePressEvent(QMouseEvent *);

private:
    class dtkWidgetsColorButtonPrivate *d;
};

//
// dtkWidgetsColorButton.h ends here
