// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsParameterDoubleSlider.h"

#include <QtWidgets>

#include <dtkThemes/dtkThemesEngine>

#include <dtkWidgetsBaseSlider>

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsParameterDoubleSliderPrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsParameterDoubleSliderPrivate
{
public:
    dtkWidgetsBaseSlider *slider = nullptr;
    QLabel  *min = nullptr;
    QLabel  *max = nullptr;
    QLineEdit *val = nullptr;
    QDoubleValidator *validator = nullptr;

    double nx = 1000.0;
};


// ///////////////////////////////////////////////////////////////////
// dtkWidgetsParameterDoubleSlider implementation
// ///////////////////////////////////////////////////////////////////

dtkWidgetsParameterDoubleSlider::dtkWidgetsParameterDoubleSlider(QWidget* parent) : dtkWidgetsParameterBase<dtk::d_real>(parent), d(new dtkWidgetsParameterDoubleSliderPrivate)
{

    d->min   = new QLabel("min");
    d->max   = new QLabel("max");
    d->val   = new QLineEdit;
    d->validator = new QDoubleValidator;
    d->slider= new dtkWidgetsBaseSlider;

    d->validator->setNotation(QDoubleValidator::StandardNotation);
    d->validator->setLocale(QLocale::C);

    d->slider->setOrientation(Qt::Horizontal);
    d->slider->setTickPosition(QSlider::TicksBelow);

    d->val->setValidator(d->validator);

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(d->val);
    layout->addWidget(d->min);
    layout->addWidget(d->slider);
    layout->addWidget(d->max);

    this->setLayout(layout);
}

dtkWidgetsParameterDoubleSlider::~dtkWidgetsParameterDoubleSlider(void)
{
    delete d;
}

void dtkWidgetsParameterDoubleSlider::setReadOnly(bool val)
{
    this->dtkWidgetsParameter::m_readonly = val;
    d->slider->setEnabled(!this->dtkWidgetsParameter::m_readonly);
    d->slider->setFocusPolicy((this->dtkWidgetsParameter::m_readonly? Qt::NoFocus : Qt::StrongFocus));
    d->val->setReadOnly(this->dtkWidgetsParameter::m_readonly);
    d->val->setFocusPolicy((this->dtkWidgetsParameter::m_readonly? Qt::NoFocus : Qt::StrongFocus));
}

void dtkWidgetsParameterDoubleSlider::resizeEvent(QResizeEvent* event)
{
    dtkWidgetsParameterBase::resizeEvent(event);
    d->val->setFixedWidth(event->size().width()/3);
}

bool dtkWidgetsParameterDoubleSlider::connect(dtkCoreParameter *p)
{
    if (!p) {
        qWarning() << Q_FUNC_INFO << "The input parameter is null. Nothing is done.";
        return false;
    }

    m_parameter = dynamic_cast<dtk::d_real *>(p);

    if(!m_parameter) {
        qWarning() << Q_FUNC_INFO << "The type of the parameter is not compatible with the widget dtkWidgetsParameterDoubleSlider.";
        return false;
    }
    int decimals = m_parameter->decimals();

    m_default  = m_parameter->variant();

    d->slider->setMinimum(0);
    d->slider->setMaximum((int)d->nx);
    d->min->setText(QString::number(m_parameter->min()));
    d->max->setText(QString::number(m_parameter->max()));
    d->validator->setRange(m_parameter->min(), m_parameter->max(), decimals);
    d->slider->setToolTip(m_parameter->documentation());

    if ((m_parameter->max()-m_parameter->min()) > std::numeric_limits<int>::max()) {
        dtkWarn() << "Min/Max range too wide, the slider will be broken";
    }
    int value = (m_parameter->value()-m_parameter->min()) * d->nx / (m_parameter->max()-m_parameter->min());

    // try to get the number of digits (integer part + decimals). first get the integer part
    int max_i = std::max((int)std::abs(m_parameter->max()), (int)std::abs(m_parameter->min())) ;
    int max_chars = std::to_string(max_i).length()+1+decimals;
    d->val->setMaxLength(max_chars);
    QString text = QString::number(m_parameter->value());
    d->val->setText(text);
    d->slider->setValue(value);
    d->slider->setTickInterval(1);

    m_c = m_parameter->connect([=,this] (QVariant v)
    {
        double value = v.value<dtk::d_real>().value();
        d->slider->blockSignals(true);

        d->min->setText(QString::number(m_parameter->min()));
        d->max->setText(QString::number(m_parameter->max()));
        d->validator->setRange(m_parameter->min(),m_parameter->max(), m_parameter->decimals());

        int i = (value-m_parameter->min()) * d->nx / (m_parameter->max()-m_parameter->min());
        d->slider->setValue(i);
        d->val->setText(QString::number(value));
        d->slider->blockSignals(false);
    });

    QObject::connect(d->slider, QOverload<int>::of(&QSlider::valueChanged), [=,this] (int v)
    {
        double value = m_parameter->min() + double(v)*(m_parameter->max()-m_parameter->min())/d->nx;
        m_parameter->shareValue(QVariant::fromValue(value));
    });

    QObject::connect(d->val, &QLineEdit::editingFinished, [=,this]() {
         double value = d->val->text().toDouble();
         m_parameter->shareValue(QVariant::fromValue(value));
    });

    return true;
}

//
// dtkWidgetsParameterDoubleSliderWidget.cpp ends here
