// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include "dtkWidgetsParameter.h"

#include <dtkCore/dtkCoreParameterInListStringList>

class  DTKWIDGETS_EXPORT dtkWidgetsParameterListStringListCheckBox : public dtkWidgetsParameterBase<dtk::d_inliststringlist>
{
    Q_OBJECT

public:
     dtkWidgetsParameterListStringListCheckBox(QWidget* parent = 0);
    ~dtkWidgetsParameterListStringListCheckBox(void);

public:
    bool connect(dtkCoreParameter *) override;
    void setReadOnly(bool) override;

private:
    using dtkWidgetsParameterBase<dtk::d_inliststringlist>::m_parameter;

    class dtkWidgetsParameterListStringListCheckBoxPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkWidgetsParameter *dtkWidgetsParameterListStringListCheckBoxCreator(void)
{
    return new dtkWidgetsParameterListStringListCheckBox();
}
//
// dtkWidgetsParameterListStringListCheckBox.h ends here
